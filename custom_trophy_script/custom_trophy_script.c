/* L2 + L1 + X = activate single trophy
 * L2 + L1 + R3 = activate all trophies
 * L2 + L1 + UP = next trophy
 * L2 + L1 + DOWN = previous trophy */

#include <natives.h>
#include <common.h>
#include <strings.h>
#include <types.h>
#include <consts.h>

#include "Functions.c"// from threesocks button input example

/********************** PS3 CONTROLLER BUTTON MAP **********************/
#define L1         0x4
#define L2         0x5
#define R1         0x6
#define R2         0x7
#define DPAD_UP    0x8
#define DPAD_DOWN  0x9
#define DPAD_LEFT  0xA
#define DPAD_RIGHT 0xB
#define START      0xC
#define SELECT     0xD
#define SQUARE     0xE
#define TRIANGLE   0xF
#define X          0x10
#define CIRCLE     0x11
#define STICK_L    0x12  //L3
#define STICK_R    0x13  //R3
/***********************************************************************/

char* trophyname[66];// names for each trophy
int trophytype[66];// type of trophy bronze, silver, or gold

int activateTrophyIncrement = false;// increase active trophy number
int activateTrophyDecrement = false;// decrease active trophy number
int activateTrophy = false;// enable only the selected trophy
int activatePlat = false;// enable all trophies
int activateYouDumbass = false;// tried to enable a trophy thats already unlocked
int activateAlreadyPlatBitch = false;// tried to enable all trophies when all are already unlocked
int TrophyNumber = 1;// the current active trophy
int trophynamesareset = false;// control to run SetTrophyNames only once
int trophytypesareset = false;// control to run SetTrophyTypes only once
float ppx, ppy, ppz;// player coordinates

void DrawRect(void)// draws the text background rectangles
{
	DRAW_RECT(0.22578125, 0.05694443, 0.36712500, 0.03888889, 0, 255, 255, 150);
	DRAW_RECT(0.22578125, 0.05694443, 0.36400000, 0.03333333, 0, 0, 0, 150);
}

void DrawTrophyNum(void)// draws the number of the selected trophy
{
	set_up_draw(2, 0.30000000, 0.30000000, 255, 255, 255, 210);
	draw_number("NUMBR", 0.05000000, 0.05000000, TrophyNumber);
}

void DrawTrophyStatus(void)// draws the status if the selected trophy
{
	if (HAS_ACHIEVEMENT_BEEN_PASSED(TrophyNumber))
	{
		set_up_draw(2, 0.30000000, 0.30000000, 1, 255, 1, 210);// green text
		draw_text("STRING", 0.10700000, 0.05000000, "OK");
	}
	else
	{
		set_up_draw(2, 0.30000000, 0.30000000, 255, 1, 1, 210);// red text
		draw_text("STRING", 0.10700000, 0.05000000, "NO");
	}
}

void DrawTrophyName(void)// draws the name of the selected trophy
{
	if (trophytype[TrophyNumber] == 1)
	{
		set_up_draw(2, 0.30000000, 0.30000000, 160, 82, 45, 210);// bronze text
	}
	else if (trophytype[TrophyNumber] == 2)
	{
		set_up_draw(2, 0.30000000, 0.30000000, 211, 211, 211, 210);// silver text
	}
	else if (trophytype[TrophyNumber] == 3)
	{
		set_up_draw(2, 0.30000000, 0.30000000, 255, 215, 1, 210);// gold text
	}
	draw_text("STRING", 0.16400000, 0.05000000, trophyname[TrophyNumber]);
}

void SetTrophyNames(void)// sets trophy names
{
	trophyname[1] = "Off The Boat";
	trophyname[2] = "One Hundred And Eighty";
	trophyname[3] = "Pool Shark";
	trophyname[4] = "King Of QUB3D";
	trophyname[5] = "Finish Him";
	trophyname[6] = "Genetically Superior";
	trophyname[7] = "Wheelie Rider";
	trophyname[8] = "Gobble Gobble";
	trophyname[9] = "Driving Mr. Bellic";
	trophyname[10] = "Rolled Over";
	trophyname[11] = "Walk Free";
	trophyname[12] = "Courier Service";
	trophyname[13] = "Retail Therapy";
	trophyname[14] = "Chain Reaction";
	trophyname[15] = "One Man Army";
	trophyname[16] = "Lowest Point";
	trophyname[17] = "Order Fulfilled";
	trophyname[18] = "Manhunt";
	trophyname[19] = "Cleaned The Mean Streets";
	trophyname[20] = "Fed The Fish";
	trophyname[21] = "It'll Cost Ya";
	trophyname[22] = "Sightseer";
	trophyname[23] = "Warm Coffee";
	trophyname[24] = "That's How We Roll!";
	trophyname[25] = "Half Million";
	trophyname[26] = "Impossible Trinity";
	trophyname[27] = "Full Exploration";
	trophyname[28] = "You Got The Message";
	trophyname[29] = "Dare Devil";
	trophyname[30] = "Assassin's Greed";
	trophyname[31] = "Endangered Species";
	trophyname[32] = "Under The Radar";
	trophyname[33] = "Dial B For Bomb";
	trophyname[34] = "Gracefully Taken";
	trophyname[35] = "Liberty City (5)";
	trophyname[36] = "No More Strangers";
	trophyname[37] = "That Special Someone";
	trophyname[38] = "You Won!";
	trophyname[39] = "Liberty City Minute";
	trophyname[40] = "Key To The City";
	trophyname[41] = "Teamplayer";
	trophyname[42] = "Cut Your Teeth";
	trophyname[43] = "Join The Midnight Club";
	trophyname[44] = "Fly The Co-op";
	trophyname[45] = "Taking It For The Team";
	trophyname[46] = "Top Of The Food Chain";
	trophyname[47] = "Top The Midnight Club";
	trophyname[48] = "Wanted";
	trophyname[49] = "Auf Wiedersehen Petrovic";
	trophyname[50] = "Let Sleeping Rockstars Lie";
	trophyname[51] = "TLAD: One Percenter";
	trophyname[52] = "TLAD: The Lost Boy";
	trophyname[53] = "TLAD: Easy Rider";
	trophyname[54] = "TLAD: Get Good Wood";
	trophyname[55] = "TLAD: Full Chat";
	trophyname[56] = "TBoGT: Gone Down";
	trophyname[57] = "TBoGT: Diamonds Forever";
	trophyname[58] = "TBoGT: Four Play";
	trophyname[59] = "TBoGT: Bear Fight";
	trophyname[60] = "TBoGT: Catch the Bus";
	trophyname[61] = "TBoGT: Snow Queen";
	trophyname[62] = "TBoGT: Adrenaline-Junkie";
	trophyname[63] = "TBoGT: Maestro";
	trophyname[64] = "TBoGT: Past the Velvet Rope";
	trophyname[65] = "TBoGT: Gold Star";
	trophynamesareset = true;
}

void SetTrophyTypes(void)// sets trophy types
{
	trophytype[1] = 1;
	trophytype[2] = 1;
	trophytype[3] = 1;
	trophytype[4] = 1;
	trophytype[5] = 1;
	trophytype[6] = 1;
	trophytype[7] = 1;
	trophytype[8] = 1;
	trophytype[9] = 1;
	trophytype[10] = 1;
	trophytype[11] = 1;
	trophytype[12] = 1;
	trophytype[13] = 1;
	trophytype[14] = 1;
	trophytype[15] = 2;
	trophytype[16] = 1;
	trophytype[17] = 1;
	trophytype[18] = 1;
	trophytype[19] = 1;
	trophytype[20] = 1;
	trophytype[21] = 1;
	trophytype[22] = 1;
	trophytype[23] = 1;
	trophytype[24] = 1;
	trophytype[25] = 1;
	trophytype[26] = 1;
	trophytype[27] = 1;
	trophytype[28] = 1;
	trophytype[29] = 2;
	trophytype[30] = 1;
	trophytype[31] = 2;
	trophytype[32] = 1;
	trophytype[33] = 1;
	trophytype[34] = 1;
	trophytype[35] = 2;
	trophytype[36] = 1;
	trophytype[37] = 1;
	trophytype[38] = 2;
	trophytype[39] = 3;
	trophytype[40] = 3;
	trophytype[41] = 1;
	trophytype[42] = 1;
	trophytype[43] = 1;
	trophytype[44] = 1;
	trophytype[45] = 1;
	trophytype[46] = 1;
	trophytype[47] = 1;
	trophytype[48] = 3;
	trophytype[49] = 1;
	trophytype[50] = 1;
	trophytype[51] = 1;
	trophytype[52] = 1;
	trophytype[53] = 1;
	trophytype[54] = 1;
	trophytype[55] = 1;
	trophytype[56] = 1;
	trophytype[57] = 1;
	trophytype[58] = 1;
	trophytype[59] = 1;
	trophytype[60] = 1;
	trophytype[61] = 1;
	trophytype[62] = 1;
	trophytype[63] = 1;
	trophytype[64] = 1;
	trophytype[65] = 1;
	trophytypesareset = true;
}

void ButtonActions(void)
{
	
	if (activateTrophyIncrement)
	{
		activateTrophyIncrement = false;
		if (TrophyNumber == 65)
		{
			TrophyNumber = 1;
		}
		else
		{
			TrophyNumber = TrophyNumber + 1;
		}
	}

	if (activateTrophyDecrement)
	{
		activateTrophyDecrement = false;
		if (TrophyNumber == 1)
		{
			TrophyNumber = 65;
		}
		else
		{
			TrophyNumber = TrophyNumber - 1;
		}
	}
	
	if (activateTrophy)
	{
		activateTrophy = false;
		if (HAS_ACHIEVEMENT_BEEN_PASSED(TrophyNumber))
		{
			activateYouDumbass = true;
		}
		else
		{
			AWARD_ACHIEVEMENT(TrophyNumber);
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Trophy unlocked.", 5000, 1);
		}
	}
	
	if (activatePlat)
	{
		activatePlat = false;
		if (HAS_ACHIEVEMENT_BEEN_PASSED(1) && HAS_ACHIEVEMENT_BEEN_PASSED(2) && HAS_ACHIEVEMENT_BEEN_PASSED(3) && HAS_ACHIEVEMENT_BEEN_PASSED(4) && HAS_ACHIEVEMENT_BEEN_PASSED(5) && HAS_ACHIEVEMENT_BEEN_PASSED(6) && HAS_ACHIEVEMENT_BEEN_PASSED(7) && HAS_ACHIEVEMENT_BEEN_PASSED(8) && HAS_ACHIEVEMENT_BEEN_PASSED(9) && HAS_ACHIEVEMENT_BEEN_PASSED(10) && HAS_ACHIEVEMENT_BEEN_PASSED(11) && HAS_ACHIEVEMENT_BEEN_PASSED(12) && HAS_ACHIEVEMENT_BEEN_PASSED(13) && HAS_ACHIEVEMENT_BEEN_PASSED(14) && HAS_ACHIEVEMENT_BEEN_PASSED(15) && HAS_ACHIEVEMENT_BEEN_PASSED(16) && HAS_ACHIEVEMENT_BEEN_PASSED(17) && HAS_ACHIEVEMENT_BEEN_PASSED(18) && HAS_ACHIEVEMENT_BEEN_PASSED(19) && HAS_ACHIEVEMENT_BEEN_PASSED(20) && HAS_ACHIEVEMENT_BEEN_PASSED(21) && HAS_ACHIEVEMENT_BEEN_PASSED(22) && HAS_ACHIEVEMENT_BEEN_PASSED(23) && HAS_ACHIEVEMENT_BEEN_PASSED(24) && HAS_ACHIEVEMENT_BEEN_PASSED(25) && HAS_ACHIEVEMENT_BEEN_PASSED(26) && HAS_ACHIEVEMENT_BEEN_PASSED(27) && HAS_ACHIEVEMENT_BEEN_PASSED(28) && HAS_ACHIEVEMENT_BEEN_PASSED(29) && HAS_ACHIEVEMENT_BEEN_PASSED(30) && HAS_ACHIEVEMENT_BEEN_PASSED(31) && HAS_ACHIEVEMENT_BEEN_PASSED(32) && HAS_ACHIEVEMENT_BEEN_PASSED(33) && HAS_ACHIEVEMENT_BEEN_PASSED(34) && HAS_ACHIEVEMENT_BEEN_PASSED(35) && HAS_ACHIEVEMENT_BEEN_PASSED(36) && HAS_ACHIEVEMENT_BEEN_PASSED(37) && HAS_ACHIEVEMENT_BEEN_PASSED(38) && HAS_ACHIEVEMENT_BEEN_PASSED(39) && HAS_ACHIEVEMENT_BEEN_PASSED(40) && HAS_ACHIEVEMENT_BEEN_PASSED(41) && HAS_ACHIEVEMENT_BEEN_PASSED(42) && HAS_ACHIEVEMENT_BEEN_PASSED(43) && HAS_ACHIEVEMENT_BEEN_PASSED(44) && HAS_ACHIEVEMENT_BEEN_PASSED(45) && HAS_ACHIEVEMENT_BEEN_PASSED(46) && HAS_ACHIEVEMENT_BEEN_PASSED(47) && HAS_ACHIEVEMENT_BEEN_PASSED(48) && HAS_ACHIEVEMENT_BEEN_PASSED(49) && HAS_ACHIEVEMENT_BEEN_PASSED(50) && HAS_ACHIEVEMENT_BEEN_PASSED(51) && HAS_ACHIEVEMENT_BEEN_PASSED(52) && HAS_ACHIEVEMENT_BEEN_PASSED(53) && HAS_ACHIEVEMENT_BEEN_PASSED(54) && HAS_ACHIEVEMENT_BEEN_PASSED(55) && HAS_ACHIEVEMENT_BEEN_PASSED(56) && HAS_ACHIEVEMENT_BEEN_PASSED(57) && HAS_ACHIEVEMENT_BEEN_PASSED(58) && HAS_ACHIEVEMENT_BEEN_PASSED(59) && HAS_ACHIEVEMENT_BEEN_PASSED(60) && HAS_ACHIEVEMENT_BEEN_PASSED(61) && HAS_ACHIEVEMENT_BEEN_PASSED(62) && HAS_ACHIEVEMENT_BEEN_PASSED(63) && HAS_ACHIEVEMENT_BEEN_PASSED(64) && HAS_ACHIEVEMENT_BEEN_PASSED(65))
		{
			activateAlreadyPlatBitch = true;
		}
		else
		{
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "All trophies unlocked, don't quit the game until they all pop.", 5000, 1);
			AWARD_ACHIEVEMENT(1);
			AWARD_ACHIEVEMENT(2);
			AWARD_ACHIEVEMENT(3);
			AWARD_ACHIEVEMENT(4);
			AWARD_ACHIEVEMENT(5);
			AWARD_ACHIEVEMENT(6);
			AWARD_ACHIEVEMENT(7);
			AWARD_ACHIEVEMENT(8);
			AWARD_ACHIEVEMENT(9);
			AWARD_ACHIEVEMENT(10);
			AWARD_ACHIEVEMENT(11);
			AWARD_ACHIEVEMENT(12);
			AWARD_ACHIEVEMENT(13);
			AWARD_ACHIEVEMENT(14);
			AWARD_ACHIEVEMENT(15);
			AWARD_ACHIEVEMENT(16);
			AWARD_ACHIEVEMENT(17);
			AWARD_ACHIEVEMENT(18);
			AWARD_ACHIEVEMENT(19);
			AWARD_ACHIEVEMENT(20);
			AWARD_ACHIEVEMENT(21);
			AWARD_ACHIEVEMENT(22);
			AWARD_ACHIEVEMENT(23);
			AWARD_ACHIEVEMENT(24);
			AWARD_ACHIEVEMENT(25);
			AWARD_ACHIEVEMENT(26);
			AWARD_ACHIEVEMENT(27);
			AWARD_ACHIEVEMENT(28);
			AWARD_ACHIEVEMENT(29);
			AWARD_ACHIEVEMENT(30);
			AWARD_ACHIEVEMENT(31);
			AWARD_ACHIEVEMENT(32);
			AWARD_ACHIEVEMENT(33);
			AWARD_ACHIEVEMENT(34);
			AWARD_ACHIEVEMENT(35);
			AWARD_ACHIEVEMENT(36);
			AWARD_ACHIEVEMENT(37);
			AWARD_ACHIEVEMENT(38);
			AWARD_ACHIEVEMENT(39);
			AWARD_ACHIEVEMENT(40);
			AWARD_ACHIEVEMENT(41);
			AWARD_ACHIEVEMENT(42);
			AWARD_ACHIEVEMENT(43);
			AWARD_ACHIEVEMENT(44);
			AWARD_ACHIEVEMENT(45);
			AWARD_ACHIEVEMENT(46);
			AWARD_ACHIEVEMENT(47);
			AWARD_ACHIEVEMENT(48);
			AWARD_ACHIEVEMENT(49);
			AWARD_ACHIEVEMENT(50);
			AWARD_ACHIEVEMENT(51);
			AWARD_ACHIEVEMENT(52);
			AWARD_ACHIEVEMENT(53);
			AWARD_ACHIEVEMENT(54);
			AWARD_ACHIEVEMENT(55);
			AWARD_ACHIEVEMENT(56);
			AWARD_ACHIEVEMENT(57);
			AWARD_ACHIEVEMENT(58);
			AWARD_ACHIEVEMENT(59);
			AWARD_ACHIEVEMENT(60);
			AWARD_ACHIEVEMENT(61);
			AWARD_ACHIEVEMENT(62);
			AWARD_ACHIEVEMENT(63);
			AWARD_ACHIEVEMENT(64);
			AWARD_ACHIEVEMENT(65);
		}
	}
	
	if (activateYouDumbass)
	{
		activateYouDumbass = false;
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "You already have that trophy dumbass!", 5000, 1);
		WAIT(3000);
		START_CHAR_FIRE(GetPlayerPed());
	}
	
	if (activateAlreadyPlatBitch)
	{
		activateAlreadyPlatBitch = false;
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "You already have platinum bitch!!", 5000, 1);
		WAIT(3000);
		GET_CHAR_COORDINATES(GetPlayerPed(), &ppx, &ppy, &ppz);
		ADD_EXPLOSION(ppx, ppy, ppz, 0, 8.00000000, 1, 0, 0.00000000);
	}

}

void ButtonInput(void)
{


	if (IS_BUTTON_PRESSED(0, L2) && IS_BUTTON_PRESSED(0, L1) && IS_BUTTON_JUST_PRESSED(0, DPAD_UP))
	{
		activateTrophyIncrement = true;
	}

	if (IS_BUTTON_PRESSED(0, L2) && IS_BUTTON_PRESSED(0, L1) && IS_BUTTON_JUST_PRESSED(0, DPAD_DOWN))
	{
		activateTrophyDecrement = true;
	}

	if (IS_BUTTON_PRESSED(0, L2) && IS_BUTTON_PRESSED(0, L1) && IS_BUTTON_JUST_PRESSED(0, X))
	{
		activateTrophy = true;
	}

	if (IS_BUTTON_PRESSED(0, L2) && IS_BUTTON_PRESSED(0, L1) && IS_BUTTON_JUST_PRESSED(0, STICK_R))
	{
		activatePlat = true;
	}

}

void main(void)
{
	while(true)
	{
		WAIT(0);
		if (trophynamesareset == false)
		{
			SetTrophyNames();
		}
		if (trophytypesareset == false)
		{
			SetTrophyTypes();
		}
		ButtonInput();
		ButtonActions();
		DrawRect();
		DrawTrophyNum();	
		DrawTrophyStatus();
		DrawTrophyName();
	}
}