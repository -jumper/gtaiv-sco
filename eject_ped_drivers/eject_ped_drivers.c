/* To compile this you need to add the following line to
your scocl\bin\nativeO.cfg if it does not already exist:
SET_PED_FORCE_FLY_THROUGH_WINDSCREEN=$f7fdd0f1
******** Controls: Hold L2 and tap L3 to toggle ********/
#include <natives.h>
#include <common.h>
#include <strings.h>
#include <types.h>
#include <consts.h>

/********************** PS3 CONTROLLER BUTTON MAP **********************/
#define L1         0x4
#define L2         0x5
#define R1         0x6
#define R2         0x7
#define DPAD_UP    0x8
#define DPAD_DOWN  0x9
#define DPAD_LEFT  0xA
#define DPAD_RIGHT 0xB
#define START      0xC
#define SELECT     0xD
#define SQUARE     0xE
#define TRIANGLE   0xF
#define X          0x10
#define CIRCLE     0x11
#define STICK_L    0x12  // L3
#define STICK_R    0x13  // R3
/***********************************************************************/

float ppedX, ppedY, ppedZ, cnX[5], cnY[5], cnZ[5];
Ped ccd[5], ccp0[5], ccp1[5], ccp2[5];
Vehicle cc[5];
int EPDActive = 0;

void EjectPedDrivers(void)
{
	int CN;
	for (CN = 0; CN <= 4; CN++)
	{
		GET_CHAR_COORDINATES(GetPlayerPed(),&ppedX, &ppedY, &ppedZ);
		GET_NTH_CLOSEST_CAR_NODE(ppedX, ppedY, ppedZ, CN * 2, &cnX[CN], &cnY[CN], &cnZ[CN]);
		cc[CN] = GET_CLOSEST_CAR(cnX[CN], cnY[CN], cnZ[CN], 20, false, 70);
		if (DOES_VEHICLE_EXIST(cc[CN]))
		{
			GET_DRIVER_OF_CAR(cc[CN], &ccd[CN]);
			if (DOES_CHAR_EXIST(ccd[CN]))
			{
				GET_CHAR_IN_CAR_PASSENGER_SEAT(cc[CN], 0, &ccp0[CN]);
				GET_CHAR_IN_CAR_PASSENGER_SEAT(cc[CN], 1, &ccp1[CN]);
				GET_CHAR_IN_CAR_PASSENGER_SEAT(cc[CN], 2, &ccp2[CN]);
				SET_PED_FORCE_FLY_THROUGH_WINDSCREEN(ccd[CN], 1);
				WAIT(50);
				APPLY_FORCE_TO_PED(ccd[CN], 1, 0.0, 8.0, 2.0, 0.0, 0.0, 0.0, 1, 1, 1, 1);
				if (DOES_CHAR_EXIST(ccp0[CN]))
				{
					SET_PED_FORCE_FLY_THROUGH_WINDSCREEN(ccp0[CN], 1);
					WAIT(50);
					APPLY_FORCE_TO_PED(ccp0[CN], 1, 0.0, 8.0, 2.0, 0.0, 0.0, 0.0, 1, 1, 1, 1);
				}
				if (DOES_CHAR_EXIST(ccp1[CN]))
				{
					SET_PED_FORCE_FLY_THROUGH_WINDSCREEN(ccp1[CN], 1);
					WAIT(50);
					APPLY_FORCE_TO_PED(ccp1[CN], 1, 0.0, 8.0, 2.0, 0.0, 0.0, 0.0, 1, 1, 1, 1);
				}
				if (DOES_CHAR_EXIST(ccp2[CN]))
				{
					SET_PED_FORCE_FLY_THROUGH_WINDSCREEN(ccp2[CN], 1);
					WAIT(50);
					APPLY_FORCE_TO_PED(ccp2[CN], 1, 0.0, 8.0, 2.0, 0.0, 0.0, 0.0, 1, 1, 1, 1);
				}
			}
		}
	}
}

void main(void)
{
	while(true)
	{
		WAIT(0);
		if (IS_BUTTON_PRESSED(0, L2) && IS_BUTTON_JUST_PRESSED(0, STICK_L))
		{
			if (EPDActive == 0)
			{
				EPDActive = 1;
				PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Eject Ped Drivers ON", 5000, 1);
			}
			else
			{
				EPDActive = 0;
				PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Eject Ped Drivers OFF", 5000, 1);
			}
		}
		if (EPDActive == 1)
		{
			EjectPedDrivers();
		}
	}
}