#include <natives.h>
#include <common.h>
#include <strings.h>
#include <types.h>
#include <consts.h>
#include <ps3pad.h>

void main(void)
{
	while(true)
	{
		WAIT(0);
		if (IS_CHAR_IN_ANY_CAR(GetPlayerPed()))
		{
			if (IS_BUTTON_PRESSED(0,R2) && IS_BUTTON_JUST_PRESSED(0,STICK_L))
			{
				SET_PED_FORCE_FLY_THROUGH_WINDSCREEN(GetPlayerPed(), 1);
			}
		}
	}
}