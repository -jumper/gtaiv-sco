/*********************
* ADJUSTABLE FLY MOD *
*********************/
#include <natives.h>
#include <common.h>
#include <strings.h>
#include <types.h>
#include <consts.h>

#include "Functions.c"// from threesocks button input example

/********************** PS3 CONTROLLER BUTTON MAP **********************/
#define L1         0x4
#define L2         0x5
#define R1         0x6
#define R2         0x7
#define DPAD_UP    0x8
#define DPAD_DOWN  0x9
#define DPAD_LEFT  0xA
#define DPAD_RIGHT 0xB
#define START      0xC
#define SELECT     0xD
#define SQUARE     0xE
#define TRIANGLE   0xF
#define X          0x10
#define CIRCLE     0x11
#define STICK_L    0x12  //L3
#define STICK_R    0x13  //R3
/***********************************************************************/

int activateFlyMod = false;
float pax, pay, paz;// player coords old
float pbx, pby, pbz;// player coords new
float fincr = 0.01000000;// distance to move for each loop
Vehicle PlayerCar;

void DoActivators(void)
{
	DRAW_RECT(0.17578125, 0.05694443, 0.26712500, 0.03888889, 255, 0, 255, 150);
	DRAW_RECT(0.17578125, 0.05694443, 0.26400000, 0.03333333, 0, 0, 0, 150);
	set_up_draw(2, 0.30000000, 0.30000000, 255, 255, 0, 210);
	draw_float("NUMBR", 0.25000000, 0.05000000, fincr);
	set_up_draw(2, 0.30000000, 0.30000000, 255, 255, 255, 210);
	draw_text("STRING", 0.05000000, 0.05000000, "FlyMod                  Speed");
	if (activateFlyMod)
	{
		set_up_draw(2, 0.30000000, 0.30000000, 10, 255, 10, 210);
		draw_text("STRING", 0.11600000, 0.05000000, "ON");
		if (IS_CHAR_IN_ANY_CAR(GetPlayerPed()))
		{
			GET_CAR_CHAR_IS_USING(GetPlayerPed(), &PlayerCar);
			GET_CAR_COORDINATES(PlayerCar, &pax, &pay, &paz);
		}
		if (!IS_CHAR_IN_ANY_CAR(GetPlayerPed()))
		{
			GET_CHAR_COORDINATES(GetPlayerPed(), &pax, &pay, &paz);
		}
		pbx = pax;
		pby = pay;
		pbz = paz;
		if (IS_BUTTON_PRESSED(0, DPAD_RIGHT))
		{
			pbx = pax + fincr;// move east
		}
		if (IS_BUTTON_PRESSED(0, DPAD_LEFT))
		{
			pbx = pax - fincr;// move west
		}
		if (IS_BUTTON_PRESSED(0, DPAD_UP))
		{
			pby = pay + fincr;// move north
		}
		if (IS_BUTTON_PRESSED(0, DPAD_DOWN))
		{
			pby = pay - fincr;// move south
		}
		if (IS_BUTTON_PRESSED(0, R1))
		{
			pbz = paz + (fincr / 2.0);// move up - distance halved seems better
		}
		if (IS_BUTTON_PRESSED(0, L1))
		{
			pbz = paz - (fincr / 2.0);// move down - distance halved seems better
		}
		if (!IS_BUTTON_PRESSED(0, STICK_R) && IS_BUTTON_PRESSED(0, R2))
		{
			fincr = fincr + 0.01000000;// increase distance per loop
		}
		if (!IS_BUTTON_PRESSED(0, STICK_R) && IS_BUTTON_PRESSED(0, L2) && (fincr > 0.01000000))
		{
			fincr = fincr - 0.01000000;// decrease distance per loop
		}
		if (IS_BUTTON_PRESSED(0, STICK_R) && IS_BUTTON_PRESSED(0, R2))
		{
			fincr = fincr + 0.10000000;// increase distance per loop large amount
		}
		if (IS_BUTTON_PRESSED(0, STICK_R) && IS_BUTTON_PRESSED(0, L2) && (fincr > 0.10000000))
		{
			fincr = fincr - 0.10000000;// decrease distance per loop large amount
		}
		if (IS_CHAR_IN_ANY_CAR(GetPlayerPed()))
		{
			SET_CAR_COORDINATES_NO_OFFSET(PlayerCar, pbx, pby, pbz);
		}
		if (!IS_CHAR_IN_ANY_CAR(GetPlayerPed()))
		{
			pbz = pbz + 0.00450000;// no idea why a slight change in the z coord happens but this is to fix that, its still kind of screwy
			SET_CHAR_COORDINATES_NO_OFFSET(GetPlayerPed(), pbx, pby, pbz);
		}
	}
	else
	{
		set_up_draw(2, 0.30000000, 0.30000000, 255, 10, 10, 210);
		draw_text("STRING", 0.11600000, 0.05000000, "OFF");
	}

}

void ButtonInput(void)
{

	if (IS_BUTTON_PRESSED(0, STICK_R) && IS_BUTTON_JUST_PRESSED(0, X))
	{
		activateFlyMod = true;
//		SET_PLAYER_CONTROL(GetPlayerIndex(), false);// disables player control
	}
	if (IS_BUTTON_PRESSED(0, STICK_R) && IS_BUTTON_JUST_PRESSED(0, CIRCLE))
	{
		activateFlyMod = false;
//		SET_PLAYER_CONTROL(GetPlayerIndex(), true);// reenables player control
	}

}

void main(void)
{
	while(true)
	{
		WAIT(0);
		ButtonInput();
		DoActivators();
	}
}