/*********************
*   ANALOG FLY MOD   *
*********************/
#include <natives.h>
#include <common.h>
#include <strings.h>
#include <types.h>
#include <consts.h>
/********************** PS3 CONTROLLER BUTTON MAP **********************/
#define L1         0x4
#define L2         0x5
#define R1         0x6
#define R2         0x7
#define DPAD_UP    0x8
#define DPAD_DOWN  0x9
#define DPAD_LEFT  0xA
#define DPAD_RIGHT 0xB
#define START      0xC
#define SELECT     0xD
#define SQUARE     0xE
#define TRIANGLE   0xF
#define X          0x10
#define CIRCLE     0x11
#define STICK_L    0x12  //L3
#define STICK_R    0x13  //R3
/***********************************************************************/
int activateFlyMod = false;
int laX, laY, raX, raY;// analog sticks
float paX, paY, paZ;// player coords old
float pbX, pbY, pbZ;// player coords new
float gcrotX, gcrotY, gcrotZ;// game cam rotation (Y is not used)
float fincr;// distance to move (set by analog)
float pbT;// weird trig stuffs
Vehicle PlayerCar;
Camera game_cam;

void MainLoop(void)
{
	if (activateFlyMod)
	{
		GET_GAME_CAM(&game_cam);
		if (IS_CAM_ACTIVE(game_cam))
		{
			GET_CAM_ROT(game_cam, &gcrotX, &gcrotY, &gcrotZ);
			if (IS_CHAR_IN_ANY_CAR(GetPlayerPed()))
			{
				GET_CAR_CHAR_IS_USING(GetPlayerPed(), &PlayerCar);
				GET_CAR_COORDINATES(PlayerCar, &paX, &paY, &paZ);
			}
			if (!IS_CHAR_IN_ANY_CAR(GetPlayerPed()))
			{
				GET_CHAR_COORDINATES(GetPlayerPed(), &paX, &paY, &paZ);
			}
			GET_POSITION_OF_ANALOGUE_STICKS(0, &laX, &laY, &raX, &raY);
			fincr = (laY * laY) * 0.0001;
			if (IS_BUTTON_PRESSED(0, R1))
			{
				fincr = fincr * 4.0;
				PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Boost mode.", 100, 1);
			}
			pbT = (fincr * COS(gcrotX));
			if (laY < 0)
			{
				pbX = paX - (pbT * SIN(gcrotZ));
				pbY = paY + (pbT * COS(gcrotZ));
				pbZ = paZ + (fincr * SIN(gcrotX));
			}
			if (laY >= 0)
			{
				pbX = paX + (pbT * SIN(gcrotZ));
				pbY = paY - (pbT * COS(gcrotZ));
				pbZ = paZ - (fincr * SIN(gcrotX));
			}
			if (IS_BUTTON_PRESSED(0, L2))
			{
				pbZ = pbZ - fincr;
			}
			if (IS_BUTTON_PRESSED(0, R2))
			{
				pbZ = pbZ + fincr;
			}
			if (IS_CHAR_IN_ANY_CAR(GetPlayerPed()))
			{
				SET_CAR_COORDINATES_NO_OFFSET(PlayerCar, pbX, pbY, pbZ);
			}
			if (!IS_CHAR_IN_ANY_CAR(GetPlayerPed()))
			{
				pbZ = pbZ + 0.00450000;// no idea why a slight change in the z coord happens but this is to fix that, its still kind of screwy
				SET_CHAR_COORDINATES_NO_OFFSET(GetPlayerPed(), pbX, pbY, pbZ);
			}
		}
	}

}

void ButtonInput(void)
{

	if (IS_BUTTON_PRESSED(0, X) && IS_BUTTON_JUST_PRESSED(0, STICK_L))
	{
		if (activateFlyMod == true)
		{
			activateFlyMod = false;
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Fly mod disabled.", 5000, 1);
		}
		else
		{
			activateFlyMod = true;
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Fly mod enabled.", 5000, 1);
		}
	}

}

void main(void)
{
	while(true)
	{
		WAIT(0);
		ButtonInput();
		MainLoop();
	}
}