#include <natives.h>
#include <common.h>
#include <strings.h>
#include <types.h>
#include <consts.h>
#include <ps3pad.h>

int FlymodActive = 0;
int laX, laY, raX, raY;												// for storing analog stick data
float movBoost;														// for storing the boost amount
float movH, movP;													// heading and pitch
float movT;															// weird trig stuff
float movX, movY, movZ;												// coords for player or vehicle
Vehicle movV;

void Flymod(void)
{
	if (IS_CHAR_IN_ANY_CAR(GetPlayerPed()))							// get start coords and heading (if in vehicle)
	{
		GET_CAR_CHAR_IS_USING(GetPlayerPed(), &movV);
		GET_CAR_COORDINATES(movV, &movX, &movY, &movZ);
		GET_CAR_HEADING(movV, &movH);
	}
	if (!IS_CHAR_IN_ANY_CAR(GetPlayerPed()))						// get start coords and heading (if on foot)
	{
		GET_CHAR_COORDINATES(GetPlayerPed(), &movX, &movY, &movZ);
		GET_CHAR_HEADING(GetPlayerPed(), &movH);
	}
	GET_POSITION_OF_ANALOGUE_STICKS(0, &laX, &laY, &raX, &raY);		// get analog stick data
	movH = movH - ((TO_FLOAT(raX)) / 30.0f);						// adjusts heading
	if (movH > 360.0f)	{	movH = movH - 360.0f;	}				// keeps heading looped between 0 and 360
	if (movH < 0.0f)	{	movH = movH + 360.0f;	}				// keeps heading looped between 0 and 360
	movP = movP - ((TO_FLOAT(raY)) / 30.0f);						// adjusts pitch
	if (movP < -75.0f)	{	movP = -75.0f;	}						// keeps pitch from going below -75
	if (movP > 75.0f)	{	movP = 75.0f;	}						// keeps pitch from going above 75
	movBoost = 1.0f;												// no boost (needs to be 1 cause we multiply by it)
	if (IS_BUTTON_PRESSED(0,R1))
	{
		movBoost = 4.0f;											// boost
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Boost mode", 100, 1);
	}
	if (IS_BUTTON_PRESSED(0,STICK_R))	{	movP = 0.0f;	}		// resets pitch to 0
	movT = (TO_FLOAT(laY) / 75.0f) * COS(movP) * movBoost;								// uses the pitch angle and the forward increment as a hypotenuse to calulate the adjacent side length to be used below
	movX = movX + ((movT * SIN(movH)) + ((TO_FLOAT(laX) / 75.0f) * COS(movH) * movBoost));																			// calculates how far to move along the maps X axis
	movY = movY - ((movT * COS(movH)) - ((TO_FLOAT(laX) / 75.0f) * SIN(movH) * movBoost));																			// calculates how far to move along the maps Y axis
	movZ = movZ - ((TO_FLOAT(laY) / 75.0f) * SIN(movP) * movBoost) - (TO_FLOAT(GET_CONTROL_VALUE(0, 6)) / 400.0f) + (TO_FLOAT(GET_CONTROL_VALUE(0, 5)) / 400.0f);	// calculates how far to move along the maps Z axis - includes input from L2 and R2
	if (IS_CHAR_IN_ANY_CAR(GetPlayerPed()))							// set finish coords and heading (if in vehicle)
	{
		GET_CAR_CHAR_IS_USING(GetPlayerPed(), &movV);
		SET_CAR_COORDINATES_NO_OFFSET(movV, movX, movY, movZ);
		SET_CAR_HEADING(movV, movH);
	}
	if (!IS_CHAR_IN_ANY_CAR(GetPlayerPed()))						// set finish coords and heading (if on foot)
	{
		SET_CHAR_COORDINATES_NO_OFFSET(GetPlayerPed(), movX, movY, movZ);
		SET_CHAR_HEADING(GetPlayerPed(), movH);
	}
}

void ButtonInput(void)
{
	if (IS_BUTTON_PRESSED(0,SQUARE) && IS_BUTTON_JUST_PRESSED(0,L1))
	{
		if (FlymodActive == 0)
		{
			FlymodActive = 1;
			movP = 0.0f;
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Fly Mod ON", 5000, 1);
		}
		else
		{
			FlymodActive = 0;
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Fly Mod OFF", 5000, 1);
		}
	}
}

void main(void)
{
	while(true)
	{
		WAIT(0);
		if (FlymodActive == 1)
		{
			Flymod();
		}
		ButtonInput();
	}
}