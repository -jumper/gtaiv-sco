#include <natives.h>
#include <common.h>
#include <strings.h>
#include <types.h>
#include <consts.h>
#include <ps3pad.h>

int FlymodActive = 0;
int laX, laY, raX, raY;												// position of analog sticks
float movS;															// movement scale (boost / slow modes)
float movH, movP;													// movement heading and pitch
float movT;															// weird trig stuff
float movX, movY, movZ;												// movement coordinates
Vehicle movV;

void Flymod(void)
{
	if (IS_CHAR_IN_ANY_CAR(GetPlayerPed()))							// get start coords and heading (if in vehicle)
	{
		GET_CAR_CHAR_IS_USING(GetPlayerPed(), &movV);
		GET_CAR_COORDINATES(movV, &movX, &movY, &movZ);
		GET_CAR_HEADING(movV, &movH);
	}
	if (!IS_CHAR_IN_ANY_CAR(GetPlayerPed()))						// get start coords and heading (if on foot)
	{
		GET_CHAR_COORDINATES(GetPlayerPed(), &movX, &movY, &movZ);
		GET_CHAR_HEADING(GetPlayerPed(), &movH);
	}
	
	GET_POSITION_OF_ANALOGUE_STICKS(0, &laX, &laY, &raX, &raY);		// get analog stick data
	
	movH = movH - (raX * 0.025f);									// adjusts heading
	if (movH > 360.0f)	{	movH = movH - 360.0f;	}				// keeps heading looped between 0 and 360
	if (movH < 0.0f)	{	movH = movH + 360.0f;	}				// keeps heading looped between 0 and 360
	
	movP = movP - (raY * 0.025f);									// adjusts pitch
	if (movP < -75.0f)	{	movP = -75.0f;	}						// keeps pitch from going below -75
	if (movP > 75.0f)	{	movP = 75.0f;	}						// keeps pitch from going above 75
	
	movS = 1.0f;													// no boost (normal scale)
	if (IS_BUTTON_PRESSED(0,R1) && !IS_BUTTON_PRESSED(0,L1))
	{
		movS = 4.0f;												// boost (4x scale)
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Boost mode", 100, 1);
	}
	if (IS_BUTTON_PRESSED(0,L1) && !IS_BUTTON_PRESSED(0,R1))
	{
		movS = 0.25f;												// slow (.25x scale)
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Slow mode", 100, 1);
	}
	
	if (IS_BUTTON_JUST_PRESSED(0,STICK_R))	{	movP = 0.0f;	}	// resets pitch to 0
	
	/*			This first var is a little trick to allow calculating XY movement in 2D - multiplying the initial		*/
	/*			target distance by the cosine of the pitch angle will give the length of the adjacent side,				*/
	/*			that result will be the new target distance to move in XY space											*/
	movT = (laY * 0.0133f) * COS(movP);
	/*			Next two lines calculate XY movement, and the combination gets multiplied by the scale					*/
	movX = movX + (movS * ((movT * SIN(movH)) + (laX * 0.0133f) * COS(movH)));
	movY = movY - (movS * ((movT * COS(movH)) - (laX * 0.0133f) * SIN(movH)));
	/*	Last line calculates Z movement and includes input from L2 and R2, and again the combo gets multiplied by scale	*/
	movZ = movZ - (movS * (((laY * 0.0133f) * SIN(movP)) + (GET_CONTROL_VALUE(0, 6) * 0.0025f) - (GET_CONTROL_VALUE(0, 5) * 0.0025f)));
	
	if (IS_CHAR_IN_ANY_CAR(GetPlayerPed()))							// set finish coords and heading (if in vehicle)
	{
		SET_CAR_COORDINATES_NO_OFFSET(movV, movX, movY, movZ);
		SET_CAR_HEADING(movV, movH);
	}
	if (!IS_CHAR_IN_ANY_CAR(GetPlayerPed()))						// set finish coords and heading (if on foot)
	{
		SET_CHAR_COORDINATES_NO_OFFSET(GetPlayerPed(), movX, movY, movZ);
		SET_CHAR_HEADING(GetPlayerPed(), movH);
	}
}

void ButtonInput(void)
{
	if (IS_BUTTON_PRESSED(0,SQUARE) && IS_BUTTON_JUST_PRESSED(0,STICK_L))
	{
		if (FlymodActive == 0)
		{
			FlymodActive = 1;
			movP = 0.0f;
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Fly Mod ON", 5000, 1);
		}
		else
		{
			FlymodActive = 0;
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Fly Mod OFF", 5000, 1);
		}
	}
}

void main(void)
{
	while(true)
	{
		WAIT(0);
		if (FlymodActive == 1)
		{
			Flymod();
		}
		ButtonInput();
	}
}