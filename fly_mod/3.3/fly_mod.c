/*
    fly mod 3.3
                */
#include <natives.h>
#include <common.h>
#include <strings.h>
#include <types.h>
#include <consts.h>
#include <ps3pad.h>

#define maxPitch 70.0
#define minPitch -70.0

bool FlymodActive;
Camera FlymodCam;
float movH, movP;

void ModeText(char *string)
{
    SET_TEXT_FONT(0);
    SET_TEXT_BACKGROUND(0);
    SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255);
    SET_TEXT_EDGE(1, 0, 0, 0, 255);
    SET_TEXT_SCALE(0.35, 0.55);
    SET_TEXT_PROPORTIONAL(1);
    SET_TEXT_CENTRE(1);
    DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.88, "STRING", string);
}

void CreateCam(bool create)
{
    if (create)
    {
        CREATE_CAM(14, &FlymodCam);
        SET_CAM_ACTIVE(FlymodCam, true);
        SET_CAM_PROPAGATE(FlymodCam, true);
        ACTIVATE_SCRIPTED_CAMS(true, true);
        ATTACH_CAM_TO_PED(FlymodCam, GetPlayerPed());
        POINT_CAM_AT_PED(FlymodCam, GetPlayerPed());
    }
    else
    {
        SET_CAM_ACTIVE(FlymodCam, 0);
        SET_CAM_PROPAGATE(FlymodCam, 0);
        ACTIVATE_SCRIPTED_CAMS(0, 0);
        UNATTACH_CAM(FlymodCam);
        DESTROY_CAM(FlymodCam);
        while (DOES_CAM_EXIST(FlymodCam))
            WAIT(0);
        SET_CAM_BEHIND_PED(GetPlayerPed());
    }
}

void Flymod(void)
{
    int laX, laY, raX, raY;
    float movS, movT, movX, movY, movZ, camDist;
    Vehicle movV;

    if (DOES_CAM_EXIST(FlymodCam))
    {
        // get coords and heading
        if (IS_CHAR_IN_ANY_CAR(GetPlayerPed()))
        {
            uint carmodel;
            Vector3 minDims, maxDims;
            
            GET_CAR_CHAR_IS_USING(GetPlayerPed(), &movV);
            GET_CAR_MODEL(movV, &carmodel);
            GET_MODEL_DIMENSIONS(carmodel, &minDims, &maxDims);
            camDist = 1.5 * (maxDims.y - minDims.y);
            GET_CAR_COORDINATES(movV, &movX, &movY, &movZ);
            GET_CAR_HEADING(movV, &movH);
        }
        else
        {
            GET_CHAR_COORDINATES(GetPlayerPed(), &movX, &movY, &movZ);
            GET_CHAR_HEADING(GetPlayerPed(), &movH);
            camDist = 4.0;
        }
        
        // get analog stick data
        GET_POSITION_OF_ANALOGUE_STICKS(0, &laX, &laY, &raX, &raY);
        
        // heading control
        movH = movH - (raX * 0.025f);
        if (movH > 360.0f)
            movH = movH - 360.0f;
        if (movH < 0.0f)
            movH = movH + 360.0f;
        
        // pitch control
        if (IS_LOOK_INVERTED())
            movP = movP + (raY * 0.025f);
        else
            movP = movP - (raY * 0.025f);
        if (movP < minPitch)
            movP = minPitch;
        if (movP > maxPitch)
            movP = maxPitch;
        
        // speed scale control
        movS = 1.0f;
        if (IS_BUTTON_PRESSED(0,R1) && !IS_BUTTON_PRESSED(0,L1))
        {
            movS = 4.0f;
            ModeText("Boost mode");
        }
        if (IS_BUTTON_PRESSED(0,L1) && !IS_BUTTON_PRESSED(0,R1))
        {
            movS = 0.25f;
            ModeText("Slow mode");
        }
        
        if (IS_BUTTON_JUST_PRESSED(0,STICK_R))    {    movP = 0.0f;    }    // resets pitch to 0
        
        // movement logic
        /*            This first var is a little trick to allow calculating XY movement in 2D - multiplying the initial        */
        /*            target distance by the cosine of the pitch angle will give the length of the adjacent side,                */
        /*            that result will be the new target distance to move in XY space                                            */
        movT = (laY * 0.0133f) * COS(movP);
        /*            Next two lines calculate XY movement, and the combination gets multiplied by the scale                    */
        movX = movX + (movS * ((movT * SIN(movH)) + (laX * 0.0133f) * COS(movH)));
        movY = movY - (movS * ((movT * COS(movH)) - (laX * 0.0133f) * SIN(movH)));
        /*    Last line calculates Z movement and includes input from L2 and R2, and again the combo gets multiplied by scale    */
        movZ = movZ - (movS * (((laY * 0.0133f) * SIN(movP)) + (PSenseL2() * 0.0025f) - (PSenseR2() * 0.0025f)));
        
        // set coords and heading
        if (IS_CHAR_IN_ANY_CAR(GetPlayerPed()))
        {
            SET_CAR_COORDINATES_NO_OFFSET(movV, movX, movY, movZ);
            SET_CAR_HEADING(movV, movH);
        }
        if (!IS_CHAR_IN_ANY_CAR(GetPlayerPed()))
        {
            SET_CHAR_COORDINATES_NO_OFFSET(GetPlayerPed(), movX, movY, movZ);
            SET_CHAR_HEADING(GetPlayerPed(), movH);
        }
            
        // update custom cam (yay more trig)
        float cam_off_X, cam_off_Y, cam_off_Z, cam_off_T;
        
        cam_off_T = (camDist * COS(movP - 18.0f));
        cam_off_X = cam_off_X + (cam_off_T * SIN(movH));
        cam_off_Y = cam_off_Y - (cam_off_T * COS(movH));
        cam_off_Z = cam_off_Z - (camDist * SIN(movP - 18.0f));
        
        SET_CAM_ATTACH_OFFSET(FlymodCam, cam_off_X, cam_off_Y, cam_off_Z);
        POINT_CAM_AT_PED(FlymodCam, GetPlayerPed());
    }
    else
    {
        CreateCam(true);
    }
}

void main(void)
{
    while(true)
    {
        WAIT(0);
        if (FlymodActive)
        {
            Flymod();
        }
        else
        {
            if (DOES_CAM_EXIST(FlymodCam))
            {
                CreateCam(false);
            }
        }
        if (IS_BUTTON_PRESSED(0,SQUARE) && IS_BUTTON_JUST_PRESSED(0,STICK_L))
        {
            if (FlymodActive)
            {
                FlymodActive = false;
                PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Fly Mod OFF", 3000, 1);
            }
            else
            {
                FlymodActive = true;
                movP = 0.0;
                PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Fly Mod ON", 3000, 1);
            }
            FREEZE_CHAR_POSITION(GetPlayerPed(), FlymodActive);
        }
    }
}