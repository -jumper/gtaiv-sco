#include <natives.h>
#include <common.h>
#include <strings.h>
#include <types.h>
#include <consts.h>
#include <ps3pad.h>

float flt[6];
int selParam = 0;
int doAdjustObject = 0;
int selDigit = 0;
int doHashInput = 0;
Object TestObj;
Vehicle TestVeh;
int TestVehVis = 1;
char* hashChar[8];
uint hashInt[8];
uint testModel;

void SpawnObject(void)
{
	float tcarX, tcarY, tcarZ;
	
	if (DOES_VEHICLE_EXIST(TestVeh))
	{
		GET_CAR_COORDINATES(TestVeh, &tcarX, &tcarY, &tcarZ);
		
		if (IS_MODEL_IN_CDIMAGE(testModel))
		{
			if (!IS_THIS_MODEL_A_VEHICLE(testModel))
			{
				CREATE_OBJECT(testModel, tcarX, tcarY, tcarZ + 10.0f, &TestObj, 1);
				while (!DOES_OBJECT_EXIST(TestObj))
				{
					WAIT(0);
				}
				FREEZE_OBJECT_POSITION(TestObj, 1);
				SET_OBJECT_VISIBLE(TestObj, 1);
				SET_OBJECT_COLLISION(TestObj, 0);
				ATTACH_OBJECT_TO_CAR(TestObj, TestVeh, 0, flt[1], flt[2], flt[3], flt[4], flt[5], flt[6]);
				doAdjustObject = 1;
			}
			else
			{
				PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "~y~Error! ~s~Vehicle models disabled", 5000, 1);
				doHashInput = 1;
			}
		}
		else
		{
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "~y~Hash Error! ~s~Model not in cdimage", 5000, 1);
			doHashInput = 1;
		}
	}
	else
	{
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "You need to have a car", 5000, 1);
	}
}

void KillObject(void)
{
	float obX, obY, obZ;
	
	if (DOES_OBJECT_EXIST(TestObj))
	{
		DETACH_OBJECT(TestObj, 1);
		GET_OBJECT_COORDINATES(TestObj, &obX, &obY, &obZ);
		MARK_OBJECT_AS_NO_LONGER_NEEDED(&TestObj);
		CLEAR_AREA_OF_OBJECTS(obX, obY, obZ, 5.0f);
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Last object deleted", 5000, 1);
	}
	else
	{
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "No object to delete", 5000, 1);
	}
}

void HashInput(void)
{
	int I;
	
	DRAW_CURVED_WINDOW(0.034f, 0.032f, 0.147f, 0.057f, 200);
	
	SET_TEXT_DROPSHADOW(1, 0, 0, 0, 255);
	SET_TEXT_SCALE(0.3f, 0.3f);
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.05f, 0.05f, "STRING", "0x");
	
	for (I = 0; I < 8; I++)
	{
		if (hashInt[I] == 0)	{	hashChar[I] = "0";	}
		if (hashInt[I] == 1)	{	hashChar[I] = "1";	}
		if (hashInt[I] == 2)	{	hashChar[I] = "2";	}
		if (hashInt[I] == 3)	{	hashChar[I] = "3";	}
		if (hashInt[I] == 4)	{	hashChar[I] = "4";	}
		if (hashInt[I] == 5)	{	hashChar[I] = "5";	}
		if (hashInt[I] == 6)	{	hashChar[I] = "6";	}
		if (hashInt[I] == 7)	{	hashChar[I] = "7";	}
		if (hashInt[I] == 8)	{	hashChar[I] = "8";	}
		if (hashInt[I] == 9)	{	hashChar[I] = "9";	}
		if (hashInt[I] == 10)	{	hashChar[I] = "A";	}
		if (hashInt[I] == 11)	{	hashChar[I] = "B";	}
		if (hashInt[I] == 12)	{	hashChar[I] = "C";	}
		if (hashInt[I] == 13)	{	hashChar[I] = "D";	}
		if (hashInt[I] == 14)	{	hashChar[I] = "E";	}
		if (hashInt[I] == 15)	{	hashChar[I] = "F";	}
		SET_TEXT_DROPSHADOW(1, 0, 0, 0, 255);
		SET_TEXT_SCALE(0.3f, 0.3f);
		if (I == selDigit)
		{
			SET_TEXT_COLOUR(255, 255, 0, 255);
		}
		else
		{
			SET_TEXT_COLOUR(255, 255, 255, 255);
		}
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.07f + (I * 0.012f), 0.05f, "STRING", hashChar[I]);		
	}
	
	SET_TEXT_DROPSHADOW(1, 0, 0, 0, 255);
	SET_TEXT_SCALE(0.3f, 0.3f);
	SET_TEXT_COLOUR(255, 255, 0, 255);
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.072f + (selDigit * 0.012f), 0.06f, "STRING", "-");
}

void AdjustObject(void)
{
	int laX, laY, raX, raY;
	int I;
	
	if (DOES_OBJECT_EXIST(TestObj))
	{
		DETACH_OBJECT(TestObj, 1);
		DRAW_CURVED_WINDOW(0.034f, 0.032f, 0.09f, 0.153f, 200);
	
		GET_POSITION_OF_ANALOGUE_STICKS(0, &laX, &laY, &raX, &raY);
		if (laX > 120 || laX < -120)
		{
			flt[selParam] = flt[selParam] + (laX * 0.001f);
		}
		else
		{
			flt[selParam] = flt[selParam] + (laX * 0.00025f);
		}
		for (I = 0; I < 6; I++)
		{
			SET_TEXT_DROPSHADOW(1, 0, 0, 0, 255);
			SET_TEXT_SCALE(0.3f, 0.3f);
			if (I == selParam)
			{
				SET_TEXT_COLOUR(255, 255, 0, 255);
			}
			else
			{
				SET_TEXT_COLOUR(255, 255, 255, 255);
			}
			DISPLAY_TEXT_WITH_FLOAT(0.05f, (I * 0.02f) + 0.05f, "NUMBR", flt[I], 4);
		}
		ATTACH_OBJECT_TO_CAR(TestObj, TestVeh, 0, flt[0], flt[1], flt[2], flt[3], flt[4], flt[5]);
	}
}

void SetCollision(bool has_collision)
{
	if (DOES_OBJECT_EXIST(TestObj))
	{
		SET_OBJECT_COLLISION(TestObj, has_collision);
		if (has_collision)
		{
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Collision for last object ~y~ON", 5000, 1);
		}
		else
		{
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Collision for last object ~y~OFF", 5000, 1);
		}
	}
	else
	{
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "No object to set collision for", 5000, 1);
	}
}

void ToggleVisible(void)
{
	if (DOES_VEHICLE_EXIST(TestVeh))
	{
		if (TestVehVis == 1)
		{
			TestVehVis = 0;
			SET_CAR_VISIBLE(TestVeh, 0);
		}
		else
		{
			TestVehVis = 1;
			SET_CAR_VISIBLE(TestVeh, 1);
		}
	}
	else
	{
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "You need to have a car", 5000, 1);
	}
}

void adjustHash(bool incr_h)
{
	if (incr_h)
	{
		if (hashInt[selDigit] < 15)
		{
			hashInt[selDigit] = hashInt[selDigit] + 1;
		}
		else
		{
			hashInt[selDigit] = 0;
		}
	}
	else
	{
		if (hashInt[selDigit] > 0)
		{
			hashInt[selDigit] = hashInt[selDigit] - 1;
		}
		else
		{
			hashInt[selDigit] = 15;
		}
	}
}

void selectDigit(bool incr_d)
{
	if (incr_d)
	{
		if (selDigit < 7)
		{
			selDigit++;
		}
		else
		{
			selDigit = 0;
		}
	}
	else
	{
		if (selDigit > 0)
		{
			selDigit--;
		}
		else
		{
			selDigit = 7;
		}
	}
}

void SelectParam(bool incr_s)
{
	if (incr_s)
	{
		if (selParam < 5)
		{
			selParam++;
		}
		else
		{
			selParam = 0;
		}
	}
	else
	{
		if (selParam > 0)
		{
			selParam--;
		}
		else
		{
			selParam = 5;
		}
	}
}

void ResetParams(void)
{
	flt[0] = 0.0f;
	flt[1] = 0.0f;
	flt[2] = 1.0f;
	flt[3] = 0.0f;
	flt[4] = 0.0f;
	flt[5] = 0.0f;
	selDigit = 0;
}

void ResetSingleParam(void)
{
	if (selParam == 2)
	{
		flt[selParam] = 1.0f;
	}
	else
	{
		flt[selParam] = 0.0f;
	}
}

void ButtonInput(void)
{
	if (IS_BUTTON_PRESSED(0,L1) && IS_BUTTON_JUST_PRESSED(0,X) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_ENTER))
	{
		if (doHashInput == 1)
		{
			doHashInput = 0;
		}
		ResetParams();
		SpawnObject();
	}
	if (IS_BUTTON_PRESSED(0,L1) && IS_BUTTON_JUST_PRESSED(0,TRIANGLE) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_K))
	{
		KillObject();
	}
	if (IS_BUTTON_PRESSED(0,L1) && IS_BUTTON_JUST_PRESSED(0,SQUARE) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_T))
	{
		if (DOES_VEHICLE_EXIST(TestVeh))
		{
			if (doAdjustObject == 0)
			{
				doAdjustObject = 1;
			}
			if (doHashInput == 1)
			{
				doHashInput = 0;
			}
		}
		else
		{
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "You need to have a car", 5000, 1);
		}
	}
	if (IS_BUTTON_PRESSED(0,L1) && IS_BUTTON_JUST_PRESSED(0,CIRCLE) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_I))
	{
		if (DOES_VEHICLE_EXIST(TestVeh))
		{
			if (doHashInput == 0)
			{
				doHashInput = 1;
			}
			if (doAdjustObject == 1)
			{
				doAdjustObject = 0;
			}
		}
		else
		{
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "You need to have a car", 5000, 1);
		}
	}
	if (IS_BUTTON_PRESSED(0,L1) && IS_BUTTON_JUST_PRESSED(0,STICK_L) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_V))
	{
		ToggleVisible();
	}
	if (IS_BUTTON_PRESSED(0,STICK_R) && IS_BUTTON_JUST_PRESSED(0,DPAD_UP) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_Y))
	{
		SetCollision(1);
	}
	if (IS_BUTTON_PRESSED(0,STICK_R) && IS_BUTTON_JUST_PRESSED(0,DPAD_DOWN) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_N))
	{
		SetCollision(0);
	}
	if (IS_BUTTON_JUST_PRESSED(0, CIRCLE) && !IS_BUTTON_PRESSED(0,L1) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_BACKSPACE))
	{
		if (doHashInput == 1)
		{
			doHashInput = 0;
		}
		if (doAdjustObject == 1)
		{
			doAdjustObject = 0;
		}
	}
	if (IS_BUTTON_JUST_PRESSED(0, STICK_R))
	{
		if (doAdjustObject == 1)
		{
			ResetSingleParam();
		}
	}
	if (IS_BUTTON_JUST_PRESSED(0, DPAD_UP))
	{
		if (doAdjustObject == 1)
		{
			SelectParam(0);
		}
		if (doHashInput == 1)
		{
			adjustHash(1);
		}
	}
	if (IS_BUTTON_JUST_PRESSED(0, DPAD_DOWN))
	{
		if (doAdjustObject == 1)
		{
			SelectParam(1);
		}
		if (doHashInput == 1)
		{
			adjustHash(0);
		}
	}
	if (IS_BUTTON_JUST_PRESSED(0, DPAD_RIGHT))
	{
		if (doHashInput == 1)
		{
			selectDigit(1);
		}
	}
	if (IS_BUTTON_JUST_PRESSED(0, DPAD_LEFT))
	{
		if (doHashInput == 1)
		{
			selectDigit(0);
		}
	}
	if (IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_0) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMLOCK_0))
	{
		if (doHashInput == 1)
		{
			hashInt[selDigit] = 0;
			selectDigit(1);
		}
	}
	if (IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_1) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMLOCK_1))
	{
		if (doHashInput == 1)
		{
			hashInt[selDigit] = 1;
			selectDigit(1);
		}
	}
	if (IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_2) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMLOCK_2))
	{
		if (doHashInput == 1)
		{
			hashInt[selDigit] = 2;
			selectDigit(1);
		}
	}
	if (IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_3) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMLOCK_3))
	{
		if (doHashInput == 1)
		{
			hashInt[selDigit] = 3;
			selectDigit(1);
		}
	}
	if (IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_4) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMLOCK_4))
	{
		if (doHashInput == 1)
		{
			hashInt[selDigit] = 4;
			selectDigit(1);
		}
	}
	if (IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_5) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMLOCK_5))
	{
		if (doHashInput == 1)
		{
			hashInt[selDigit] = 5;
			selectDigit(1);
		}
	}
	if (IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_6) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMLOCK_6))
	{
		if (doHashInput == 1)
		{
			hashInt[selDigit] = 6;
			selectDigit(1);
		}
	}
	if (IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_7) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMLOCK_7))
	{
		if (doHashInput == 1)
		{
			hashInt[selDigit] = 7;
			selectDigit(1);
		}
	}
	if (IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_8) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMLOCK_8))
	{
		if (doHashInput == 1)
		{
			hashInt[selDigit] = 8;
			selectDigit(1);
		}
	}
	if (IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_9) || IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMLOCK_9))
	{
		if (doHashInput == 1)
		{
			hashInt[selDigit] = 9;
			selectDigit(1);
		}
	}
	if (IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_A))
	{
		if (doHashInput == 1)
		{
			hashInt[selDigit] = 10;
			selectDigit(1);
		}
	}
	if (IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_B))
	{
		if (doHashInput == 1)
		{
			hashInt[selDigit] = 11;
			selectDigit(1);
		}
	}
	if (IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_C))
	{
		if (doHashInput == 1)
		{
			hashInt[selDigit] = 12;
			selectDigit(1);
		}
	}
	if (IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_D))
	{
		if (doHashInput == 1)
		{
			hashInt[selDigit] = 13;
			selectDigit(1);
		}
	}
	if (IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_E))
	{
		if (doHashInput == 1)
		{
			hashInt[selDigit] = 14;
			selectDigit(1);
		}
	}
	if (IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
	{
		if (doHashInput == 1)
		{
			hashInt[selDigit] = 15;
			selectDigit(1);
		}
	}
}

void main(void)
{
	while(true)
	{
		WAIT(0);
		if (IS_CHAR_IN_ANY_CAR(GetPlayerPed()))
		{
			GET_CAR_CHAR_IS_USING(GetPlayerPed(), &TestVeh);
		}
		testModel = (hashInt[0] * 268435456) + (hashInt[1] * 16777216) + (hashInt[2] * 1048576) + (hashInt[3] * 65536) + (hashInt[4] * 4096) + (hashInt[5] * 256) + (hashInt[6] * 16) + hashInt[7];
		ButtonInput();
		if (doAdjustObject == 1)
		{
			AdjustObject();
		}
		if (doHashInput == 1)
		{
			HashInput();
		}
	}
}