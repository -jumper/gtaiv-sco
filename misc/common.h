/*	---------------------------------------------------------------
	 This header contains GTAIV/EFLC PC script common functions.

	 This file is a part of scocl project (C) Alexander Blade 2011
	---------------------------------------------------------------  */

#pragma once

#include "natives.h"
#include "consts.h"
/*
Player GetPlayerIndex(void)
{
	return GET_PLAYER_ID();
}
*/
Player GetPlayerIndex(void)
{
	return CONVERT_INT_TO_PLAYERINDEX(GET_PLAYER_ID());
}

Ped GetPlayerPed(void)
{
	Ped playerped = INVALID_HANDLE;
	if ( PLAYER_HAS_CHAR(GetPlayerIndex()) )
		GET_PLAYER_CHAR(GetPlayerIndex(), &playerped);
	return playerped;
}

void UpdateWeaponOfPed(Ped ped, Weapon weapon)
{
	if (HAS_CHAR_GOT_WEAPON(ped, weapon))
	{
		SET_CHAR_AMMO(ped, weapon, AMMO_MAX);    
	} 	else
	{
		GIVE_WEAPON_TO_CHAR(ped, weapon, AMMO_MAX, FALSE);	
	}
}

void debugwait(char *string)
{
	while (1)
	{
		WAIT(0);
		SET_TEXT_DROPSHADOW(1, 0, 0, 0, 255);
		SET_TEXT_SCALE(0.3f, 0.3f);
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.5f, 0.475f, "STRING", string);

		SET_TEXT_DROPSHADOW(1, 0, 0, 0, 255);
		SET_TEXT_SCALE(0.3f, 0.3f);
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.5f, 0.525f, "STRING", "press ~PAD_X~ to continue");
		if (IS_BUTTON_JUST_PRESSED(0,0xE))
		{
			break;
		}
	}
}

void SetEpisodicColors(int *eRed, int *eGreen, int *eBlue)
{
	if (GET_CURRENT_EPISODE() == 0)
	{
		*eRed = 240;
		*eGreen = 160;
		*eBlue = 1;
	}
	else if (GET_CURRENT_EPISODE() == 1)
	{
		*eRed = 117;
		*eGreen = 14;
		*eBlue = 16;
	}
	else if (GET_CURRENT_EPISODE() == 2)
	{
		*eRed = 215;
		*eGreen = 17;
		*eBlue = 110;
	}
}

void GetCoordsFromGameCam(float inputDist, float *outputX, float *outputY, float *outputZ)
{
	Camera gameCam;

	GET_GAME_CAM(&gameCam);
	if (IS_CAM_ACTIVE(gameCam))
	{
		Vector3 gameCamPos, gameCamRot;

		GET_CAM_POS(gameCam, &gameCamPos.x, &gameCamPos.y, &gameCamPos.z);
		GET_CAM_ROT(gameCam, &gameCamRot.x, &gameCamRot.y, &gameCamRot.z);
		// cam rot Y is not needed, so I am dumping it into temp
		// this is only to save a variable, temp gets calculated next
		//temp = (inputDist * COS(gameCamRot.x));
		*outputX = gameCamPos.x - ((inputDist * COS(gameCamRot.x)) * SIN(gameCamRot.z));
		*outputY = gameCamPos.y + ((inputDist * COS(gameCamRot.x)) * COS(gameCamRot.z));
		*outputZ = gameCamPos.z + (inputDist * SIN(gameCamRot.x));
	}
}

uint fStatTimeToHr(int statIndex)
{
	return (int)(GET_FLOAT_STAT(statIndex) / 3600000.0);
}

uint fStatTimeToMin(int statIndex)
{
	return ((int)GET_FLOAT_STAT(statIndex) % 3600000) / 60000;
}

uint fStatTimeToSec(int statIndex)
{
	return (((int)GET_FLOAT_STAT(statIndex) % 3600000) % 60000) / 1000;
}

void setStatTimeHr(int statIndex, int newVal)
{
	float newHr, newMin, newSec;

	newHr = (float)newVal * 3600000.0;
	newMin = (float)fStatTimeToMin(statIndex) * 60000.0;
	newSec = (float)fStatTimeToSec(statIndex) * 1000.0;
	SET_FLOAT_STAT(statIndex, newHr + newMin + newSec);
}

void setStatTimeMin(int statIndex, int newVal)
{
	float newHr, newMin, newSec;

	newHr = (float)fStatTimeToHr(statIndex) * 3600000.0;
	newMin = (float)newVal * 60000.0;
	newSec = (float)fStatTimeToSec(statIndex) * 1000.0;
	SET_FLOAT_STAT(statIndex, newHr + newMin + newSec);
}

void setStatTimeSec(int statIndex, int newVal)
{
	float newHr, newMin, newSec;

	newHr = (float)fStatTimeToHr(statIndex) * 3600000.0;
	newMin = (float)fStatTimeToMin(statIndex) * 60000.0;
	newSec = (float)newVal * 1000.0;
	SET_FLOAT_STAT(statIndex, newHr + newMin + newSec);
}
