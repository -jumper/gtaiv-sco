#pragma once

#define L1         0x4
#define L2         0x5
#define R1         0x6
#define R2         0x7
#define DPAD_UP    0x8
#define DPAD_DOWN  0x9
#define DPAD_LEFT  0xA
#define DPAD_RIGHT 0xB
#define START      0xC
#define SELECT     0xD
#define SQUARE     0xE
#define TRIANGLE   0xF
#define X          0x10
#define CIRCLE     0x11
#define STICK_L    0x12  // L3
#define STICK_R    0x13  // R3

//	pressure sensitive triggers for both control schemes
//	each function returns 0-255 depending on trigger pressure
//	player control must be on for these to work! otherwise you only get 0
int PSenseR2(void)
{
	int result;
	if (USING_STANDARD_CONTROLS())
		result = GET_CONTROL_VALUE(0, 55);
	else
		result = GET_CONTROL_VALUE(0, 58);
	return result;
}

int PSenseL2(void)
{
	int result;
	if (USING_STANDARD_CONTROLS())
		result = GET_CONTROL_VALUE(0, 56);
	else
		result = GET_CONTROL_VALUE(0, 57);
	return result;
}