/*	---------------------------------------------------------------
	 This header contains GTAIV/EFLC PC script string functions.

	 This file is a part of scocl project (C) Alexander Blade 2011
	---------------------------------------------------------------  */

#pragma once

#include "types.h"

extern void itos(int value, void *str, uint strsize);

extern void print_basic(char *string)
{
	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", string, 3000, 1);
}

extern void print(char *string)
{
	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", string, 3000, 1);
}

extern void print_timed(char *string, uint seconds)
{
	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", string, seconds * 1000, 1);
}
