#include <natives.h>
#include <common.h>
#include <strings.h>
#include <types.h>
#include <consts.h>
/*** added to scocl/inc/consts.h ***
MODEL_dildo1 = 0x3675A6C3,
MODEL_BM_drum_exp = 0x8F2A7EB3,
MODEL_BM_drum_tox = 0x61BBB992,
************************************/
/********************** PS3 CONTROLLER BUTTON MAP **********************/
#define L1         0x4
#define L2         0x5
#define R1         0x6
#define R2         0x7
#define DPAD_UP    0x8
#define DPAD_DOWN  0x9
#define DPAD_LEFT  0xA
#define DPAD_RIGHT 0xB
#define START      0xC
#define SELECT     0xD
#define SQUARE     0xE
#define TRIANGLE   0xF
#define X          0x10
#define CIRCLE     0x11
#define STICK_L    0x12  // L3
#define STICK_R    0x13  // R3
/**************************************************  *********************/
int ProjectilesAreSet = 0;
int SelectedProjectile = 1;
int ShootObject = 0, NextProjectile = 0, ClearObjects = 0;
int CharWeapon;
int ProjectileModel[11];
char* ProjectileModelName[11];
Object ObjectProjectile;
float prjX, prjY, prjZ, prjT, gcX, gcY, gcZ, gcrotX, gcrotY, gcrotZ, objrotX, objrotZ;
Camera game_cam;

void SetProjectiles()
{
	ProjectileModel[1] = MODEL_BM_drum_tox;	ProjectileModelName[1] = "MODEL_BM_drum_tox";
	ProjectileModel[2] = MODEL_BM_drum_exp;	ProjectileModelName[2] = "MODEL_BM_drum_exp";
	ProjectileModel[3] = MODEL_CJ_WASTEBIN;	ProjectileModelName[3] = "MODEL_CJ_WASTEBIN";
	ProjectileModel[4] = MODEL_CJ_B_CAN1;	ProjectileModelName[4] = "MODEL_CJ_B_CAN1";
	ProjectileModel[5] = MODEL_CJ_BLOX_1;	ProjectileModelName[5] = "MODEL_CJ_BLOX_1";
	ProjectileModel[6] = MODEL_CJ_BOWLING_BALL3;	ProjectileModelName[6] = "MODEL_CJ_BOWLING_BALL3";
	ProjectileModel[7] = MODEL_CJ_BOWLING_PIN;	ProjectileModelName[7] = "MODEL_CJ_BOWLING_PIN";
	ProjectileModel[8] = MODEL_CJ_CEREAL_BOX2;	ProjectileModelName[8] = "MODEL_CJ_CEREAL_BOX2";
	ProjectileModel[9] = MODEL_dildo1;	ProjectileModelName[9] = "MODEL_dildo1";
	ProjectileModel[10] = MODEL_CJ_GAME_CUBE_3;	ProjectileModelName[10] = "MODEL_CJ_GAME_CUBE_3";
	ProjectilesAreSet = 1;
}

void MainLoop()
{
	if(!HAS_CHAR_GOT_WEAPON(GetPlayerPed(), 18))
	{
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "RPG not found, have an RPG.", 5000, 1);
		GIVE_WEAPON_TO_CHAR(GetPlayerPed(), 18, 8, 1);
	}
	GET_CURRENT_CHAR_WEAPON(GetPlayerPed(), &CharWeapon);
	REQUEST_MODEL(ProjectileModel[SelectedProjectile]);
	GET_GAME_CAM(&game_cam);
	if (IS_CAM_ACTIVE(game_cam))
	{
		GET_CAM_ROT(game_cam, &gcrotX, &gcrotY, &gcrotZ);// used for setting the object rotation and for some weird trig stuff below
		GET_CAM_POS(game_cam, &gcX, &gcY, &gcZ);// used for the spawn point of the object, because the player is offset while aiming
		if (gcrotX < 0.0)// the range for cam rot is -180 to 180, to set object rot we need 0 to 360
		{
			objrotX = gcrotX + 360.0;
		}
		else
		{
			objrotX = gcrotX;
		}
		if (gcrotZ < 0.0)
		{
			objrotZ = gcrotZ + 360.0;
		}
		else
		{
			objrotZ = gcrotZ;
		}
		/*  the trig stuff below could possibly be replaced with vectors, I have no idea how to do that though.  *
		*   I apologize if this is confusing, but if you want to change the distance from the game_cam that the  *
		*   object is spawned, adjust "3.0" to your preference on the first and fourth lines.  Also prjT is the  *
		*   adjacent side from the pitch calculation, its purpose is to be the tangent in the following 2 lines */
		prjT = (3.0 * COS(gcrotX));       // adj side calculation to be used as a tangent below
		prjX = gcX - (prjT * SIN(gcrotZ));// calculates how far to spawn the object from the game_cam on the X plane
		prjY = gcY + (prjT * COS(gcrotZ));// calculates how far to spawn the object from the game_cam on the Y plane
		prjZ = gcZ + (3.0 * SIN(gcrotX)); // calculates how far to spawn the object from the game_cam on the Z plane
	}
}

void Actions()
{
	if (NextProjectile == 1)
	{
		NextProjectile = 0;
		if (SelectedProjectile == 10)
		{
			SelectedProjectile = 1;
		}
		else
		{
			SelectedProjectile = SelectedProjectile + 1;
		}
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", ProjectileModelName[SelectedProjectile], 5000, 1);
	}
	if (ClearObjects == 1)
	{
		ClearObjects = 0;
		CLEAR_AREA_OF_OBJECTS(gcX, gcY, gcZ, 100.0);
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Objects Cleared", 5000, 1);
	}
	if (ShootObject == 1)
	{
		ShootObject = 0;
		while(!HAS_MODEL_LOADED(ProjectileModel[SelectedProjectile]))
		{
			WAIT(0);
		}
		CREATE_OBJECT(ProjectileModel[SelectedProjectile], prjX, prjY, prjZ, &ObjectProjectile, 1);
		SET_OBJECT_VISIBLE(ObjectProjectile, 0);
		MARK_MODEL_AS_NO_LONGER_NEEDED(ProjectileModel[SelectedProjectile]);
		if(DOES_OBJECT_EXIST(ObjectProjectile))
		{
			WAIT(100);
			SET_OBJECT_AS_STEALABLE(ObjectProjectile, 1);
			SET_OBJECT_ROTATION(ObjectProjectile, objrotX, 0.0, objrotZ);
			SET_OBJECT_DYNAMIC(ObjectProjectile, 1);
			APPLY_FORCE_TO_OBJECT(ObjectProjectile, 1, 0.0, 90.0, 0.0, 0.0, 0.0, 0.0, 1, 1, 1, 1);
			SET_OBJECT_VISIBLE(ObjectProjectile, 1);
		}
	}
}

void ButtonInput(void)
{
	if (CharWeapon == 18)
	{
		if (IS_BUTTON_PRESSED(0,L2) && IS_BUTTON_JUST_PRESSED(0,X))
		{
			ShootObject = 1;
		}
		if (IS_BUTTON_PRESSED(0,L2) && IS_BUTTON_JUST_PRESSED(0,DPAD_RIGHT))
		{
			NextProjectile = 1;
		}
		if (IS_BUTTON_PRESSED(0,L2) && IS_BUTTON_JUST_PRESSED(0,STICK_L))
		{
			ClearObjects = 1;
		}
	}
}

void main(void)
{
	while(true)
	{
		WAIT(0);
		if (ProjectilesAreSet == 0)
		{
			SetProjectiles();
		}
		MainLoop();
		Actions();
		ButtonInput();
	}
}