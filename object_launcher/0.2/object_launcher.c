#include <natives.h>
#include <common.h>
#include <strings.h>
#include <types.h>
#include <consts.h>
#include <ps3pad.h>

int CharWeapon, RunOnceCheck = 0, SelPrj = 1;
int PrjMdl[61];// models
char* PrjMdlName[61];// model names
Object ObjProjectile;
float prjX, prjY, prjZ, prjT;// XYZ coord to launch object from (T is for weird trig stuff)
float gcX, gcY, gcZ;// XYZ coord of the game_cam
float gcrotX, gcrotY, gcrotZ;// ROT of the game_cam (Y is not used)
float objrotX, objrotZ;// ROT of the object being launched
uint objForceInt = 50;// force applied to the object being launched (gets converted to float when used)
Camera game_cam;

void RunOnce()
{
	if(!HAS_CHAR_GOT_WEAPON(GetPlayerPed(), 18))
	{
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "RPG not found, have an RPG.", 5000, 1);
		GIVE_WEAPON_TO_CHAR(GetPlayerPed(), 18, 8, 1);
	}
	PrjMdl[1] = 0x3C4E43BC;		PrjMdlName[1] = "CJ_DONUT";
	PrjMdl[2] = 0xEE548951;		PrjMdlName[2] = "CJ_DONUT2";
	PrjMdl[3] = 0xFEAD2A02;		PrjMdlName[3] = "CJ_DONUT3";
	PrjMdl[4] = 0x7B5E63B5;		PrjMdlName[4] = "BrickDark";
	PrjMdl[5] = 0xA6BBCCDB;		PrjMdlName[5] = "BXE2_dyn_brick01";
	PrjMdl[6] = 0xFE520830;		PrjMdlName[6] = "CJ_PROC_BRICK";
	PrjMdl[7] = 0xB49F6A82;		PrjMdlName[7] = "CJ_PROC_BRICK2";
	PrjMdl[8] = 0xB00ABE6D;		PrjMdlName[8] = "CJ_GOLF_BALL";
	PrjMdl[9] = 0x94A8F60F;		PrjMdlName[9] = "CJ_BIN_1";
	PrjMdl[10] = 0x6E77A9AD;		PrjMdlName[10] = "CJ_BIN_2";
	PrjMdl[11] = 0x410DCED6;		PrjMdlName[11] = "CJ_BIN_3";
	PrjMdl[12] = 0xBBC8C44E;		PrjMdlName[12] = "CJ_BIN_4";
	PrjMdl[13] = 0xD682F9C2;		PrjMdlName[13] = "CJ_BIN_5";
	PrjMdl[14] = 0xA72D1B17;		PrjMdlName[14] = "CJ_BIN_6";
	PrjMdl[15] = 0xF936BF15;		PrjMdlName[15] = "CJ_BIN_8";
	PrjMdl[16] = 0xC37F53A3;		PrjMdlName[16] = "CJ_BIN_9";
	PrjMdl[17] = 0x9683F15D;		PrjMdlName[17] = "CJ_BIN_10";
	PrjMdl[18] = 0xD5B8EFC6;		PrjMdlName[18] = "CJ_BIN_11";
	PrjMdl[19] = 0xB95536FF;		PrjMdlName[19] = "CJ_BIN_13";
	PrjMdl[20] = 0xA2DE0A11;		PrjMdlName[20] = "CJ_BIN_14";
	PrjMdl[21] = 0x169EF191;		PrjMdlName[21] = "CJ_BIN_15";
	PrjMdl[22] = 0x0894557C;		PrjMdlName[22] = "CJ_BIN_16";
	PrjMdl[23] = 0x90FA92C6;		PrjMdlName[23] = "CJ_BOWLING_BALL3";
	PrjMdl[24] = 0xF4A206E4;		PrjMdlName[24] = "CJ_BOWLING_PIN";
	PrjMdl[25] = 0xE6C7978D;		PrjMdlName[25] = "CJ_B_CAN1";
	PrjMdl[26] = 0xF20514DA;		PrjMdlName[26] = "CJ_BAGUETTE_1";
	PrjMdl[27] = 0x19AF4794;		PrjMdlName[27] = "CJ_GAS_FIRE";
	PrjMdl[28] = 0x69F3A0EE;		PrjMdlName[28] = "CJ_LD_POOLBALL_8";
	PrjMdl[29] = 0xD825FA46;		PrjMdlName[29] = "CJ_B_CAN3";
	PrjMdl[30] = 0xA477525C;		PrjMdlName[30] = "CJ_MUMS_VASE";
	PrjMdl[31] = 0x1417B936;		PrjMdlName[31] = "CJ_KICKSTOOL";
	PrjMdl[32] = 0x81AC84C8;		PrjMdlName[32] = "CJ_FILEING_CAB_1";
	PrjMdl[33] = 0xDAB3D3FA;		PrjMdlName[33] = "CJ_FLIGHT_CASE_1";
	PrjMdl[34] = 0xF9525FC1;		PrjMdlName[34] = "CJ_FORK";
	PrjMdl[35] = 0x2718C626;		PrjMdlName[35] = "CJ_GAME_CUBE_1";
	PrjMdl[36] = 0xDD28B247;		PrjMdlName[36] = "CJ_GAME_CUBE_2";
	PrjMdl[37] = 0xCCEA11CA;		PrjMdlName[37] = "CJ_GAME_CUBE_3";
	PrjMdl[38] = 0xBB1F6E71;		PrjMdlName[38] = "CJ_GAME_CUBE_4";
	PrjMdl[39] = 0xA6E545FD;		PrjMdlName[39] = "CJ_GAME_CUBE_5";
	PrjMdl[40] = 0x5C5030D4;		PrjMdlName[40] = "CJ_GAME_CUBE_6";
	PrjMdl[41] = 0x3675A6C3;		PrjMdlName[41] = "dildo1";
	PrjMdl[42] = 0x260C05F0;		PrjMdlName[42] = "dildo2";
	PrjMdl[43] = 0x9976ECC4;		PrjMdlName[43] = "dildo3";
	PrjMdl[44] = 0x8BD0D178;		PrjMdlName[44] = "dildo4";
	PrjMdl[45] = 0x7F0337DD;		PrjMdlName[45] = "dildo5";
	PrjMdl[46] = 0x6F181807;		PrjMdlName[46] = "dildo6";
	PrjMdl[47] = 0x63A1012D;		PrjMdlName[47] = "dildo7";
	PrjMdl[48] = 0xD540646A;		PrjMdlName[48] = "dildo8";
	PrjMdl[49] = 0xC60EC607;		PrjMdlName[49] = "dildo9";
	PrjMdl[50] = 0xFAC0120A;		PrjMdlName[50] = "dildo10";
	PrjMdl[51] = 0xB1DE0047;		PrjMdlName[51] = "dildo11";
	PrjMdl[52] = 0xE89C6DC3;		PrjMdlName[52] = "dildo12";
	PrjMdl[53] = 0x7AFF9287;		PrjMdlName[53] = "dildo13";
	PrjMdl[54] = 0xB13DFF03;		PrjMdlName[54] = "dildo14";
	PrjMdl[55] = 0x686AED56;		PrjMdlName[55] = "dildo15";
	PrjMdl[56] = 0x56A549CB;		PrjMdlName[56] = "dildo16";
	PrjMdl[57] = 0x43F6246D;		PrjMdlName[57] = "dildo17";
	PrjMdl[58] = 0x322780D0;		PrjMdlName[58] = "dildo18";
	PrjMdl[59] = 0x32BBE5F4;		PrjMdlName[59] = "CJ_Gas_Canz";
	PrjMdl[60] = 0xD86B0B28;		PrjMdlName[60] = "CJ_CONE_SM";
	RunOnceCheck = 1;
}

void MainLoop()
{
	GET_CURRENT_CHAR_WEAPON(GetPlayerPed(), &CharWeapon);
	if (CharWeapon == 18)
	{
		SET_TEXT_DROPSHADOW(1, 0, 0, 0, 255);
		SET_TEXT_SCALE(0.2f, 0.2f);
		DISPLAY_TEXT_WITH_NUMBER(0.832f, 0.069f, "NUMBR", objForceInt);
		GET_GAME_CAM(&game_cam);
		if (IS_CAM_ACTIVE(game_cam))
		{
			GET_CAM_ROT(game_cam, &gcrotX, &gcrotY, &gcrotZ);// used for setting the object rotation and for some weird trig stuff below
			GET_CAM_POS(game_cam, &gcX, &gcY, &gcZ);// used for the spawn point of the object, because the player is offset while aiming
			if (gcrotX < 0.0)// the range for cam rot is -180 to 180, to set object rot we need 0 to 360
			{
				objrotX = gcrotX + 360.0;
			}
			else
			{
				objrotX = gcrotX;
			}
			if (gcrotZ < 0.0)
			{
				objrotZ = gcrotZ + 360.0;
			}
			else
			{
				objrotZ = gcrotZ;
			}
			/*  the trig stuff below could possibly be replaced with vectors, I have no idea how to do that though.  *
			*   I apologize if this is confusing, but if you want to change the distance from the game_cam that the  *
			*   object is spawned, adjust "2.0" to your preference on the first and fourth lines.  Also prjT is the  *
			*   adjacent side from the pitch calculation, its purpose is to be the tangent in the following 2 lines */
			prjT = (2.0 * COS(gcrotX));       // adj side calculation to be used as a tangent below
			prjX = gcX - (prjT * SIN(gcrotZ));// calculates how far to spawn the object from the game_cam on the X plane
			prjY = gcY + (prjT * COS(gcrotZ));// calculates how far to spawn the object from the game_cam on the Y plane
			prjZ = gcZ + (2.0 * SIN(gcrotX)); // calculates how far to spawn the object from the game_cam on the Z plane
		}
	}
}

void RemoveObject()
{
	if (DOES_OBJECT_EXIST(ObjProjectile))
	{
		DELETE_OBJECT(&ObjProjectile);
		MARK_MODEL_AS_NO_LONGER_NEEDED(PrjMdl[SelPrj]);
	}
}

void AdjustForce(bool incr)
{
	if (incr)
	{
		if (objForceInt < 1000)
		{
			objForceInt = objForceInt + 10;
		}
	}
	else
	{
		if (objForceInt > 0)
		{
			objForceInt = objForceInt - 10;
		}
	}
}

void SelectModel(bool next)
{
	RemoveObject();
	if (next)
	{
		if (SelPrj == 60)
		{
			SelPrj = 1;
		}
		else
		{
			SelPrj = SelPrj + 1;
		}
	}
	else
	{
		if (SelPrj == 1)
		{
			SelPrj = 60;
		}
		else
		{
			SelPrj = SelPrj - 1;
		}
	}
	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", PrjMdlName[SelPrj], 5000, 1);
}

void SpawnObject()
{
	RemoveObject();
	REQUEST_MODEL(PrjMdl[SelPrj]);
	while(!HAS_MODEL_LOADED(PrjMdl[SelPrj]))
	{
		WAIT(0);
	}
	CREATE_OBJECT(PrjMdl[SelPrj], prjX, prjY, prjZ, &ObjProjectile, 1);
	while(!DOES_OBJECT_EXIST(ObjProjectile))
	{
		WAIT(0);
	}
	SET_OBJECT_AS_STEALABLE(ObjProjectile, 1);
	SET_OBJECT_DYNAMIC(ObjProjectile, 1);
	GIVE_PED_PICKUP_OBJECT(GetPlayerPed(), ObjProjectile, 1);
}

void ShootObject()
{
	if(DOES_OBJECT_EXIST(ObjProjectile))
	{
		SET_OBJECT_COORDINATES(ObjProjectile, prjX, prjY, prjZ);
		SET_OBJECT_ROTATION(ObjProjectile, objrotX, 0.0, objrotZ);
		APPLY_FORCE_TO_OBJECT(ObjProjectile, 1, 0.0, TO_FLOAT(objForceInt), 0.0, 0.0, 0.0, 0.0, 1, 1, 1, 1);
	}
	else
	{
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "No object to shoot", 5000, 1);
	}
}

void ButtonInput(void)
{
	if (CharWeapon == 18)
	{
		if (IS_BUTTON_PRESSED(0,L2) && IS_BUTTON_JUST_PRESSED(0,L1))
		{
			ShootObject();
		}
		if (IS_BUTTON_PRESSED(0,L2) && IS_BUTTON_JUST_PRESSED(0,DPAD_RIGHT))
		{
			SelectModel(1);
		}
		if (IS_BUTTON_PRESSED(0,L2) && IS_BUTTON_JUST_PRESSED(0,DPAD_LEFT))
		{
			SelectModel(0);
		}
		if (IS_BUTTON_PRESSED(0,L2) && IS_BUTTON_PRESSED(0,DPAD_UP))
		{
			AdjustForce(1);
		}
		if (IS_BUTTON_PRESSED(0,L2) && IS_BUTTON_PRESSED(0,DPAD_DOWN))
		{
			AdjustForce(0);
		}
		if (IS_BUTTON_PRESSED(0,L2) && IS_BUTTON_JUST_PRESSED(0,SQUARE))
		{
			SpawnObject();
		}
		if (IS_BUTTON_PRESSED(0,L2) && IS_BUTTON_JUST_PRESSED(0,CIRCLE))
		{
			RemoveObject();
		}
	}
}

void main(void)
{
	while(true)
	{
		WAIT(0);
		if (RunOnceCheck == 0)
		{
			RunOnce();
		}
		MainLoop();
		ButtonInput();
	}
}