#include <natives.h>
#include <common.h>
#include <strings.h>
#include <types.h>
#include <consts.h>
#include <ps3pad.h>

//float flt[7];
int PrjMdl[61];					// models
char* PrjMdlName[61];			// model names
Object BaseObject;				// base object that everything attaches to (invisible)
Object OLauncher;				// object for the launcher
Object OLauncherMuz;			// object for the launcher muzzle (invisible - only used to get coords for launched objects)
Object OLauncherMisc1;			// motherfuckin hippo lol
Object ObjProjectile;			// object getting launched
float olH = 0.0f, olP = 0.0f;	// heading and pitch for launcher
Camera ObjLaunchCam;			// camera attached to launcher
int BaseObjectActive = 0;
int SelObjPage = 1;
int SelPrj = 1;
int RapidLaunch = 0;
int PhysEnabled = 0;
int PedEnabled = 0;
//int ObjectOnFire = 0;
float oPP[11];
int selPhysPrm = 1;
//int ObjPtfx = 0;
uint objForceInt = 50;// force applied to the object being launched (gets converted to float when used)

void SetObjects(int selection)
{
	SelPrj = 1;
	if (selection == 1)
	{
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Objects page 1", 5000, 1);
		PrjMdl[1] = 0x3C4E43BC;		PrjMdlName[1] = "CJ_DONUT";
		PrjMdl[2] = 0xEE548951;		PrjMdlName[2] = "CJ_DONUT2";
		PrjMdl[3] = 0xFEAD2A02;		PrjMdlName[3] = "CJ_DONUT3";
		PrjMdl[4] = 0x7B5E63B5;		PrjMdlName[4] = "BrickDark";
		PrjMdl[5] = 0xA6BBCCDB;		PrjMdlName[5] = "BXE2_dyn_brick01";
		PrjMdl[6] = 0xFE520830;		PrjMdlName[6] = "CJ_PROC_BRICK";
		PrjMdl[7] = 0xB49F6A82;		PrjMdlName[7] = "CJ_PROC_BRICK2";
		PrjMdl[8] = 0xB00ABE6D;		PrjMdlName[8] = "CJ_GOLF_BALL";
		PrjMdl[9] = 0x94A8F60F;		PrjMdlName[9] = "CJ_BIN_1";
		PrjMdl[10] = 0x6E77A9AD;		PrjMdlName[10] = "CJ_BIN_2";
		PrjMdl[11] = 0x410DCED6;		PrjMdlName[11] = "CJ_BIN_3";
		PrjMdl[12] = 0xBBC8C44E;		PrjMdlName[12] = "CJ_BIN_4";
		PrjMdl[13] = 0xD682F9C2;		PrjMdlName[13] = "CJ_BIN_5";
		PrjMdl[14] = 0xA72D1B17;		PrjMdlName[14] = "CJ_BIN_6";
		PrjMdl[15] = 0xF936BF15;		PrjMdlName[15] = "CJ_BIN_8";
		PrjMdl[16] = 0xC37F53A3;		PrjMdlName[16] = "CJ_BIN_9";
		PrjMdl[17] = 0x9683F15D;		PrjMdlName[17] = "CJ_BIN_10";
		PrjMdl[18] = 0xD5B8EFC6;		PrjMdlName[18] = "CJ_BIN_11";
		PrjMdl[19] = 0xB95536FF;		PrjMdlName[19] = "CJ_BIN_13";
		PrjMdl[20] = 0xA2DE0A11;		PrjMdlName[20] = "CJ_BIN_14";
		PrjMdl[21] = 0x169EF191;		PrjMdlName[21] = "CJ_BIN_15";
		PrjMdl[22] = 0x0894557C;		PrjMdlName[22] = "CJ_BIN_16";
		PrjMdl[23] = 0x90FA92C6;		PrjMdlName[23] = "CJ_BOWLING_BALL3";
		PrjMdl[24] = 0xF4A206E4;		PrjMdlName[24] = "CJ_BOWLING_PIN";
		PrjMdl[25] = 0xE6C7978D;		PrjMdlName[25] = "CJ_B_CAN1";
		PrjMdl[26] = 0xF20514DA;		PrjMdlName[26] = "CJ_BAGUETTE_1";
		PrjMdl[27] = 0x19AF4794;		PrjMdlName[27] = "CJ_GAS_FIRE";
		PrjMdl[28] = 0x69F3A0EE;		PrjMdlName[28] = "CJ_LD_POOLBALL_8";
		PrjMdl[29] = 0xD825FA46;		PrjMdlName[29] = "CJ_B_CAN3";
		PrjMdl[30] = 0xA477525C;		PrjMdlName[30] = "CJ_MUMS_VASE";
		PrjMdl[31] = 0x1417B936;		PrjMdlName[31] = "CJ_KICKSTOOL";
		PrjMdl[32] = 0x81AC84C8;		PrjMdlName[32] = "CJ_FILEING_CAB_1";
		PrjMdl[33] = 0xDAB3D3FA;		PrjMdlName[33] = "CJ_FLIGHT_CASE_1";
		PrjMdl[34] = 0xF9525FC1;		PrjMdlName[34] = "CJ_FORK";
		PrjMdl[35] = 0x2718C626;		PrjMdlName[35] = "CJ_GAME_CUBE_1";
		PrjMdl[36] = 0xDD28B247;		PrjMdlName[36] = "CJ_GAME_CUBE_2";
		PrjMdl[37] = 0xCCEA11CA;		PrjMdlName[37] = "CJ_GAME_CUBE_3";
		PrjMdl[38] = 0xBB1F6E71;		PrjMdlName[38] = "CJ_GAME_CUBE_4";
		PrjMdl[39] = 0xA6E545FD;		PrjMdlName[39] = "CJ_GAME_CUBE_5";
		PrjMdl[40] = 0x5C5030D4;		PrjMdlName[40] = "CJ_GAME_CUBE_6";
		PrjMdl[41] = 0x3675A6C3;		PrjMdlName[41] = "dildo1";
		PrjMdl[42] = 0x260C05F0;		PrjMdlName[42] = "dildo2";
		PrjMdl[43] = 0x9976ECC4;		PrjMdlName[43] = "dildo3";
		PrjMdl[44] = 0x8BD0D178;		PrjMdlName[44] = "dildo4";
		PrjMdl[45] = 0x7F0337DD;		PrjMdlName[45] = "dildo5";
		PrjMdl[46] = 0x6F181807;		PrjMdlName[46] = "dildo6";
		PrjMdl[47] = 0x63A1012D;		PrjMdlName[47] = "dildo7";
		PrjMdl[48] = 0xD540646A;		PrjMdlName[48] = "dildo8";
		PrjMdl[49] = 0xC60EC607;		PrjMdlName[49] = "dildo9";
		PrjMdl[50] = 0xFAC0120A;		PrjMdlName[50] = "dildo10";
		PrjMdl[51] = 0xB1DE0047;		PrjMdlName[51] = "dildo11";
		PrjMdl[52] = 0xE89C6DC3;		PrjMdlName[52] = "dildo12";
		PrjMdl[53] = 0x7AFF9287;		PrjMdlName[53] = "dildo13";
		PrjMdl[54] = 0xB13DFF03;		PrjMdlName[54] = "dildo14";
		PrjMdl[55] = 0x686AED56;		PrjMdlName[55] = "dildo15";
		PrjMdl[56] = 0x56A549CB;		PrjMdlName[56] = "dildo16";
		PrjMdl[57] = 0x43F6246D;		PrjMdlName[57] = "dildo17";
		PrjMdl[58] = 0x322780D0;		PrjMdlName[58] = "dildo18";
		PrjMdl[59] = 0x32BBE5F4;		PrjMdlName[59] = "CJ_Gas_Canz";
		PrjMdl[60] = 0xD86B0B28;		PrjMdlName[60] = "CJ_CONE_SM";
	}
	if (selection == 2)
	{
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Objects page 2", 5000, 1);
		PrjMdl[1] = 0xEB12D336;		PrjMdlName[1] = "CJ_Dumpster_1";
		PrjMdl[2] = 0xFBCD74AB;		PrjMdlName[2] = "CJ_Dumpster_2";
		PrjMdl[3] = 0xCD7E180D;		PrjMdlName[3] = "CJ_Dumpster_3";
		PrjMdl[4] = 0xE02FBD70;		PrjMdlName[4] = "CJ_Dumpster_4";
		PrjMdl[5] = 0xA2AB4268;		PrjMdlName[5] = "CJ_Dumpster_5";
		PrjMdl[6] = 0xB5F96904;		PrjMdlName[6] = "CJ_Dumpster_6";
		PrjMdl[7] = 0x0FDB87E5;		PrjMdlName[7] = "CJ_FENCE_1_1";
		PrjMdl[8] = 0x212DAA89;		PrjMdlName[8] = "CJ_FENCE_1_2";
		PrjMdl[9] = 0x33684EFE;		PrjMdlName[9] = "CJ_FENCE_1_3";
		PrjMdl[10] = 0x44B17190;		PrjMdlName[10] = "CJ_FENCE_1_4";
		PrjMdl[11] = 0xD62C148B;		PrjMdlName[11] = "CJ_FENCE_1_5";
		PrjMdl[12] = 0xE751B6D6;		PrjMdlName[12] = "CJ_FENCE_1_6";
		PrjMdl[13] = 0xFA9EDD70;		PrjMdlName[13] = "CJ_FENCE_1_7";
		PrjMdl[14] = 0xA8241C65;		PrjMdlName[14] = "CJ_FENCE_10_1";
		PrjMdl[15] = 0x8A39E0A1;		PrjMdlName[15] = "CJ_FENCE_10_2";
		PrjMdl[16] = 0x5BFB0424;		PrjMdlName[16] = "CJ_FENCE_10_3";
		PrjMdl[17] = 0x35BF5588;		PrjMdlName[17] = "CJ_FENCE_11_1";
		PrjMdl[18] = 0x435CF0C3;		PrjMdlName[18] = "CJ_FENCE_11_2";
		PrjMdl[19] = 0x10FA9F8C;		PrjMdlName[19] = "CJ_FENCE_12_1";
		PrjMdl[20] = 0xD6B5AB03;		PrjMdlName[20] = "CJ_FENCE_12_2";
		PrjMdl[21] = 0x741C0299;		PrjMdlName[21] = "CJ_FENCE_13_1";
		PrjMdl[22] = 0x95E4FE43;		PrjMdlName[22] = "CJ_FENCE_14_1";
		PrjMdl[23] = 0x60B693E7;		PrjMdlName[23] = "CJ_FENCE_14_2";
		PrjMdl[24] = 0xAB16962F;		PrjMdlName[24] = "CJ_FENCE_15_1";
		PrjMdl[25] = 0xA78C8BB7;		PrjMdlName[25] = "CJ_FENCE_15_11";
		PrjMdl[26] = 0x50D4E1AD;		PrjMdlName[26] = "CJ_FENCE_15_6";
		PrjMdl[27] = 0x3E9ABD39;		PrjMdlName[27] = "CJ_FENCE_15_7";
		PrjMdl[28] = 0x50C66184;		PrjMdlName[28] = "CJ_FENCE_15_8";
		PrjMdl[29] = 0x3F1E3E34;		PrjMdlName[29] = "CJ_FENCE_15_9";
		PrjMdl[30] = 0x7E31F893;		PrjMdlName[30] = "CJ_FENCE_16_1";
		PrjMdl[31] = 0xFE4778BC;		PrjMdlName[31] = "CJ_FENCE_16_2";
		PrjMdl[32] = 0xCFFD9C29;		PrjMdlName[32] = "CJ_FENCE_16_3";
		PrjMdl[33] = 0xA5B54799;		PrjMdlName[33] = "CJ_FENCE_16_4";
		PrjMdl[34] = 0xB75B6AE5;		PrjMdlName[34] = "CJ_FENCE_16_5";
		PrjMdl[35] = 0x584E8A99;		PrjMdlName[35] = "CJ_FENCE_17_1";
		PrjMdl[36] = 0x1A0B8E10;		PrjMdlName[36] = "CJ_FENCE_17_2";
		PrjMdl[37] = 0x286D2AD3;		PrjMdlName[37] = "CJ_FENCE_17_3";
		PrjMdl[38] = 0x7FE259C4;		PrjMdlName[38] = "CJ_FENCE_17_4";
		PrjMdl[39] = 0xD4FDFF5B;		PrjMdlName[39] = "CJ_FENCE_18_1";
		PrjMdl[40] = 0x4BB57E77;		PrjMdlName[40] = "CJ_FENCE_19_1";
		PrjMdl[41] = 0xDB691DE0;		PrjMdlName[41] = "CJ_FENCE_19_2";
		PrjMdl[42] = 0x67BD0609;		PrjMdlName[42] = "CJ_FENCE_19_43";
		PrjMdl[43] = 0x761DA2CA;		PrjMdlName[43] = "CJ_FENCE_19_44";
		PrjMdl[44] = 0x9A916BB1;		PrjMdlName[44] = "CJ_FENCE_19_46";
		PrjMdl[45] = 0xD79FE5C9;		PrjMdlName[45] = "CJ_FENCE_19_47";
		PrjMdl[46] = 0x6B798D6A;		PrjMdlName[46] = "CJ_FENCE_19_48";
		PrjMdl[47] = 0xB884FC3F;		PrjMdlName[47] = "CJ_FENCE_2_1";
		PrjMdl[48] = 0x9CC5C4C1;		PrjMdlName[48] = "CJ_FENCE_2_2";
		PrjMdl[49] = 0x4DF1A716;		PrjMdlName[49] = "CJ_FENCE_2_3";
		PrjMdl[50] = 0x402B8B8A;		PrjMdlName[50] = "CJ_FENCE_2_4";
		PrjMdl[51] = 0x71776E21;		PrjMdlName[51] = "CJ_FENCE_2_5";
		PrjMdl[52] = 0x4434A84C;		PrjMdlName[52] = "CJ_FENCE_20_1";
		PrjMdl[53] = 0xF4762EFB;		PrjMdlName[53] = "CJ_FENCE_21_1";
		PrjMdl[54] = 0x02D8CBC0;		PrjMdlName[54] = "CJ_FENCE_21_2";
		PrjMdl[55] = 0x69811903;		PrjMdlName[55] = "CJ_FENCE_21_3";
		PrjMdl[56] = 0x77BE357D;		PrjMdlName[56] = "CJ_FENCE_21_4";
		PrjMdl[57] = 0x463AFC43;		PrjMdlName[57] = "CJ_FENCE_22_3";
		PrjMdl[58] = 0xE5B1BB32;		PrjMdlName[58] = "CJ_FENCE_22_4";
		PrjMdl[59] = 0x76BFF682;		PrjMdlName[59] = "CJ_FENCE_23_1";
		PrjMdl[60] = 0x1C9CC23D;		PrjMdlName[60] = "CJ_FENCE_23_2";
	}
	if (selection == 3)
	{
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Objects page 3", 5000, 1);
		PrjMdl[1] = 0x26ADD65F;		PrjMdlName[1] = "CJ_FENCE_23_3";
		PrjMdl[2] = 0xCD03A308;		PrjMdlName[2] = "CJ_FENCE_23_4";
		PrjMdl[3] = 0x7DA7DF32;		PrjMdlName[3] = "CJ_FENCE_23_PST";
		PrjMdl[4] = 0xBCF23C02;		PrjMdlName[4] = "CJ_FENCE_3_1";
		PrjMdl[5] = 0xD078630E;		PrjMdlName[5] = "CJ_FENCE_3_2";
		PrjMdl[6] = 0xC23AC693;		PrjMdlName[6] = "CJ_FENCE_3_3";
		PrjMdl[7] = 0x440CCA39;		PrjMdlName[7] = "CJ_FENCE_3_4";
		PrjMdl[8] = 0x77E031DF;		PrjMdlName[8] = "CJ_FENCE_3_5";
		PrjMdl[9] = 0x699A9554;		PrjMdlName[9] = "CJ_FENCE_3_6";
		PrjMdl[10] = 0x9B50F8C0;		PrjMdlName[10] = "CJ_FENCE_3_7";
		PrjMdl[11] = 0xF9E6B5E2;		PrjMdlName[11] = "CJ_FENCE_3_8";
		PrjMdl[12] = 0xD99475D2;		PrjMdlName[12] = "CJ_FENCE_4_01";
		PrjMdl[13] = 0x43CE4A48;		PrjMdlName[13] = "CJ_FENCE_4_06";
		PrjMdl[14] = 0x73642973;		PrjMdlName[14] = "CJ_FENCE_4_07";
		PrjMdl[15] = 0x5CA47C00;		PrjMdlName[15] = "CJ_FENCE_4_08";
		PrjMdl[16] = 0x4400BC3E;		PrjMdlName[16] = "CJ_FENCE_4_1";
		PrjMdl[17] = 0x33AB1B93;		PrjMdlName[17] = "CJ_FENCE_4_2";
		PrjMdl[18] = 0x60C475C5;		PrjMdlName[18] = "CJ_FENCE_4_3";
		PrjMdl[19] = 0x4E35D0A8;		PrjMdlName[19] = "CJ_FENCE_4_4";
		PrjMdl[20] = 0xFCE52E08;		PrjMdlName[20] = "CJ_FENCE_4_5";
		PrjMdl[21] = 0x6B7C1CA1;		PrjMdlName[21] = "CJ_FENCE_5_1";
		PrjMdl[22] = 0x9935F814;		PrjMdlName[22] = "CJ_FENCE_5_2";
		PrjMdl[23] = 0x8B895CBB;		PrjMdlName[23] = "CJ_FENCE_5_3";
		PrjMdl[24] = 0x394A383E;		PrjMdlName[24] = "CJ_FENCE_5_4";
		PrjMdl[25] = 0x0CCA73A0;		PrjMdlName[25] = "CJ_FENCE_6_1";
		PrjMdl[26] = 0x8EF6F7F7;		PrjMdlName[26] = "CJ_FENCE_6_2";
		PrjMdl[27] = 0x7D20544A;		PrjMdlName[27] = "CJ_FENCE_6_3";
		PrjMdl[28] = 0x2F78B8FC;		PrjMdlName[28] = "CJ_FENCE_6_4";
		PrjMdl[29] = 0x21AB1D61;		PrjMdlName[29] = "CJ_FENCE_6_5";
		PrjMdl[30] = 0x26C8106D;		PrjMdlName[30] = "CJ_FENCE_6_54";
		PrjMdl[31] = 0x45B04E3D;		PrjMdlName[31] = "CJ_FENCE_6_55";
		PrjMdl[32] = 0x6A312ED8;		PrjMdlName[32] = "CJ_FENCE_6_6";
		PrjMdl[33] = 0x57D78A25;		PrjMdlName[33] = "CJ_FENCE_6_7";
		PrjMdl[34] = 0xD738201A;		PrjMdlName[34] = "CJ_FENCE_6_70";
		PrjMdl[35] = 0xB261AC79;		PrjMdlName[35] = "CJ_FENCE_7_1";
		PrjMdl[36] = 0x6E272401;		PrjMdlName[36] = "CJ_FENCE_7_2";
		PrjMdl[37] = 0xB4A01E13;		PrjMdlName[37] = "CJ_FENCE_8_1";
		PrjMdl[38] = 0xC312BAF8;		PrjMdlName[38] = "CJ_FENCE_8_2";
		PrjMdl[39] = 0xC6B553F6;		PrjMdlName[39] = "CJ_FENCE_9_1";
		PrjMdl[40] = 0xB83AB701;		PrjMdlName[40] = "CJ_FENCE_9_2";
		PrjMdl[41] = 0x674EDE84;		PrjMdlName[41] = "CJ_FENCE_CROWD";
		PrjMdl[42] = 0x5F7FC466;		PrjMdlName[42] = "CJ_J_CAN_used";
		PrjMdl[43] = 0x355B211A;		PrjMdlName[43] = "CJ_J_CAN1";
		PrjMdl[44] = 0xB893A78D;		PrjMdlName[44] = "CJ_J_CAN2";
		PrjMdl[45] = 0xE204FA6F;		PrjMdlName[45] = "CJ_J_CAN3";
		PrjMdl[46] = 0x73329CC8;		PrjMdlName[46] = "CJ_J_CAN4";
		PrjMdl[47] = 0x7E80B364;		PrjMdlName[47] = "CJ_J_CAN5";
		PrjMdl[48] = 0x4C244EC4;		PrjMdlName[48] = "CJ_J_CAN6";
		PrjMdl[49] = 0x55F66268;		PrjMdlName[49] = "CJ_J_CAN7";
		PrjMdl[50] = 0xC8DF9328;		PrjMdlName[50] = "CJ_MATRESS_1";
		PrjMdl[51] = 0xBB24F7B3;		PrjMdlName[51] = "CJ_MATRESS_2";
		PrjMdl[52] = 0x44D226C3;		PrjMdlName[52] = "CJ_MED_SHAD_1";
		PrjMdl[53] = 0x380944B1;		PrjMdlName[53] = "CJ_MED_SINK_3";
		PrjMdl[54] = 0x4140A534;		PrjMdlName[54] = "CJ_MED_TOILET_2";
		PrjMdl[55] = 0xDCD23B8A;		PrjMdlName[55] = "CJ_MENU_1";
		PrjMdl[56] = 0xEA7956D8;		PrjMdlName[56] = "CJ_MENU_2";
		PrjMdl[57] = 0xA46D3EAC;		PrjMdlName[57] = "CJ_MICRO1";
		PrjMdl[58] = 0x577BA4C6;		PrjMdlName[58] = "CJ_MICRO2";
		PrjMdl[59] = 0x748ADF1C;		PrjMdlName[59] = "CJ_MICRO4";
		PrjMdl[60] = 0xD7396014;		PrjMdlName[60] = "CJ_MILK_4";
	}
	if (selection == 4)
	{
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Objects page 4", 5000, 1);
		PrjMdl[1] = 0x5CD48548;		PrjMdlName[1] = "CJ_MK_DRUG_BAG";
		PrjMdl[2] = 0xC4555BDF;		PrjMdlName[2] = "CJ_MK_DRUG_BAG2";
		PrjMdl[3] = 0x0ED04C49;		PrjMdlName[3] = "CJ_MOBILE_1";
		PrjMdl[4] = 0xB37B15A0;		PrjMdlName[4] = "CJ_MOBILE_3";
		PrjMdl[5] = 0x9306A8C6;		PrjMdlName[5] = "CJ_MOBILE_HAND_1";
		PrjMdl[6] = 0x5985A1D2;		PrjMdlName[6] = "BM_drum_1";
		PrjMdl[7] = 0xF1D1EE6A;		PrjMdlName[7] = "BM_drum_10";
		PrjMdl[8] = 0xC19C0DFF;		PrjMdlName[8] = "BM_drum_11";
		PrjMdl[9] = 0x8B4CA161;		PrjMdlName[9] = "BM_drum_12";
		PrjMdl[10] = 0xA28BB3D9;		PrjMdlName[10] = "BM_drum_2";
		PrjMdl[11] = 0x7D0368CD;		PrjMdlName[11] = "BM_drum_3";
		PrjMdl[12] = 0xA8B63FEE;		PrjMdlName[12] = "BM_drum_4";
		PrjMdl[13] = 0xB6E3DC49;		PrjMdlName[13] = "BM_drum_5";
		PrjMdl[14] = 0x83FF7685;		PrjMdlName[14] = "BM_drum_6";
		PrjMdl[15] = 0x9266134E;		PrjMdlName[15] = "BM_drum_7";
		PrjMdl[16] = 0xDF17ACB0;		PrjMdlName[16] = "BM_drum_8";
		PrjMdl[17] = 0xECC34807;		PrjMdlName[17] = "BM_drum_9";
		PrjMdl[18] = 0x493186E9;		PrjMdlName[18] = "BM_drum_corr";
		PrjMdl[19] = 0x8F2A7EB3;		PrjMdlName[19] = "BM_drum_exp";
		PrjMdl[20] = 0x8EFCD32A;		PrjMdlName[20] = "BM_drum_fire";
		PrjMdl[21] = 0xDDC20498;		PrjMdlName[21] = "BM_drum_fla";
		PrjMdl[22] = 0x758AD87A;		PrjMdlName[22] = "BM_drum_fla2";
		PrjMdl[23] = 0x61BBB992;		PrjMdlName[23] = "BM_drum_tox";
		PrjMdl[24] = 0x145D8395;		PrjMdlName[24] = "BM_fire_exting";
		PrjMdl[25] = 0x07861B72;		PrjMdlName[25] = "BM_fire_extingB";
		PrjMdl[26] = 0xEF269FB5;		PrjMdlName[26] = "BM_fire_extingRusA";
		PrjMdl[27] = 0xDD8BFC90;		PrjMdlName[27] = "BM_fire_extingRusB";
		PrjMdl[28] = 0x3129B913;		PrjMdlName[28] = "BM_fireaxe";
		PrjMdl[29] = 0xD413C1CE;		PrjMdlName[29] = "CJ_COM_COUCH_1";
		PrjMdl[30] = 0xCA61AE6A;		PrjMdlName[30] = "CJ_COM_COUCH_2";
		PrjMdl[31] = 0x9E9F9F86;		PrjMdlName[31] = "CJ_COMP_GATE_POST";
		PrjMdl[32] = 0x8F0E97FA;		PrjMdlName[32] = "CJ_COMP2_GATE_L";
		PrjMdl[33] = 0x13B7A14A;		PrjMdlName[33] = "CJ_COMP2_GATE_R";
		PrjMdl[34] = 0xDDDE8F4D;		PrjMdlName[34] = "CJ_COUCH10";
		PrjMdl[35] = 0xF9634656;		PrjMdlName[35] = "CJ_COUCH12";
		PrjMdl[36] = 0x06A7270B;		PrjMdlName[36] = "CJ_COUCH3";
		PrjMdl[37] = 0x7EBE173B;		PrjMdlName[37] = "CJ_COUCH5";
		PrjMdl[38] = 0xA407E1CE;		PrjMdlName[38] = "CJ_COUCH8";
		PrjMdl[39] = 0x1C9096DF;		PrjMdlName[39] = "CJ_CRATE_1";
		PrjMdl[40] = 0x4BCF755C;		PrjMdlName[40] = "CJ_CRATE_2";
		PrjMdl[41] = 0x5737FBF4;		PrjMdlName[41] = "CJ_DL_Imposter";
		PrjMdl[42] = 0x75F81419;		PrjMdlName[42] = "CJ_DL_V_Imposter";
		PrjMdl[43] = 0x1DB93AB9;		PrjMdlName[43] = "CJ_DM_SPRAYCAN";
		PrjMdl[44] = 0xECC2F072;		PrjMdlName[44] = "CJ_DOCK_BARRIER";
		PrjMdl[45] = 0xEBD7EFD8;		PrjMdlName[45] = "CJ_DOCK_CLOCK";
		PrjMdl[46] = 0x968D3CA6;		PrjMdlName[46] = "CJ_DOCK_FENCE";
		PrjMdl[47] = 0x13916478;		PrjMdlName[47] = "CJ_DOCK_HUT";
		PrjMdl[48] = 0x79DAB92E;		PrjMdlName[48] = "CJ_F1";
		PrjMdl[49] = 0x9DE40110;		PrjMdlName[49] = "CJ_F2";
		PrjMdl[50] = 0x15E8F118;		PrjMdlName[50] = "CJ_F4";
		PrjMdl[51] = 0x083055A7;		PrjMdlName[51] = "CJ_F5";
		PrjMdl[52] = 0xFB4E3BE3;		PrjMdlName[52] = "CJ_F6";
		PrjMdl[53] = 0xEBEC9D20;		PrjMdlName[53] = "CJ_F7";
		PrjMdl[54] = 0x4E806246;		PrjMdlName[54] = "CJ_F8";
		PrjMdl[55] = 0x4A4259CA;		PrjMdlName[55] = "CJ_F9";
		PrjMdl[56] = 0x18116D41;		PrjMdlName[56] = "CJ_GHETTO_BLASTER_1";
		PrjMdl[57] = 0xFA5A31D3;		PrjMdlName[57] = "CJ_GHETTO_BLASTER_2";
		PrjMdl[58] = 0xF5F716EC;		PrjMdlName[58] = "CJ_HOTEL_TROLLY";
		PrjMdl[59] = 0x154A3634;		PrjMdlName[59] = "CJ_K_LANTERN";
		PrjMdl[60] = 0x2444128B;		PrjMdlName[60] = "CJ_K_LANTERN2";
	}
	if (selection == 5)
	{
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Objects page 5", 5000, 1);
		PrjMdl[1] = 0x31892D15;		PrjMdlName[1] = "CJ_K_LANTERN3";
		PrjMdl[2] = 0x8B1C603E;		PrjMdlName[2] = "CJ_K_LANTERN5";
		PrjMdl[3] = 0x5C6B02DC;		PrjMdlName[3] = "CJ_K_LANTERN6";
		PrjMdl[4] = 0x0CDD46F8;		PrjMdlName[4] = "CJ_LAUND_BASKET";
		PrjMdl[5] = 0xF40475E7;		PrjMdlName[5] = "CJ_LAUND_BASKET_2";
		PrjMdl[6] = 0x0814606D;		PrjMdlName[6] = "CJ_LIFT";
		PrjMdl[7] = 0xB59A4625;		PrjMdlName[7] = "CJ_LIFT_2";
		PrjMdl[8] = 0xEABAD99D;		PrjMdlName[8] = "CJ_LOGO_BLOCK_1";
		PrjMdl[9] = 0xFC6F7D06;		PrjMdlName[9] = "CJ_LOGO_BLOCK_2";
		PrjMdl[10] = 0xCC6D9D03;		PrjMdlName[10] = "CJ_LOGO_BLOCK_3";
		PrjMdl[11] = 0xAE6CA564;		PrjMdlName[11] = "carstairs";
		PrjMdl[12] = 0x2A839A55;		PrjMdlName[12] = "CAUTION_sign";
		PrjMdl[13] = 0xE3AF263B;		PrjMdlName[13] = "fcked_rail01";
		PrjMdl[14] = 0x98C41066;		PrjMdlName[14] = "fcked_rail02";
		PrjMdl[15] = 0x7822CF24;		PrjMdlName[15] = "fcked_rail03";
		PrjMdl[16] = 0xBCDDD899;		PrjMdlName[16] = "fcked_rail04";
		PrjMdl[17] = 0xBCE66643;		PrjMdlName[17] = "Ramp_bx04";
		PrjMdl[18] = 0x6FC3CBFF;		PrjMdlName[18] = "Ramp_bx05";
		PrjMdl[19] = 0x6FBA0984;		PrjMdlName[19] = "Ramp_bxbay";
		PrjMdl[20] = 0xDD9970E6;		PrjMdlName[20] = "Ramp_bxbay01";
		PrjMdl[21] = 0x69ABE064;		PrjMdlName[21] = "vladsdildorail";
		PrjMdl[22] = 0x744C879A;		PrjMdlName[22] = "CJ_BIN_BAG_1";
		PrjMdl[23] = 0x82132327;		PrjMdlName[23] = "CJ_BIN_BAG_2";
		PrjMdl[24] = 0x58C35D28;		PrjMdlName[24] = "CJ_BIN_BAG_INT";
		PrjMdl[25] = 0xBF486370;		PrjMdlName[25] = "CJ_BIN_BAG_PICKUP";
		PrjMdl[26] = 0x05771EC9;		PrjMdlName[26] = "CJ_BIN_BAG_PICKUP2";
		PrjMdl[27] = 0x25CE10AC;		PrjMdlName[27] = "CJ_BM_FLAM_DRUM";
		PrjMdl[28] = 0x2ACCEFB4;		PrjMdlName[28] = "CJ_BM_HALF_CABINET";
		PrjMdl[29] = 0xE6359423;		PrjMdlName[29] = "CJ_BM_PHONE1";
		PrjMdl[30] = 0x6A800C32;		PrjMdlName[30] = "LODsandbag";
		PrjMdl[31] = 0x805A2D79;		PrjMdlName[31] = "BM_sandbag";
		PrjMdl[32] = 0x07DA99A0;		PrjMdlName[32] = "BM_sandbag20";
		PrjMdl[33] = 0xC986ADAD;		PrjMdlName[33] = "CJ_BENCH";
		PrjMdl[34] = 0x9718E067;		PrjMdlName[34] = "CJ_BENCH_2";
		PrjMdl[35] = 0x919E2330;		PrjMdlName[35] = "EC_Chopper_satnav";
		PrjMdl[36] = 0x38412DAA;		PrjMdlName[36] = "EC_DEADBINT";
		PrjMdl[37] = 0x5FAC9C5D;		PrjMdlName[37] = "EC_diamond";
		PrjMdl[38] = 0xBC28EFEA;		PrjMdlName[38] = "EC_ENGINEHOIST";
		PrjMdl[39] = 0x97C0B97E;		PrjMdlName[39] = "EC_IB_keg_01";
		PrjMdl[40] = 0x178E3917;		PrjMdlName[40] = "EC_IB_keg_02";
		PrjMdl[41] = 0x6C7AE2F3;		PrjMdlName[41] = "EC_IB_keg_03";
		PrjMdl[42] = 0x59306DE7;		PrjMdlName[42] = "EC_pint_beer1";
		PrjMdl[43] = 0x8AFAD17B;		PrjMdlName[43] = "EC_pint_beer2";
		PrjMdl[44] = 0xDAFA26DD;		PrjMdlName[44] = "EC_pint_stout1";
		PrjMdl[45] = 0xCCBD0A63;		PrjMdlName[45] = "EC_pint_stout2";
		PrjMdl[46] = 0x96185DFB;		PrjMdlName[46] = "EC_PS_Arm";
		PrjMdl[47] = 0xCAEE546C;		PrjMdlName[47] = "EC_PS_table";
		PrjMdl[48] = 0xF814AD1E;		PrjMdlName[48] = "EC_slutpole";
		PrjMdl[49] = 0xD130ADEF;		PrjMdlName[49] = "EC_Spliff";
		PrjMdl[50] = 0x4DFA9DB6;		PrjMdlName[50] = "EC_tool_cab_01";
		PrjMdl[51] = 0x227446AA;		PrjMdlName[51] = "EC_tool_cab_02";
		PrjMdl[52] = 0x699D10B2;		PrjMdlName[52] = "eeris_logo_bkn";
		PrjMdl[53] = 0x19F5881A;		PrjMdlName[53] = "el_lights";
		PrjMdl[54] = 0xD380F3BF;		PrjMdlName[54] = "el_lights01";
		PrjMdl[55] = 0xA4F316A4;		PrjMdlName[55] = "el_lights02";
		PrjMdl[56] = 0xC8203714;		PrjMdlName[56] = "el_lights02a";
		PrjMdl[57] = 0xE04C0D45;		PrjMdlName[57] = "el_lights04";
		PrjMdl[58] = 0x21EFFEE6;		PrjMdlName[58] = "BM_bong1";
		PrjMdl[59] = 0x0B2F0C17;		PrjMdlName[59] = "BM_bong10";
		PrjMdl[60] = 0xEFB61A73;		PrjMdlName[60] = "BM_bong2";
	}
	/*if (selection == 6)
	{
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Objects page 6", 5000, 1);
		
	}*/	
}
/*
void adjObjAttOffset(int fx, int fy, int fz, int fax, int fay, int faz)
{
	if (fx == 1)	{	flt[1] = flt[1] - 0.001f;	}
	if (fx == 2)	{	flt[1] = flt[1] + 0.001f;	}
	if (fy == 1)	{	flt[2] = flt[2] - 0.001f;	}
	if (fy == 2)	{	flt[2] = flt[2] + 0.001f;	}
	if (fz == 1)	{	flt[3] = flt[3] - 0.001f;	}
	if (fz == 2)	{	flt[3] = flt[3] + 0.001f;	}
	if (fax == 1)	{	flt[4] = flt[4] - 0.001f;	}
	if (fax == 2)	{	flt[4] = flt[4] + 0.001f;	}
	if (fay == 1)	{	flt[5] = flt[5] - 0.001f;	}
	if (fay == 2)	{	flt[5] = flt[5] + 0.001f;	}
	if (faz == 1)	{	flt[6] = flt[6] - 0.001f;	}
	if (faz == 2)	{	flt[6] = flt[6] + 0.001f;	}
}

void DebugText(void)
{	
	int I;
	float yPOS;
	for (I = 1; I < 7; I++)
	{
		SET_TEXT_DROPSHADOW(1, 0, 0, 0, 255);
		SET_TEXT_SCALE(0.3f, 0.3f);
		DISPLAY_TEXT_WITH_FLOAT(0.08f, (TO_FLOAT(I) / 50) + 0.049f, "NUMBR", flt[I], 4);
	}
	DRAW_CURVED_WINDOW(0.034f, 0.032f, 0.113f, 0.327f, 200);
	//DRAW_RECT(0.5f, 0.5f, 1.0f, 0.002f, 255, 255, 255, 255);
	//DRAW_RECT(0.5f, 0.5f, 0.002f, 1.0f, 255, 255, 255, 255);
}

void ReattachObject(void)
{
	if (DOES_OBJECT_EXIST(BaseObject))
	{
		DETACH_OBJECT(OLauncherMuz, 1);
		WAIT(100);
		ATTACH_OBJECT_TO_OBJECT(OLauncherMisc1, BaseObject, 0, flt[1], flt[2], flt[3], flt[4], flt[5], flt[6]);
	}
}
*/
void CreateLauncher(void)
{
	float ppedX, ppedY, ppedZ;
	SetObjects(SelObjPage);
	FREEZE_CHAR_POSITION(GetPlayerPed(), 1);
	SET_CHAR_COLLISION(GetPlayerPed(), 0);
	SET_CHAR_VISIBLE(GetPlayerPed(), 0);
	//SET_PLAYER_CONTROL(GetPlayerIndex(), 0);
	GET_CHAR_COORDINATES(GetPlayerPed(), &ppedX, &ppedY, &ppedZ);
	
	REQUEST_MODEL(0xB00ABE6D);// CJ_GOLF_BALL (invisible)
	CREATE_OBJECT(0xB00ABE6D, ppedX, ppedY, ppedZ + 1.0f, &BaseObject, 1);
	FREEZE_OBJECT_POSITION(BaseObject, 1);
	SET_OBJECT_COLLISION(BaseObject, 0);
	SET_OBJECT_VISIBLE(BaseObject, 0);
	
	REQUEST_MODEL(0xB00ABE6D);// CJ_GOLF_BALL (invisible)
	CREATE_OBJECT(0xB00ABE6D, ppedX, ppedY, ppedZ + 1.0f, &OLauncherMuz, 1);
	FREEZE_OBJECT_POSITION(OLauncherMuz, 1);
	SET_OBJECT_COLLISION(OLauncherMuz, 0);
	SET_OBJECT_VISIBLE(OLauncherMuz, 0);
	ATTACH_OBJECT_TO_OBJECT(OLauncherMuz, BaseObject, 0, 0.0f, 0.541f, -0.079f, 0.0f, 0.0f, 0.0f);
	
	REQUEST_MODEL(0x579384A4);// CJ_PROP_RPG
	CREATE_OBJECT(0x579384A4, ppedX, ppedY, ppedZ + 1.0f, &OLauncher, 1);
	FREEZE_OBJECT_POSITION(OLauncher, 1);
	SET_OBJECT_COLLISION(OLauncher, 0);
	//SET_OBJECT_ROTATION(OLauncher, olP, 0.0, olH);
	ATTACH_OBJECT_TO_OBJECT(OLauncher, BaseObject, 0, 0.031f, 0.211f, -0.168f, 0.0f, -0.09f, 1.586f);

	REQUEST_MODEL(0x1B42315D);// CJ_HIPPO_BIN
	CREATE_OBJECT(0x1B42315D, ppedX, ppedY, ppedZ + 1.0f, &OLauncherMisc1, 1);
	FREEZE_OBJECT_POSITION(OLauncherMisc1, 1);
	SET_OBJECT_COLLISION(OLauncherMisc1, 0);
	//SET_OBJECT_ROTATION(OLauncherMisc1, olP, 0.0, olH);
	ATTACH_OBJECT_TO_OBJECT(OLauncherMisc1, BaseObject, 0, -0.5f, 0.0f, -0.653f, 0.0f, 0.0f, -3.149f);
/*
	REQUEST_MODEL(0x_);// CJ_HIPPO_BIN
	CREATE_OBJECT(0x_, ppedX, ppedY, ppedZ + 1.0f, &_, 1);
	FREEZE_OBJECT_POSITION(_, 1);
	SET_OBJECT_COLLISION(_, 0);
	ATTACH_OBJECT_TO_OBJECT(_, BaseObject, 0, flt[1], flt[2], flt[3], flt[4], flt[5], flt[6]);
*/
}

void AttachCam(bool doAttach)
{
	if (doAttach)
	{
		CREATE_CAM(14, &ObjLaunchCam);
		SET_CAM_ACTIVE(ObjLaunchCam, 1);
		SET_CAM_PROPAGATE(ObjLaunchCam, 1);
		ACTIVATE_SCRIPTED_CAMS(1, 1);
		ATTACH_CAM_TO_OBJECT(ObjLaunchCam, BaseObject);
	}
	else
	{
		SET_CAM_ACTIVE(ObjLaunchCam, 0);
		SET_CAM_PROPAGATE(ObjLaunchCam, 0);
		ACTIVATE_SCRIPTED_CAMS(0, 0);
		UNATTACH_CAM(ObjLaunchCam);
		DESTROY_CAM(ObjLaunchCam);
		while (DOES_CAM_EXIST(ObjLaunchCam))
		{
			WAIT(0);
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "debug wait cam destroy", 10, 1);
		}
		SET_CAM_BEHIND_PED(GetPlayerPed());
	}
}

void DestroyLauncher(void)
{
	float olX, olY, olZ;
	GET_OBJECT_COORDINATES(BaseObject, &olX, &olY, &olZ);
	DETACH_OBJECT(OLauncher, 1);
	MARK_OBJECT_AS_NO_LONGER_NEEDED(&OLauncher);
	MARK_OBJECT_AS_NO_LONGER_NEEDED(&OLauncherMuz);
	MARK_OBJECT_AS_NO_LONGER_NEEDED(&OLauncherMisc1);
	MARK_OBJECT_AS_NO_LONGER_NEEDED(&BaseObject);
	CLEAR_AREA_OF_OBJECTS(olX, olY, olZ, 10.0f);
	//SET_PLAYER_CONTROL(GetPlayerIndex(), 1);
	FREEZE_CHAR_POSITION(GetPlayerPed(), 0);
	SET_CHAR_COLLISION(GetPlayerPed(), 1);
	SET_CHAR_VISIBLE(GetPlayerPed(), 1);
}

void MoveCamera(void)
{
	int laX, laY, raX, raY;										// for storing analog stick data
	float maxPitch = 90.0f, minPitch = -90.0f;					// sets the min and max pitch
	float olX, olY, olZ, olT;									// base object coords + 4th var for trig stuffs
	float f_incr_LX, f_incr_LY, f_incr_RX, f_incr_RY;			// for tweaking - can be avoided by combining formulas
	float fltL2, fltR2;											// for tweaking - can be avoided by combining formulas
	
	GET_OBJECT_COORDINATES(BaseObject, &olX, &olY, &olZ);		// get object starting coordinates
	GET_POSITION_OF_ANALOGUE_STICKS(0, &laX, &laY, &raX, &raY);	// get analog stick data
	
	fltL2 = (TO_FLOAT(GET_CONTROL_VALUE(0, 6))) / 400.0f;		// for tweaking - can be avoided by combining formulas
	fltR2 = (TO_FLOAT(GET_CONTROL_VALUE(0, 5))) / 400.0f;		// for tweaking - can be avoided by combining formulas
	f_incr_LX = (TO_FLOAT(laX)) / 150.0f;						// for tweaking - can be avoided by combining formulas
	f_incr_LY = (TO_FLOAT(laY)) / 100.0f;						// for tweaking - can be avoided by combining formulas
	f_incr_RX = (TO_FLOAT(raX)) / 12.0f;						// for tweaking - can be avoided by combining formulas
	f_incr_RY = (TO_FLOAT(raY)) / 20.0f;						// for tweaking - can be avoided by combining formulas
	
	olP = olP - f_incr_RY;										// adjusts pitch
	if (olP < minPitch)	{	olP = minPitch;	}					// keeps pitch above min
	if (olP > maxPitch)	{	olP = maxPitch;	}					// keeps pitch below max
	
	olH = olH - f_incr_RX;										// adjusts heading
	if (olH > 180.0f)	{	olH = olH - 360.0f;	}				// keeps rot looped between 180 and -180
	if (olH < -180.0f)	{	olH = olH + 360.0f;	}				// keeps rot looped between 180 and -180
	
	olT = (f_incr_LY * COS(olP));								// uses the pitch angle and the forward increment as a hypotenuse to calulate the adjacent side length to be used below
	olX = olX + ((olT * SIN(olH)) + (f_incr_LX * COS(olH)));	// calculates how far to move along the maps X axis
	olY = olY - ((olT * COS(olH)) - (f_incr_LX * SIN(olH)));	// calculates how far to move along the maps Y axis
	olZ = olZ - (f_incr_LY * SIN(olP)) - fltL2 + fltR2;			// calculates how far to move along the maps Z axis - includes input from L2 and R2
	
	SET_OBJECT_COORDINATES(BaseObject, olX, olY, olZ);			// set object end coordinates
	SET_OBJECT_ROTATION(BaseObject, olP - 20.0f, 0.0f, olH);			// set object rot to the pitch and heading in olP and olH
	SET_CAM_ROT(ObjLaunchCam, olP - 20.0f, 0.0f, olH);					// set camera rot as the camera only follows the object xyz - maybe possible to avoid?
	
	SET_CHAR_COORDINATES_NO_OFFSET(GetPlayerPed(), olX, olY, olZ);		// set player position and heading
	if (olH < 0)
	{
		SET_CHAR_HEADING(GetPlayerPed(), olH + 360.0f);
	}
	else
	{
		SET_CHAR_HEADING(GetPlayerPed(), olH);
	}
}

void ShootObject(void)
{
	float muzX, muzY, muzZ;
	GET_OBJECT_COORDINATES(OLauncherMuz, &muzX, &muzY, &muzZ);
	REQUEST_MODEL(PrjMdl[SelPrj]);
	while(!HAS_MODEL_LOADED(PrjMdl[SelPrj]))	{	WAIT(0);	}
	CREATE_OBJECT(PrjMdl[SelPrj], muzX, muzY, muzZ, &ObjProjectile, 1);
	while(!DOES_OBJECT_EXIST(ObjProjectile))	{	WAIT(0);	}
	/*SET_OBJECT_DYNAMIC(ObjProjectile, 1);
	SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(ObjProjectile, 1);
	FREEZE_OBJECT_POSITION(ObjProjectile, 0);*/
	SET_OBJECT_ROTATION(ObjProjectile, olP - 20.0f, 0.0f, olH);
	while(IS_OBJECT_STATIC(ObjProjectile))
	{
		WAIT(0);
		SET_OBJECT_DYNAMIC(ObjProjectile, 1);
		if (IS_BUTTON_PRESSED(0,STICK_L) && IS_BUTTON_PRESSED(0,STICK_R))
		{
			break;
			//TERMINATE_THIS_SCRIPT();
		}
	}
	//WAIT(150);
	APPLY_FORCE_TO_OBJECT(ObjProjectile, 1, 0.0f, TO_FLOAT(objForceInt), 0.0f, 0.0f, 0.0f, 0.0f, 1, 1, 1, 1);
	/*if (ObjectOnFire == 1)
	{
		ObjPtfx = START_PTFX_ON_OBJ("fire_object_flames", ObjProjectile, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Fire!", 5000, 1);
	}*/
	MARK_OBJECT_AS_NO_LONGER_NEEDED(&ObjProjectile);
	PLAY_AUDIO_EVENT_FROM_OBJECT("G5_SEWAGE_SHOOTOUT_CASE_LAND", OLauncherMuz);
}

void ShootObjectPedMode(void)
{
	float muzX, muzY, muzZ;
	GET_OBJECT_COORDINATES(OLauncherMuz, &muzX, &muzY, &muzZ);
	if(DOES_OBJECT_EXIST(ObjProjectile))
	{
		FREEZE_OBJECT_POSITION(ObjProjectile, 1);
		SET_OBJECT_COORDINATES(ObjProjectile, muzX, muzY, muzZ);
		FREEZE_OBJECT_POSITION(ObjProjectile, 0);
		SET_OBJECT_ROTATION(ObjProjectile, olP - 20.0f, 0.0f, olH);
		APPLY_FORCE_TO_OBJECT(ObjProjectile, 1, 0.0f, TO_FLOAT(objForceInt), 0.0f, 0.0f, 0.0f, 0.0f, 1, 1, 1, 1);
		/*if (ObjectOnFire == 1)
		{
			ObjPtfx = START_PTFX_ON_OBJ("fire_object_flames", ObjProjectile, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Fire!", 5000, 1);
		}*/
		PLAY_AUDIO_EVENT_FROM_OBJECT("G5_SEWAGE_SHOOTOUT_CASE_LAND", OLauncherMuz);
	}
	else
	{
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "No object to shoot", 5000, 1);
	}
}

void SpawnObjectPedMode(void)
{
	float muzX, muzY, muzZ;
	GET_OBJECT_COORDINATES(OLauncherMuz, &muzX, &muzY, &muzZ);
	REQUEST_MODEL(PrjMdl[SelPrj]);
	while(!HAS_MODEL_LOADED(PrjMdl[SelPrj]))
	{
		WAIT(0);
	}
	CREATE_OBJECT(PrjMdl[SelPrj], muzX, muzY, muzZ, &ObjProjectile, 1);
	while(!DOES_OBJECT_EXIST(ObjProjectile))
	{
		WAIT(0);
	}
	SET_OBJECT_AS_STEALABLE(ObjProjectile, 1);
	SET_OBJECT_DYNAMIC(ObjProjectile, 1);
	GIVE_PED_PICKUP_OBJECT(GetPlayerPed(), ObjProjectile, 1);
	WAIT(1000);
	FORCE_CHAR_TO_DROP_WEAPON(GetPlayerPed());
	SET_OBJECT_VISIBLE(ObjProjectile, 1);
}

void RemoveObject(void)
{
	float muzX, muzY, muzZ;
	GET_OBJECT_COORDINATES(OLauncherMuz, &muzX, &muzY, &muzZ);
	/*if (ObjectOnFire == 1)
	{
	REMOVE_PTFX_FROM_OBJECT(ObjProjectile);
	}*/
	CLEAR_AREA_OF_OBJECTS(muzX, muzY, muzZ, 100.0);
	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Objects Cleared", 5000, 1);
}

void RemoveObjectPedMode(void)
{
	if (DOES_OBJECT_EXIST(ObjProjectile))
	{
		/*if (ObjectOnFire == 1)
		{
			REMOVE_PTFX_FROM_OBJECT(ObjProjectile);
		}*/
		DELETE_OBJECT(&ObjProjectile);
		MARK_MODEL_AS_NO_LONGER_NEEDED(PrjMdl[SelPrj]);
	}
}

void AdjustForce(bool incr_f)
{
	if (incr_f)
	{
		if (objForceInt < 1000)
		{
			objForceInt = objForceInt + 1;
		}
	}
	else
	{
		if (objForceInt > 0)
		{
			objForceInt = objForceInt - 1;
		}
	}
}

void SelectModel(bool next)
{
	if (next)
	{
		if (SelPrj == 60)
		{
			SelPrj = 1;
		}
		else
		{
			SelPrj = SelPrj + 1;
		}
	}
	else
	{
		if (SelPrj == 1)
		{
			SelPrj = 60;
		}
		else
		{
			SelPrj = SelPrj - 1;
		}
	}
	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", PrjMdlName[SelPrj], 5000, 1);
}
/*
void ToggleFire(bool Firemode)
{
	if (Firemode)
	{
		if (ObjectOnFire != 1)
		{
			ObjectOnFire = 1;
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Fire Mode enabled", 5000, 1);
		}
	}
	else
	{
		if (ObjectOnFire != 0)
		{
			ObjectOnFire = 0;
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Fire Mode disabled", 5000, 1);
		}
	}
}
*/
void TogglePed(bool Pedmode)
{
	if (Pedmode)
	{
		if (PedEnabled != 1)
		{
			PedEnabled = 1;
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Ped Mode enabled", 5000, 1);
		}
	}
	else
	{
		if (PedEnabled != 0)
		{
			PedEnabled = 0;
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Ped Mode disabled", 5000, 1);
		}
	}
}

void ToggleTurbo(bool Turbomode)
{
	if (Turbomode)
	{
		if (RapidLaunch != 1)
		{
			RapidLaunch = 1;
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Rapid Fire enabled", 5000, 1);
		}
	}
	else
	{
		if (RapidLaunch != 0)
		{
			RapidLaunch = 0;
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Rapid Fire disabled", 5000, 1);
		}
	}
}

void ButtonInput(void)
{
	if (IS_BUTTON_PRESSED(0,L1) && IS_BUTTON_JUST_PRESSED(0,DPAD_DOWN))				// toggle on / off
	{
		if (BaseObjectActive == 0)
		{
			CreateLauncher();
			AttachCam(1);
			BaseObjectActive = 1;
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Activated.", 2000, 1);
		}
		else
		{
			AttachCam(0);
			DestroyLauncher();
			BaseObjectActive = 0;
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Deactivated.", 2000, 1);
		}
	}
	if (BaseObjectActive == 1)
	{
		if (IS_BUTTON_PRESSED(0,X) && IS_BUTTON_JUST_PRESSED(0,DPAD_UP))			// next page of objects
		{
			if (PedEnabled == 1)	{	RemoveObjectPedMode();	}
			if (SelObjPage == 5)	{	SelObjPage = 1;	}
			else	{	SelObjPage++;	}
			SetObjects(SelObjPage);
		}
		if (IS_BUTTON_PRESSED(0,X) && IS_BUTTON_JUST_PRESSED(0,DPAD_DOWN))			// previous page of objects
		{
			if (PedEnabled == 1)	{	RemoveObjectPedMode();	}
			if (SelObjPage == 1)	{	SelObjPage = 5;	}
			else	{	SelObjPage--;	}
			SetObjects(SelObjPage);
		}
		if (IS_BUTTON_PRESSED(0,X) && IS_BUTTON_JUST_PRESSED(0,DPAD_RIGHT))			// next object
		{
			if (PedEnabled == 1)	{	RemoveObjectPedMode();	}
			SelectModel(1);
		}
		if (IS_BUTTON_PRESSED(0,X) && IS_BUTTON_JUST_PRESSED(0,DPAD_LEFT))			// previous object
		{
			if (PedEnabled == 1)	{	RemoveObjectPedMode();	}
			SelectModel(0);
		}
		if (IS_BUTTON_PRESSED(0,SQUARE) && IS_BUTTON_PRESSED(0,DPAD_UP))			// increase force
		{
			AdjustForce(1);
		}
		if (IS_BUTTON_PRESSED(0,SQUARE) && IS_BUTTON_PRESSED(0,DPAD_DOWN))			// decrease force
		{
			AdjustForce(0);
		}
		if (IS_BUTTON_PRESSED(0,STICK_L) && IS_BUTTON_JUST_PRESSED(0,TRIANGLE))		// toggle ped mode
		{
			if (PedEnabled == 0)
			{
				RemoveObject();
				TogglePed(1);
			}
			else
			{
				RemoveObjectPedMode();
				TogglePed(0);
			}
		}/*
		if (IS_BUTTON_PRESSED(0,STICK_L) && IS_BUTTON_JUST_PRESSED(0,SQUARE))		// toggle fire mode
		{
			if (ObjectOnFire == 0)
			{
				ToggleFire(1);
			}
			else
			{
				ToggleFire(0);
			}
		}*/
		if (PedEnabled == 1)														// -------- controls for ped mode --------
		{
			if (IS_BUTTON_JUST_PRESSED(0,R1))											// shoot object
			{
				ShootObjectPedMode();
			}
			if (IS_BUTTON_JUST_PRESSED(0,X))											// spawn object
			{
				RemoveObjectPedMode();
				SpawnObjectPedMode();
			}
			if (IS_BUTTON_JUST_PRESSED(0,CIRCLE))										// remove object
			{
				RemoveObjectPedMode();
			}
		}
		if (PedEnabled == 0)														// -------- controls for spam mode --------
		{			
			if (IS_BUTTON_PRESSED(0,STICK_L) && IS_BUTTON_JUST_PRESSED(0,X))			// toggle rapid fire
			{
				if (RapidLaunch == 0)
				{
					ToggleTurbo(1);
				}
				else
				{
					ToggleTurbo(0);
				}
			}
			if (RapidLaunch == 1)														// shoot if rapid
			{
				if (IS_BUTTON_PRESSED(0,R1))
				{
					ShootObject();
				}
			}
			if (RapidLaunch == 0)														// shoot if not rapid
			{
				if (IS_BUTTON_JUST_PRESSED(0,R1))
				{
					ShootObject();
				}
			}
			if (IS_BUTTON_JUST_PRESSED(0,CIRCLE))										// clear objects
			{
				RemoveObject();
			}
		}
	}
/*
	if (IS_BUTTON_PRESSED(0,X) && IS_BUTTON_JUST_PRESSED(0,DPAD_RIGHT))
	{
		if (BaseObjectActive == 1)
		{
			AttachCam(1);
		}
	}
	if (IS_BUTTON_PRESSED(0,X) && IS_BUTTON_JUST_PRESSED(0,DPAD_LEFT))
	{
		if (BaseObjectActive == 1)
		{
			AttachCam(0);
		}
	}
	if (IS_BUTTON_PRESSED(0,L1) && IS_BUTTON_PRESSED(0,R1))
	{
		if (BaseObjectActive == 1)
		{
			ReattachObject();
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Object Reattached", 10, 1);
		}
	}
	if (IS_BUTTON_PRESSED(0, SQUARE) && IS_BUTTON_PRESSED(0, DPAD_UP))
	{
		adjObjAttOffset(2, 0, 0, 0, 0, 0);
	}
	if (IS_BUTTON_PRESSED(0, SQUARE) && IS_BUTTON_PRESSED(0, DPAD_DOWN))
	{
		adjObjAttOffset(1, 0, 0, 0, 0, 0);
	}
	if (IS_BUTTON_PRESSED(0, TRIANGLE) && IS_BUTTON_PRESSED(0, DPAD_UP))
	{
		adjObjAttOffset(0, 2, 0, 0, 0, 0);
	}
	if (IS_BUTTON_PRESSED(0, TRIANGLE) && IS_BUTTON_PRESSED(0, DPAD_DOWN))
	{
		adjObjAttOffset(0, 1, 0, 0, 0, 0);
	}
	if (IS_BUTTON_PRESSED(0, CIRCLE) && IS_BUTTON_PRESSED(0, DPAD_UP))
	{
		adjObjAttOffset(0, 0, 2, 0, 0, 0);
	}
	if (IS_BUTTON_PRESSED(0, CIRCLE) && IS_BUTTON_PRESSED(0, DPAD_DOWN))
	{
		adjObjAttOffset(0, 0, 1, 0, 0, 0);
	}
	if (IS_BUTTON_PRESSED(0, SQUARE) && IS_BUTTON_PRESSED(0, DPAD_LEFT))
	{
		adjObjAttOffset(0, 0, 0, 2, 0, 0);
	}
	if (IS_BUTTON_PRESSED(0, SQUARE) && IS_BUTTON_PRESSED(0, DPAD_RIGHT))
	{
		adjObjAttOffset(0, 0, 0, 1, 0, 0);
	}
	if (IS_BUTTON_PRESSED(0, TRIANGLE) && IS_BUTTON_PRESSED(0, DPAD_LEFT))
	{
		adjObjAttOffset(0, 0, 0, 0, 2, 0);
	}
	if (IS_BUTTON_PRESSED(0, TRIANGLE) && IS_BUTTON_PRESSED(0, DPAD_RIGHT))
	{
		adjObjAttOffset(0, 0, 0, 0, 1, 0);
	}
	if (IS_BUTTON_PRESSED(0, CIRCLE) && IS_BUTTON_PRESSED(0, DPAD_LEFT))
	{
		adjObjAttOffset(0, 0, 0, 0, 0, 2);
	}
	if (IS_BUTTON_PRESSED(0, CIRCLE) && IS_BUTTON_PRESSED(0, DPAD_RIGHT))
	{
		adjObjAttOffset(0, 0, 0, 0, 0, 1);
	}
*/
}

void main(void)
{
	while(true)
	{
		WAIT(0);
		if (BaseObjectActive == 1)
		{
			MoveCamera();
			SET_TEXT_DROPSHADOW(1, 0, 0, 0, 255);
			SET_TEXT_SCALE(0.3f, 0.3f);
			DISPLAY_TEXT_WITH_NUMBER(0.832f, 0.069f, "NUMBR", objForceInt);
		}
		ButtonInput();
		//DebugText();
	}
}