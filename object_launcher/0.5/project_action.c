#pragma once

void project_doAction(void)
{
	menu_items_set = true;

	// Helpers
	uint num_val_selected = menu_item[item_selected].num_val;
	uint mainMenu = last_selected[1];
	uint subMenu = last_selected[2]; 

	if (menu_level == 1)
	{
		if (item_selected == 2)
		{
			if (pedmode_enabled)
			{
				menu_item[item_selected].extra_val = false;
			}
			else
			{
				if (rapidmode_enabled)
				{
					project_error(OBJL_ERROR_ID_RAPID);
				}
				else
				{
					menu_item[item_selected].extra_val = true;
				}
			}
			pedmode_enabled = menu_item[item_selected].extra_val;
			return;
		}
		else if (item_selected == 3)
		{
			// Adjust Force
			objl_force = menu_item[item_selected].num_val;
			return;
		}
		else if (item_selected == 5)
		{
			// Toggle Enabled
			if (objl_enabled)
			{
				menu_item[item_selected].extra_val = false;
				AttachCam(0);
				if (objl_model_enabled);
				{
					CreateLauncherExtra(0);
				}
				CreateLauncher(0);
			}
			else
			{
				menu_item[item_selected].extra_val = true;
				CreateLauncher(1);
				if (objl_model_enabled);
				{
					CreateLauncherExtra(1);
				}
				AttachCam(1);
			}
			objl_enabled = menu_item[item_selected].extra_val;
			return;
		}
		else if (item_selected == 6)
		{
			// Exit
			disableMenu = true;
			menu_level = 0;
			return;
		}
	}
	else if (mainMenu == 1)
	{
		if (menu_level == 3)
		{
			if (pedmode_enabled)
			{
				RemoveObjectPedMode();
			}
			if (IS_MODEL_IN_CDIMAGE(num_val_selected))
			{
				objl_model = num_val_selected;
			}
			else
			{
				project_error(OBJL_ERROR_ID_CDIMAGE);
			}
			
			return;
		}
	}
	else if (mainMenu == 4)
	{
		if (menu_level == 2)
		{
			if (item_selected == 1)
			{
				if (objl_model_enabled)
				{
					menu_item[item_selected].extra_val = false;
				}
				else
				{
					menu_item[item_selected].extra_val = true;
				}
				objl_model_enabled = menu_item[item_selected].extra_val;
				return;
			}
			else if (item_selected == 2)
			{
				if (objl_sights_enabled)
				{
					menu_item[item_selected].extra_val = false;
				}
				else
				{
					menu_item[item_selected].extra_val = true;
				}
				objl_sights_enabled = menu_item[item_selected].extra_val;
				return;
			}
			else if (item_selected == 3)
			{
				if (pedmode_enabled)
				{
					project_error(OBJL_ERROR_ID_PED);
				}
				else
				{
					if (rapidmode_enabled)
					{
						menu_item[item_selected].extra_val = false;
					}
					else
					{
						menu_item[item_selected].extra_val = true;
					}
					rapidmode_enabled = menu_item[item_selected].extra_val;
				}
				return;
			}
			else if (item_selected == 4)
			{
				if (slomo_enabled)
				{
					menu_item[item_selected].extra_val = false;
				}
				else
				{
					menu_item[item_selected].extra_val = true;
				}
				slomo_enabled = menu_item[item_selected].extra_val;
				return;
			}
			else if (item_selected == 5)
			{
				slo_mo = menu_item[item_selected].float_val;
				return;
			}
		}
	}

	project_error(OBJL_ERROR_ID_INVALID);
}