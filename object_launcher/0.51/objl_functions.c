
void objl_movement(void)
{
	int laX, laY, raX, raY;										// for storing analog stick data
	float maxPitch = 90.0f, minPitch = -90.0f;					// sets the min and max pitch
	float olX, olY, olZ, olT;									// base object coords + 4th var for trig stuffs
	float f_incr_LX, f_incr_LY, f_incr_RX, f_incr_RY;			// for tweaking - can be avoided by combining formulas
	float fltL2, fltR2;											// for tweaking - can be avoided by combining formulas
	
	GET_OBJECT_COORDINATES(objl_base, &olX, &olY, &olZ);		// get object starting coordinates
	GET_POSITION_OF_ANALOGUE_STICKS(0, &laX, &laY, &raX, &raY);	// get analog stick data
	
	fltL2 = (TO_FLOAT(GET_CONTROL_VALUE(0, 6))) / 400.0f;		// for tweaking - can be avoided by combining formulas
	fltR2 = (TO_FLOAT(GET_CONTROL_VALUE(0, 5))) / 400.0f;		// for tweaking - can be avoided by combining formulas
	f_incr_LX = (TO_FLOAT(laX)) / 150.0f;						// for tweaking - can be avoided by combining formulas
	f_incr_LY = (TO_FLOAT(laY)) / 100.0f;						// for tweaking - can be avoided by combining formulas
	f_incr_RX = (TO_FLOAT(raX)) / 12.0f;						// for tweaking - can be avoided by combining formulas
	f_incr_RY = (TO_FLOAT(raY)) / 20.0f;						// for tweaking - can be avoided by combining formulas
	
	olP = olP - f_incr_RY;										// adjusts pitch
	if (olP < minPitch)	{	olP = minPitch;	}					// keeps pitch above min
	if (olP > maxPitch)	{	olP = maxPitch;	}					// keeps pitch below max
	
	olH = olH - f_incr_RX;										// adjusts heading
	if (olH > 180.0f)	{	olH = olH - 360.0f;	}				// keeps rot looped between 180 and -180
	if (olH < -180.0f)	{	olH = olH + 360.0f;	}				// keeps rot looped between 180 and -180
	
	olT = (f_incr_LY * COS(olP));								// uses the pitch angle and the forward increment as a hypotenuse to calulate the adjacent side length to be used below
	olX = olX + ((olT * SIN(olH)) + (f_incr_LX * COS(olH)));	// calculates how far to move along the maps X axis
	olY = olY - ((olT * COS(olH)) - (f_incr_LX * SIN(olH)));	// calculates how far to move along the maps Y axis
	olZ = olZ - (f_incr_LY * SIN(olP)) - fltL2 + fltR2;			// calculates how far to move along the maps Z axis - includes input from L2 and R2
	
	SET_OBJECT_COORDINATES(objl_base, olX, olY, olZ);			// set object end coordinates
	SET_OBJECT_ROTATION(objl_base, olP - 20.0f, 0.0f, olH);	// set object rot to the pitch and heading in olP and olH
	SET_CAM_ROT(objl_cam, olP - 20.0f, 0.0f, olH);			// set camera rot as the camera only follows the object xyz - maybe possible to avoid?
	
	SET_CHAR_COORDINATES_NO_OFFSET(GetPlayerPed(), olX, olY, olZ);	// set player position and heading
	if (olH < 0)													// convert -180/180 (rot) to 0/360 (heading)
	{
		SET_CHAR_HEADING(GetPlayerPed(), olH + 360.0f);
	}
	else
	{
		SET_CHAR_HEADING(GetPlayerPed(), olH);
	}
}

void CreateLauncher(bool create)
{
	float cX, cY, cZ;
	if (create)
	{
		FREEZE_CHAR_POSITION(GetPlayerPed(), 1);
		SET_CHAR_COLLISION(GetPlayerPed(), 0);
		SET_CHAR_VISIBLE(GetPlayerPed(), 0);
		GET_CHAR_COORDINATES(GetPlayerPed(), &cX, &cY, &cZ);
		
		REQUEST_MODEL(0xB00ABE6D);											// CJ_GOLF_BALL
		CREATE_OBJECT(0xB00ABE6D, cX, cY, cZ + 1.0f, &objl_base, 1);		// base object everything anchors to
		FREEZE_OBJECT_POSITION(objl_base, 1);
		SET_OBJECT_COLLISION(objl_base, 0);
		SET_OBJECT_VISIBLE(objl_base, 0);									// always invisible
		
		REQUEST_MODEL(0xB00ABE6D);											// CJ_GOLF_BALL
		CREATE_OBJECT(0xB00ABE6D, cX, cY, cZ + 1.0f, &objl_muz, 1);			// object used to get muzzle coordinates
		FREEZE_OBJECT_POSITION(objl_muz, 1);
		SET_OBJECT_COLLISION(objl_muz, 0);
		SET_OBJECT_VISIBLE(objl_muz, 0);									// always invisible
		ATTACH_OBJECT_TO_OBJECT(objl_muz, objl_base, 0, 0.0f, 0.541f, -0.079f, 0.0f, 0.0f, 0.0f);
	}
	else
	{
		GET_OBJECT_COORDINATES(objl_base, &cX, &cY, &cZ);
		DETACH_OBJECT(objl_muz, 1);
		MARK_OBJECT_AS_NO_LONGER_NEEDED(&objl_muz);
		MARK_OBJECT_AS_NO_LONGER_NEEDED(&objl_base);
		
		CLEAR_AREA_OF_OBJECTS(cX, cY, cZ, 10.0f);
		
		FREEZE_CHAR_POSITION(GetPlayerPed(), 0);
		SET_CHAR_COLLISION(GetPlayerPed(), 1);
		SET_CHAR_VISIBLE(GetPlayerPed(), 1);
	}
}

void CreateLauncherExtra(bool create)
{
	float cX, cY, cZ;
	GET_OBJECT_COORDINATES(objl_base, &cX, &cY, &cZ);
	if (create)
	{
		REQUEST_MODEL(0x579384A4);										// CJ_PROP_RPG
		CREATE_OBJECT(0x579384A4, cX, cY, cZ + 1.0f, &objl_rpg, 1);		// object to mimic rpg launcher
		FREEZE_OBJECT_POSITION(objl_rpg, 1);
		SET_OBJECT_COLLISION(objl_rpg, 0);
		ATTACH_OBJECT_TO_OBJECT(objl_rpg, objl_base, 0, 0.031f, 0.211f, -0.168f, 0.0f, -0.09f, 1.586f);

		REQUEST_MODEL(0x1B42315D);										// CJ_HIPPO_BIN
		CREATE_OBJECT(0x1B42315D, cX, cY, cZ + 1.0f, &objl_misc1, 1);	// for the lols
		FREEZE_OBJECT_POSITION(objl_misc1, 1);
		SET_OBJECT_COLLISION(objl_misc1, 0);
		ATTACH_OBJECT_TO_OBJECT(objl_misc1, objl_base, 0, -0.5f, 0.0f, -0.653f, 0.0f, 0.0f, -3.149f);
	}
	else
	{
		DETACH_OBJECT(objl_rpg, 1);
		DETACH_OBJECT(objl_misc1, 1);
		MARK_OBJECT_AS_NO_LONGER_NEEDED(&objl_rpg);
		MARK_OBJECT_AS_NO_LONGER_NEEDED(&objl_misc1);
		
		CLEAR_AREA_OF_OBJECTS(cX, cY, cZ, 10.0f);
	}
}

void AttachCam(bool doAttach)
{
	if (doAttach)
	{
		CREATE_CAM(14, &objl_cam);
		SET_CAM_ACTIVE(objl_cam, 1);
		SET_CAM_PROPAGATE(objl_cam, 1);
		ACTIVATE_SCRIPTED_CAMS(1, 1);
		ATTACH_CAM_TO_OBJECT(objl_cam, objl_base);
	}
	else
	{
		SET_CAM_ACTIVE(objl_cam, 0);
		SET_CAM_PROPAGATE(objl_cam, 0);
		ACTIVATE_SCRIPTED_CAMS(0, 0);
		UNATTACH_CAM(objl_cam);
		DESTROY_CAM(objl_cam);
		while (DOES_CAM_EXIST(objl_cam))	{	WAIT(0);	}
		SET_CAM_BEHIND_PED(GetPlayerPed());
	}
}

void ShootObject(void)
{
	char* launch_sfx = "V2_HUNG_OUT_TO_DRY_BASKET_HIT";
	
	GET_OBJECT_COORDINATES(objl_muz, &muzX, &muzY, &muzZ);
	REQUEST_MODEL(objl_model);
	while(!HAS_MODEL_LOADED(objl_model))	{	WAIT(0);	}
	CREATE_OBJECT(objl_model, muzX, muzY, muzZ, &objl_object, 1);
	while(!DOES_OBJECT_EXIST(objl_object))	{	WAIT(0);	}
	SET_OBJECT_ROTATION(objl_object, olP - 20.0f, 0.0f, olH);
	while(IS_OBJECT_STATIC(objl_object))
	{
		WAIT(0);
		SET_OBJECT_DYNAMIC(objl_object, 1);
		if(IS_BUTTON_PRESSED(0, BUTTON_O))
		{
			break;
		}
	}
	APPLY_FORCE_TO_OBJECT(objl_object, 1, 0.0f, TO_FLOAT(objl_force) * 10, 0.0f, 0.0f, 0.0f, 0.0f, 1, 1, 1, 1);
	MARK_OBJECT_AS_NO_LONGER_NEEDED(&objl_object);
	PLAY_AUDIO_EVENT_FROM_OBJECT(launch_sfx, objl_muz);
}

void ShootObjectPedMode(void)
{
	char* launch_sfx = "V2_HUNG_OUT_TO_DRY_BASKET_HIT";
	
	if (!DOES_OBJECT_EXIST(objl_object))
	{
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "No object found, spawning...", 3000, 1);
		REQUEST_MODEL(objl_model);
		while(!HAS_MODEL_LOADED(objl_model))	{	WAIT(0);	}
		CREATE_OBJECT(objl_model, muzX, muzY, muzZ, &objl_object, 1);
		while(!DOES_OBJECT_EXIST(objl_object))	{	WAIT(0);	}
		SET_OBJECT_AS_STEALABLE(objl_object, 1);
		SET_OBJECT_DYNAMIC(objl_object, 1);
		GIVE_PED_PICKUP_OBJECT(GetPlayerPed(), objl_object, 1);
		WAIT(500);
		FORCE_CHAR_TO_DROP_WEAPON(GetPlayerPed());
		SET_OBJECT_VISIBLE(objl_object, 1);
		WAIT(200);
	}
	if (!DOES_OBJECT_EXIST(objl_object))
	{
		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Error! Object must have been destroyed.", 3000, 1);
	}
	else
	{
		GET_OBJECT_COORDINATES(objl_muz, &muzX, &muzY, &muzZ);
		FREEZE_OBJECT_POSITION(objl_object, 1);
		SET_OBJECT_COORDINATES(objl_object, muzX, muzY, muzZ);
		FREEZE_OBJECT_POSITION(objl_object, 0);
		SET_OBJECT_ROTATION(objl_object, olP - 20.0f, 0.0f, olH);
		APPLY_FORCE_TO_OBJECT(objl_object, 1, 0.0f, objl_force * 10.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1, 1, 1, 1);
		PLAY_AUDIO_EVENT_FROM_OBJECT(launch_sfx, objl_muz);
	}
}

void RemoveObject(void)
{
	GET_OBJECT_COORDINATES(objl_muz, &muzX, &muzY, &muzZ);
	CLEAR_AREA_OF_OBJECTS(muzX, muzY, muzZ, 100.0f);
}

void RemoveObjectPedMode(void)
{
	if (DOES_OBJECT_EXIST(objl_object))
	{
		DELETE_OBJECT(&objl_object);
		MARK_MODEL_AS_NO_LONGER_NEEDED(objl_model);
	}
}

void DrawFakeSights(void)
{
	DRAW_RECT(0.50000000, 0.50000000, 0.00234375, 0.04444444, 10, 10, 10, 122);	// vertical line shadow
	DRAW_RECT(0.50000000, 0.50000000, 0.02500000, 0.00416667, 10, 10, 10, 122);	// horizontal line shadow
	DRAW_RECT(0.50000000, 0.50000000, 0.00078125, 0.04166667, 255, 255, 255, 202);	// vertical line
	DRAW_RECT(0.50000000, 0.50000000, 0.02343750, 0.00138889, 255, 255, 255, 202);	// horizontal line
}