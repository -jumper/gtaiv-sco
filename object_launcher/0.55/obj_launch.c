#include <natives.h>
#include <common.h>
#include <strings.h>
#include <types.h>
#include <consts.h>
#include <ps3pad.h>

//#define PC
#define MAX_MENU_ITEMS 52
#define MAX_MENU_LEVLS 5
#define STYLE 4

// Menu
#include "obj_launch_locals.h"
#include "menu/menu.h"

// Trainer
#include "obj_launch_lang.h"
//#include "obj_launch_locals.h"
#include "objl_functions.c"
#include "objl_looped_functions.c"

#include "project_error.c"

// Project
#include "project_set.c"
#include "project_action.c"

// Menu
#include "menu/menu_core.c"

extern void obj_launch_startup(void);
extern void obj_launch(void);

// Mimic a startup script.
void obj_launch_startup(void)
{
	bool load_trainer;

	while(true)
	{
		WAIT(0);

		if (objl_enabled)
		{
			objl_looped();
		}
		
		if (load_trainer)
		{
			if (GET_NUMBER_OF_INSTANCES_OF_STREAMED_SCRIPT("menu_globals") == 0)
			{
				disableMenu = false;
				obj_launch();
			}
		}

		if ((IS_BUTTON_PRESSED(0, BUTTON_L1) && IS_BUTTON_JUST_PRESSED(0, BUTTON_DPAD_LEFT)) || (IS_GAME_KEYBOARD_KEY_PRESSED(KEY_E) && IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_UP_ARROW)))
		{
			load_trainer = true;
		}
		
	}
}

void obj_launch(void)
{
	menu_core_startup();
	draw_startup();

	while(true)
	{
		WAIT(0);

		if (objl_enabled)
		{
			objl_looped();
		}
		
		if (menu_back_pressed() && menu_level == 1 && !inError)
		{
			disableMenu = true;
			menu_level = 0;
		}

		if (!disableMenu)
		{
			menu_core();
			drawWindow();
			drawFrontend();
			drawHeader();
			menu_draw();
		}
		else
		{
			menu_core_shutdown();
			obj_launch_startup();
		}

		if ((IS_BUTTON_PRESSED(0, BUTTON_L1) && IS_BUTTON_JUST_PRESSED(0, BUTTON_DPAD_LEFT)) || (IS_GAME_KEYBOARD_KEY_PRESSED(KEY_E) && IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN_ARROW)))
		disableMenu = true;
	}
}

void main(void)
{/*
	if (GET_INT_STAT(450) < 1 || GET_INT_STAT(451) < 1 || GET_INT_STAT(452) < 1)
	{*/
		SetEpisodicColors(&menu_h_r, &menu_h_g, &menu_h_b);
	/*}
	else
	{
		menu_h_r = GET_INT_STAT(450);
		menu_h_g = GET_INT_STAT(451);
		menu_h_b = GET_INT_STAT(452);
	}*/
	obj_launch_startup();
}
