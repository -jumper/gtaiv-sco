#pragma once

// Error ID's
enum eGenvareErrorID
{
	OBJL_ERROR_ID_UNKNOWN,
	OBJL_ERROR_ID_INVALID,
	OBJL_ERROR_ID_CDIMAGE,
	OBJL_ERROR_ID_RAPID,
	OBJL_ERROR_ID_SPAM,
	OBJL_ERROR_ID_PED
};

bool inMenu;

uint objl_model = 0xFE520830;

Object objl_object, objl_base, objl_rpg, objl_muz, objl_misc1;

Camera objl_cam;

bool objl_enabled;
bool pedmode_enabled;
bool slomo_enabled;
bool slomo_active;
bool rapidmode_enabled;
bool objl_model_enabled = true;
bool objl_sights_enabled;

int objl_force = 10;

float olH = 0.0f, olP = 0.0f;
float muzX, muzY, muzZ;
float slo_mo = 0.5f;
uint menu_h_r, menu_h_g, menu_h_b;