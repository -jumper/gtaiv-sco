void objl_looped(void)
{
	objl_movement();
	if (objl_sights_enabled)
	{
		DrawFakeSights();
	}
	if (objl_model_enabled && !DOES_OBJECT_EXIST(objl_rpg))
	{
		CreateLauncherExtra(1);
	}
	if (!objl_model_enabled && DOES_OBJECT_EXIST(objl_rpg))
	{
		CreateLauncherExtra(0);
	}
	if (slomo_enabled)
	{
		if (IS_BUTTON_JUST_PRESSED(0, BUTTON_S))
		{
			if (slomo_active)
			{
				slomo_active = false;
				SET_TIME_SCALE(1.0f);
			}
			else
			{
				slomo_active = true;
				SET_TIME_SCALE(slo_mo);
			}
		}
	}
	if (pedmode_enabled)
	{
		if (IS_BUTTON_JUST_PRESSED(0,BUTTON_R1))
		{
			ShootObjectPedMode();
		}
		if (IS_BUTTON_JUST_PRESSED(0,BUTTON_STICK_RIGHT))
		{
			RemoveObjectPedMode();
		}
	}
	else
	{
		if (IS_BUTTON_JUST_PRESSED(0,BUTTON_STICK_RIGHT))
		{
			RemoveObject();
		}
		if (rapidmode_enabled)
		{
			if (IS_BUTTON_PRESSED(0,BUTTON_R1))
			{
				ShootObject();
			}
		}
		else
		{
			if (IS_BUTTON_JUST_PRESSED(0,BUTTON_R1))
			{
				ShootObject();
			}
		}
	}
	
}