void project_error(uint error_id)
{
	if (menu_item[item_selected].action)
		actionError = true;

	menu_clean();

	// Invalid Menu.
	menu_header = objl_error;

	if (error_id == OBJL_ERROR_ID_INVALID)
		menu_addItem(objl_error_invalid);
	else if (error_id == OBJL_ERROR_ID_CDIMAGE)
	{
		menu_addItem(objl_error_cdimage);
	}
	else if (error_id == OBJL_ERROR_ID_RAPID)
	{
		menu_addItem(objl_error_disabled);
		menu_addItem(objl_error_rapid);
	}
	else if (error_id == OBJL_ERROR_ID_PED)
	{
		menu_addItem(objl_error_disabled);
		menu_addItem(objl_error_ped);
	}
/*	else if (error_id == OBJL_ERROR_ID_)
	{
		menu_addItem(objl_error_);
	}*/

	menu_addItem(objl_error_goback);
	inError = true;

	PLAY_AUDIO_EVENT("FRONTEND_MENU_ERROR");
}