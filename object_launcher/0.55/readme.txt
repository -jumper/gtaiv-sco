
      ______________________
-  = /                      \
 - =/    Object Launcher    /
- = \______________________/

Controls:
	- L1 + Dpad Left Opens and closes the menu (Key E + Left Arrow on keyboard also works)
	- R1 shoots
	- R3 clears objects (and also gets you "unstuck")
	- Square toggles slo-mo (only if it is enabled)
	- holding L1 will slow down the controls (and show fake crosshairs if they are enabled)
	- Regular fly mod controls:
	--- Left analog moves f/b/s/s based on your heading/pitch
	--- Right analog changes heading/pitch
	--- L2 decreases your height (pressure sensing)
	--- R2 increases your height (pressure sensing)

Version 0.55
	- analog input is now inverted when "invert pitch" is set in options
	- pressure sensitive L2/R2 now working when classic controls are set in options
	- new style for Three-Socks menu library
	- added finer controls (only while holding L1)
	- changed clear / unstuck control to R3
	- controls are active while in menu - downside is menu flicker, and sometimes pedmode objects get stuck to your ped

Version 0.51
	- fixed crash when objects were destroyed before being launched in ped mode

Version 0.5
	- Three-Socks menu library added
	- 136 tested and categorized models
	- 1020 untested models broken up over 20 pages
	- Force is now 1 - 50 (it gets multiplied by 10, so the effective range is 0.0 to 500.0, in increments of 10.0)
	- Toggled slow motion that's adjustable
	- Toggled display of fake rpg and/or fake crosshairs
	- "unstuck" is now just the circle button
	- cdimage check to prevent crashing on hash errors (first listed object "ErrorTest" demonstrates this)
	- Shooting, Clearing, and Fly Mod style controls remain the same, all other controls are removed

Version 0.3
	- abandoned holding the RPG on foot for fly mod style control
	- 300 objects (lots of untested)
	- Hold L1 and tap Down toggles on / off
	- Left analog moves you around, right analog steers and adjusts pitch, L2 lowers you, R2 raises you
	- R1 shoots, Circle clears object(s), X spawns an item (ped mode only)
	- Hold L3 and tap triangle to toggle ped mode
	- Hold L3 and tap X to toggle rapid fire (disabled in ped mode)
	- Hold X and tap Left / Right to change objects (60 per set)
	- Hold X and Up / Down to change sets of objects (5 sets)
	- L3 + R3 to get unstuck when using objects that refuse to be dynamic

Version 0.2
	- 60 objects
	- adjustable force
	- less variable use

Version 0.1
	- initial version - launches objects when the player has the RPG equipped
	- L2 + X launches the object
	- L2 + DPad Right changes the object
	- L2 + L3 Clears objects (has no effect at this point) 