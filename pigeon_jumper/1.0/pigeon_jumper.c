/**
 * Pigeon Jumper 1.0
 * This is made with code from https://bitbucket.org/ThreeSocks/gtaiv-example-mods/
 * Pigeon locations are mostly taken from Gforce's location file for GTA IV Location Handler
 * - location file (http://www.gtagarage.com/mods/show.php?id=5716)
 * - GTA IV Location Handler (by Voodoo48 - http://www.gtagarage.com/mods/show.php?id=5442)
 * Some locations have been fixed / moved, and heading info was added for all.
 */

#include <natives.h>
#include <common.h>
#include <strings.h>
#include <types.h>
#include <consts.h>

// below would normally be in functions.c
void set_up_draw(int type, float width, float height, uint r, uint g, uint b, uint a)
{

	SET_TEXT_FONT(0);

	SET_TEXT_BACKGROUND(0);
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255);
	SET_TEXT_EDGE(0, 0, 0, 0, 255);

	if (type == 1)
	{
		SET_TEXT_BACKGROUND(1);
	}
	else if (type == 2)
	{
		SET_TEXT_DROPSHADOW(1, 0, 0, 0, 255);
	}
	else if (type == 3)
	{
		SET_TEXT_EDGE(1, 0, 0, 0, 255);
	}

	SET_TEXT_COLOUR(r, g, b, a);
	SET_TEXT_SCALE(width, height);
	SET_TEXT_PROPORTIONAL(1);

	SET_TEXT_JUSTIFY(0);
	SET_TEXT_CENTRE(0);
	SET_TEXT_RIGHT_JUSTIFY(0);

}

float draw_text(char *gxtName, float x, float y, char *val)
{
	float result;
	result = GET_STRING_WIDTH_WITH_STRING(gxtName, val);
	DISPLAY_TEXT_WITH_LITERAL_STRING(x, y, gxtName, val);
	return result;
}

void draw_number(char *gxtName, float x, float y, int val)
{
	DISPLAY_TEXT_WITH_NUMBER(x, y, gxtName, val);
}

void draw_float(char *gxtName, float x, float y, float val)
{
	DISPLAY_TEXT_WITH_FLOAT(x, y, gxtName, val, 4);
}
//end functions.c

int activatePigeonIncrement = false;
int activatePigeonDecrement = false;
int activateJump = false;
int activateToggleInfo = false;
float pX, pY, pZ, pH, width = 0.30000000, height = 0.30000000;
int PigeonNumber = 1;
int InfoToggle = 1;

void CoordSelect(void)
{
	if (PigeonNumber == 1)
	{
		pX = 1383.285, pY = -740.899, pZ = 9.416676, pH = 323.5584;
	}
	if (PigeonNumber == 2)
	{
		pX = 1600.553, pY = 176.6339, pZ = 23.06749, pH = 235.2389;
	}
	if (PigeonNumber == 3)
	{
		pX = 1434.26, pY = 207.7654, pZ = 31.65166, pH = 178.4481;
	}
	if (PigeonNumber == 4)
	{
		pX = 1083.7644, pY = 35.6579, pZ = 39.0993, pH = 2.1856;
	}
	if (PigeonNumber == 5)
	{
		pX = 851.2717, pY = -175.5297, pZ = 13.76238, pH = 246.6126;
	}
	if (PigeonNumber == 6)
	{
		pX = 934.554, pY = -848.7029, pZ = 1.611683, pH = 110.399;
	}
	if (PigeonNumber == 7)
	{
		pX = 1151.703, pY = -589.5602, pZ = 39.07723, pH = 305.4143;
	}
	if (PigeonNumber == 8)
	{
		pX = 1002.1315, pY = -655.6766, pZ = 17.7107, pH = 286.4843;
	}
	if (PigeonNumber == 9)
	{
		pX = 819.5607, pY = -585.6087, pZ = 17.0262, pH = 271.5810;
	}
	if (PigeonNumber == 10)
	{
		pX = 1515.798, pY = -420.8813, pZ = 33.09132, pH = 87.5694;
	}
	if (PigeonNumber == 11)
	{
		pX = 1314.177, pY = -508.7329, pZ = 15.06778, pH = 109.5210;
	}
	if (PigeonNumber == 12)
	{
		pX = 1285.1742, pY = -316.4314, pZ = 25.0464, pH = 276.5133;
	}
	if (PigeonNumber == 13)
	{
		pX = 793.163, pY = -232.9077, pZ = 22.07077, pH = 106.0238;
	}
	if (PigeonNumber == 14)
	{
		pX = 1160.622, pY = -458.762, pZ = 14.07339, pH = 308.9102;
	}
	if (PigeonNumber == 15)
	{
		pX = 721.6820, pY = -440.0259, pZ = 3.2570, pH = 268.8303;
	}
	if (PigeonNumber == 16)
	{
		pX = 959.0553, pY = -293.523, pZ = 25.63162, pH = 77.7806;
	}
	if (PigeonNumber == 17)
	{
		pX = 1068.73, pY = -171.8173, pZ = 29.9158, pH = 347.5034;
	}
	if (PigeonNumber == 18)
	{
		pX = 1131.2736, pY = 236.9722, pZ = 36.9350, pH = 257.2211;
	}
	if (PigeonNumber == 19)
	{
		pX = 1302.736, pY = 163.7834, pZ = 32.64115, pH = 247.0886;
	}
	if (PigeonNumber == 20)
	{
		pX = 1309.407, pY = -176.9174, pZ = 28.52095, pH = 47.2476;
	}
	if (PigeonNumber == 21)
	{
		pX = 1326.699, pY = -45.38095, pZ = 26.29384, pH = 321.4253;
	}
	if (PigeonNumber == 22)
	{
		pX = 797.7863, pY = 130.8565, pZ = 11.21149, pH = 101.9958;
	}
	if (PigeonNumber == 23)
	{
		pX = 949.525, pY = 419.7842, pZ = 15.37073, pH = 162.3789;
	}
	if (PigeonNumber == 24)
	{
		pX = 1406.677, pY = 404.5299, pZ = 34.08654, pH = 287.2391;
	}
	if (PigeonNumber == 25)
	{
		pX = 1175.9348, pY = 441.9866, pZ = 32.1274, pH = 186.7382;
	}
	if (PigeonNumber == 26)
	{
		pX = 858.4955, pY = 586.0189, pZ = 10.19532, pH = 317.7452;
	}
	if (PigeonNumber == 27)
	{
		pX = 1318.031, pY = 666.8534, pZ = 51.55698, pH = 259.0385;
	}
	if (PigeonNumber == 28)
	{
		pX = 1267.582, pY = 773.151, pZ = 48.82607, pH = 262.2472;
	}
	if (PigeonNumber == 29)
	{
		pX = 1622.288, pY = 443.4826, pZ = 44.24736, pH = 109.9884;
	}
	if (PigeonNumber == 30)
	{
		pX = 1611.998, pY = 851.67, pZ = 15.84786, pH = 92.7201;
	}
	if (PigeonNumber == 31)
	{
		pX = 1488.626, pY = 615.8231, pZ = 29.87448, pH = 214.0062;
	}
	if (PigeonNumber == 32)
	{
		pX = 1380.223, pY = 532.2262, pZ = 45.04935, pH = 276.8433;
	}
	if (PigeonNumber == 33)
	{
		pX = 1151.2571, pY = 686.8099, pZ = 40.7997, pH = 100.8311;
	}
	if (PigeonNumber == 34)
	{
		pX = 894.6882, pY = 700.5655, pZ = 19.0958, pH = 54.0379;
	}
	if (PigeonNumber == 35)
	{
		pX = 908.8833, pY = 943.3969, pZ = 11.95245, pH = 215.0709;
	}
	if (PigeonNumber == 36)
	{
		pX = 1168.6318, pY = 780.7050, pZ = 36.12385, pH = 198.7474;
	}
	if (PigeonNumber == 37)
	{
		pX = 1255.965, pY = 997.7284, pZ = 13.69034, pH = 143.4326;
	}
	if (PigeonNumber == 38)
	{
		pX = 1256.5928, pY = 999.0252, pZ = 13.69034, pH = 149.6828;
	}
	if (PigeonNumber == 39)
	{
		pX = 1120.766, pY = 914.2169, pZ = 32.71244, pH = 95.6927;
	}
	if (PigeonNumber == 40)
	{
		pX = 1272.756, pY = 897.1676, pZ = 30.49624, pH = 143.7759;
	}
	if (PigeonNumber == 41)
	{
		pX = 1097.8457, pY = 1000.7336, pZ = 16.96753, pH = 133.9240;
	}
	if (PigeonNumber == 42)
	{
		pX = 1745.382, pY = 651.2075, pZ = 33.1183, pH = 277.6213;
	}
	if (PigeonNumber == 43)
	{
		pX = 1830.5061, pY = 399.0555, pZ = 33.30363, pH = 100.9558;
	}
	if (PigeonNumber == 44)
	{
		pX = 1888.728, pY = 781.4185, pZ = 22.40464, pH = 108.4054;
	}
	if (PigeonNumber == 45)
	{
		pX = 2306.737, pY = 613.4068, pZ = 17.02295, pH = 56.4391;
	}
	if (PigeonNumber == 46)
	{
		pX = 2620.785, pY = 416.8716, pZ = 79.35115, pH = 121.9735;
	}
	if (PigeonNumber == 47)
	{
		pX = 2366.842, pY = 365.9368, pZ = 12.74285, pH = 358.6936;
	}
	if (PigeonNumber == 48)
	{
		pX = 2319.946, pY = 336.0617, pZ = 5.921841, pH = 80.7880;
	}
	if (PigeonNumber == 49)
	{
		pX = 747.8826, pY = 2099.4143, pZ = 1.45123, pH = 172.0587;
	}
	if (PigeonNumber == 50)
	{
		pX = 625.7219, pY = 1759.83, pZ = 38.40894, pH = 202.3147;
	}
	if (PigeonNumber == 51)
	{
		pX = 408.6021, pY = 2053.654, pZ = 6.704692, pH = 270.4292;
	}
	if (PigeonNumber == 52)
	{
		pX = 806.4045, pY = 1804.4945, pZ = 39.3746, pH = 333.5993;
	}
	if (PigeonNumber == 53)
	{
		pX = 612.8613, pY = 1867.4178, pZ = 33.20713, pH = 76.3805;
	}
	if (PigeonNumber == 54)
	{
		pX = 783.539, pY = 1403.63, pZ = 15.1128, pH = 264.0440;
	}
	if (PigeonNumber == 55)
	{
		pX = 701.3419, pY = 1292.367, pZ = 9.811462, pH = 188.2473;
	}
	if (PigeonNumber == 56)
	{
		pX = 394.4674, pY = 1709.2351, pZ = 19.4610, pH = 278.2657;
	}
	if (PigeonNumber == 57)
	{
		pX = 387.5847, pY = 1655.8699, pZ = 14.9850, pH = 290.9471;
	}
	if (PigeonNumber == 58)
	{
		pX = 966.6344, pY = 1630.7975, pZ = 33.3168, pH = 241.3181;
	}
	if (PigeonNumber == 59)
	{
		pX = 1031.042, pY = 1573.752, pZ = 8.682259, pH = 247.9034;
	}
	if (PigeonNumber == 60)
	{
		pX = 1252.0646, pY = 1553.1099, pZ = 22.5909, pH = 9.6287;
	}
	if (PigeonNumber == 61)
	{
		pX = 1503.936, pY = 1820.952, pZ = 3.433424, pH = 297.1246;
	}
	if (PigeonNumber == 62)
	{
		pX = 906.5904, pY = 1896.427, pZ = 38.11735, pH = 251.9811;
	}
	if (PigeonNumber == 63)
	{
		pX = 1254.798, pY = 1833.884, pZ = 10.16821, pH = 290.9968;
	}
	if (PigeonNumber == 64)
	{
		pX = 1073.138, pY = 1819.125, pZ = 12.75187, pH = 329.6127;
	}
	if (PigeonNumber == 65)
	{
		pX = 299.3426, pY = 1359.3217, pZ = 8.3788, pH = 10.1082;
	}
	if (PigeonNumber == 66)
	{
		pX = 513.4402, pY = 1250.0426, pZ = 2.6471, pH = 171.0679;
	}
	if (PigeonNumber == 67)
	{
		pX = 483.647, pY = 1493.084, pZ = 11.66659, pH = 25.6367;
	}
	if (PigeonNumber == 68)
	{
		pX = 482.739, pY = 1498.871, pZ = 17.80412, pH = 188.5360;
	}
	if (PigeonNumber == 69)
	{
		pX = 571.6302, pY = 1506.615, pZ = 22.69814, pH = 241.7429;
	}
	if (PigeonNumber == 70)
	{
		pX = 571.5735, pY = 1508.8665, pZ = 22.6981, pH = 229.0048;
	}
	if (PigeonNumber == 71)
	{
		pX = 453.2568, pY = 1112.64, pZ = 2.718945, pH = 107.9832;
	}
	if (PigeonNumber == 72)
	{
		pX = 344.927, pY = 1012.221, pZ = 36.14861, pH = 185.3477;
	}
	if (PigeonNumber == 73)
	{
		pX = 742.874, pY = 608.1206, pZ = 34.59634, pH = 233.7561;
	}
	if (PigeonNumber == 74)
	{
		pX = -359.9076, pY = -678.3126, pZ = 2.512487, pH = 63.7899;
	}
	if (PigeonNumber == 75)
	{
		pX = -30.03113, pY = -955.344, pZ = 13.15468, pH = 313.5756;
	}
	if (PigeonNumber == 76)
	{
		pX = 111.9983, pY = -963.4715, pZ = 5.183868, pH = 317.6277;
	}
	if (PigeonNumber == 77)
	{
		pX = -282.3553, pY = -389.2843, pZ = 8.736195, pH = 145.6278;
	}
	if (PigeonNumber == 78)
	{
		pX = -86.30613, pY = -339.443, pZ = 14.71113, pH = 212.3926;
	}
	if (PigeonNumber == 79)
	{
		pX = -208.0092, pY = 1235.391, pZ = 22.33825, pH = 216.8851;
	}
	if (PigeonNumber == 80)
	{
		pX = 139.1682, pY = 1302.995, pZ = 2.519422, pH = 295.4556;
	}
	if (PigeonNumber == 81)
	{
		pX = -28.0231, pY = 1393.44, pZ = 30.20756, pH = 75.0356;
	}
	if (PigeonNumber == 82)
	{
		pX = 39.06916, pY = 110.9621, pZ = 14.95967, pH = 252.7628;
	}
	if (PigeonNumber == 83)
	{
		pX = 237.9668, pY = -171.7449, pZ = 4.966308, pH = 276.4021;
	}
	if (PigeonNumber == 84)
	{
		pX = 241.5198, pY = -419.7524, pZ = 9.014573, pH = 16.8836;
	}
	if (PigeonNumber == 85)
	{
		pX = 335.3305, pY = -640.4701, pZ = 4.684227, pH = 290.7975;
	}
	if (PigeonNumber == 86)
	{
		pX = -12.8181, pY = 411.9324, pZ = 90.4552, pH = 271.2514;
	}
	if (PigeonNumber == 87)
	{
		pX = 147.2867, pY = 482.5543, pZ = 15.19174, pH = 158.9817;
	}
	if (PigeonNumber == 88)
	{
		pX = -679.8953, pY = 1168.988, pZ = 12.08342, pH = 180.7973;
	}
	if (PigeonNumber == 89)
	{
		pX = -668.459, pY = 1157.071, pZ = 19.3763, pH = 187.1863;
	}
	if (PigeonNumber == 90)
	{
		pX = 117.3897, pY = 919.8654, pZ = 14.71668, pH = 179.0776;
	}
	if (PigeonNumber == 91)
	{
		pX = 201.9003, pY = 263.9229, pZ = 8.432988, pH = 355.1497;
	}
	if (PigeonNumber == 92)
	{
		pX = 154.3948, pY = 226.5417, pZ = 20.86341, pH = 279.6035;
	}
	if (PigeonNumber == 93)
	{
		pX = 495.0918, pY = -391.4711, pZ = 85.40078, pH = 53.4189;
	}
	if (PigeonNumber == 94)
	{
		pX = 277.8798, pY = 682.8503, pZ = 5.33554, pH = 304.2930;
	}
	if (PigeonNumber == 95)
	{
		pX = -261.3190, pY = 710.554, pZ = 12.3349, pH = 115.2337;
	}
	if (PigeonNumber == 96)
	{
		pX = -212.0801, pY = 1040.172, pZ = 11.25033, pH = 297.0136;
	}
	if (PigeonNumber == 97)
	{
		pX = -33.43482, pY = 722.3671, pZ = 19.99577, pH = 118.5545;
	}
	if (PigeonNumber == 98)
	{
		pX = -24.76914, pY = 843.3276, pZ = 19.61367, pH = 174.8987;
	}
	if (PigeonNumber == 99)
	{
		pX = -73.0489, pY = 945.5134, pZ = 18.1216, pH = 192.4595;
	}
	if (PigeonNumber == 100)
	{
		pX = -599.8979, pY = 841.4153, pZ = 14.7133, pH = 320.5447;
	}
	if (PigeonNumber == 101)
	{
		pX = -523.6762, pY = 643.7216, pZ = 13.09177, pH = 289.0952;
	}
	if (PigeonNumber == 102)
	{
		pX = -389.0036, pY = 874.2890, pZ = 18.1903, pH = 101.3588;
	}
	if (PigeonNumber == 103)
	{
		pX = -415.8018, pY = 1310.263, pZ = 94.10352, pH = 217.5175;
	}
	if (PigeonNumber == 104)
	{
		pX = -439.0022, pY = 1286.241, pZ = 41.5662, pH = 62.1338;
	}
	if (PigeonNumber == 105)
	{
		pX = -369.2202, pY = 1276.3971, pZ = 24.6813, pH = 5.7491;
	}
	if (PigeonNumber == 106)
	{
		pX = -607.2336, pY = 1396.384, pZ = 8.856962, pH = 80.1005;
	}
	if (PigeonNumber == 107)
	{
		pX = -205.8305, pY = 1512.9873, pZ = 29.5307, pH = 182.5688;
	}
	if (PigeonNumber == 108)
	{
		pX = -250.25, pY = 1769.298, pZ = 3.036861, pH = 335.3689;
	}
	if (PigeonNumber == 109)
	{
		pX = -428.4367, pY = 1548.678, pZ = 22.77744, pH = 345.0095;
	}
	if (PigeonNumber == 110)
	{
		pX = -320.0247, pY = 1507.756, pZ = 19.22275, pH = 14.4681;
	}
	if (PigeonNumber == 111)
	{
		pX = -10.09543, pY = 1495.568, pZ = 18.43006, pH = 5.7147;
	}
	if (PigeonNumber == 112)
	{
		pX = 270.5848, pY = 34.24569, pZ = 4.730324, pH = 183.9399;
	}
	if (PigeonNumber == 113)
	{
		pX = -420.966, pY = 437.3576, pZ = 12.48775, pH = 218.9459;
	}
	if (PigeonNumber == 114)
	{
		pX = -174.3938, pY = 210.0668, pZ = 14.7641, pH = 94.8932;
	}
	if (PigeonNumber == 115)
	{
		pX = -117.803, pY = 428.5324, pZ = 14.61196, pH = 298.7517;
	}
	if (PigeonNumber == 116)
	{
		pX = -289.1924, pY = 238.1573, pZ = 204.3569, pH = 246.3204;
	}
	if (PigeonNumber == 117)
	{
		pX = -246.948, pY = 245.6846, pZ = 14.81791, pH = 172.3754;
	}
	if (PigeonNumber == 118)
	{
		pX = -453.2754, pY = -256.6083, pZ = 7.920385, pH = 66.3034;
	}
	if (PigeonNumber == 119)
	{
		pX = -318.3607, pY = -290.9279, pZ = 14.16998, pH = 127.0191;
	}
	if (PigeonNumber == 120)
	{
		pX = -119.9337, pY = -706.1703, pZ = 10.68728, pH = 273.3777;
	}
	if (PigeonNumber == 121)
	{
		pX = 29.89901, pY = -600.777, pZ = 14.68614, pH = 51.6255;
	}
	if (PigeonNumber == 122)
	{
		pX = 104.7059, pY = -762.7701, pZ = 4.945235, pH = 351.8055;
	}
	if (PigeonNumber == 123)
	{
		pX = -462.8968, pY = 4.4506, pZ = 12.4180, pH = 23.2200;
	}
	if (PigeonNumber == 124)
	{
		pX = -410.1084, pY = -86.1474, pZ = 15.24263, pH = 319.6714;
	}
	if (PigeonNumber == 125)
	{
		pX = -299.5299, pY = -87.13524, pZ = 334.7692, pH = 337.0668;
	}
	if (PigeonNumber == 126)
	{
		pX = -122.0459, pY = 16.4138, pZ = 32.6816, pH = 117.1678;
	}
	if (PigeonNumber == 127)
	{
		pX = -69.71039, pY = -89.14568, pZ = 19.62749, pH = 226.2583;
	}
	if (PigeonNumber == 128)
	{
		pX = -466.7071, pY = 1023.0390, pZ = 14.9370, pH = 185.1889;
	}
	if (PigeonNumber == 129)
	{
		pX = -505.567, pY = 1125.57, pZ = 12.84894, pH = 278.5105;
	}
	if (PigeonNumber == 130)
	{
		pX = -505.9778, pY = 282.1847, pZ = 20.73759, pH = 286.1236;
	}
	if (PigeonNumber == 131)
	{
		pX = -1653.411, pY = 413.7862, pZ = 46.89301, pH = 240.1678;
	}
	if (PigeonNumber == 132)
	{
		pX = -1324.434, pY = 516.8026, pZ = 20.73969, pH = 0.6895;
	}
	if (PigeonNumber == 133)
	{
		pX = -1769.425, pY = 260.9086, pZ = 22.42062, pH = 337.1423;
	}
	if (PigeonNumber == 134)
	{
		pX = -1423.07, pY = 405.0429, pZ = 18.88369, pH = 96.2722;
	}
	if (PigeonNumber == 135)
	{
		pX = -1266.0066, pY = -252.1225, pZ = 3.2728, pH = 274.2257;
	}
	if (PigeonNumber == 136)
	{
		pX = -1266.0066, pY = -252.1225, pZ = 3.2728, pH = 265.1406;
	}
	if (PigeonNumber == 137)
	{
		pX = -1442.543, pY = -540.6039, pZ = 9.588781, pH = 98.9276;
	}
	if (PigeonNumber == 138)
	{
		pX = -1675.439, pY = -486.9536, pZ = 51.1483, pH = 87.3287;
	}
	if (PigeonNumber == 139)
	{
		pX = -2019.197, pY = -410.2822, pZ = 4.370708, pH = 9.4103;
	}
	if (PigeonNumber == 140)
	{
		pX = -2016.546, pY = -491.181, pZ = 7.179315, pH = 174.3658;
	}
	if (PigeonNumber == 141)
	{
		pX = -1035.699, pY = -295.3102, pZ = 12.58006, pH = 219.3775;
	}
	if (PigeonNumber == 142)
	{
		pX = -1701.049, pY = -329.4572, pZ = 2.93644, pH = 77.2212;
	}
	if (PigeonNumber == 143)
	{
		pX = -843.715, pY = 825.903, pZ = 4.220965, pH = 92.1965;
	}
	if (PigeonNumber == 144)
	{
		pX = -1259.989, pY = 1075.347, pZ = 23.72748, pH = 307.6093;
	}
	if (PigeonNumber == 145)
	{
		pX = -986.5027, pY = 1032.3661, pZ = 31.6032, pH = 270.5611;
	}
	if (PigeonNumber == 146)
	{
		pX = -1421.007, pY = 888.954, pZ = 23.03611, pH = 196.8506;
	}
	if (PigeonNumber == 147)
	{
		pX = -1241.611, pY = 1087.416, pZ = 24.79119, pH = 25.4162;
	}
	if (PigeonNumber == 148)
	{
		pX = -898.5242, pY = 772.4515, pZ = 7.6794, pH = 182.7326;
	}
	if (PigeonNumber == 149)
	{
		pX = -1197.601, pY = 988.6091, pZ = 19.76106, pH = 93.7904;
	}
	if (PigeonNumber == 150)
	{
		pX = -1390.362, pY = 995.0355, pZ = 22.26179, pH = 107.0984;
	}
	if (PigeonNumber == 151)
	{
		pX = -1015.375, pY = 939.4417, pZ = 23.01725, pH = 105.8467;
	}
	if (PigeonNumber == 152)
	{
		pX = -837.1313, pY = 1030.7878, pZ = 15.6335, pH = 79.2022;
	}
	if (PigeonNumber == 153)
	{
		pX = -1399.563, pY = 1146.322, pZ = 19.74726, pH = 44.6165;
	}
	if (PigeonNumber == 154)
	{
		pX = -1371.4835, pY = 742.0445, pZ = 19.7305, pH = 163.7292;
	}
	if (PigeonNumber == 155)
	{
		pX = -1649.537, pY = 604.081, pZ = 24.69057, pH = 355.8536;
	}
	if (PigeonNumber == 156)
	{
		pX = -1597.918, pY = 507.162, pZ = 30.48269, pH = 49.6442;
	}
	if (PigeonNumber == 157)
	{
		pX = -1500.17, pY = 580.5726, pZ = 23.33288, pH = 293.5972;
	}
	if (PigeonNumber == 158)
	{
		pX = -1593.9229, pY = 840.0094, pZ = 23.4767, pH = 52.0435;
	}
	if (PigeonNumber == 159)
	{
		pX = -1499.854, pY = 1391.769, pZ = 22.14738, pH = 332.3361;
	}
	if (PigeonNumber == 160)
	{
		pX = -1032.172, pY = 1187.303, pZ = 23.3954, pH = 1.9148;
	}
	if (PigeonNumber == 161)
	{
		pX = -931.6993, pY = 1340.272, pZ = 24.84639, pH = 287.5217;
	}
	if (PigeonNumber == 162)
	{
		pX = -1313.046, pY = 1270.708, pZ = 22.37042, pH = 61.1242;
	}
	if (PigeonNumber == 163)
	{
		pX = -1073.457, pY = 1282.583, pZ = 39.10277, pH = 279.2809;
	}
	if (PigeonNumber == 164)
	{
		pX = -727.8417, pY = 1245.613, pZ = 1.941154, pH = 296.4911;
	}
	if (PigeonNumber == 165)
	{
		pX = -854.4346, pY = -399.9461, pZ = 7.565543, pH = 32.4411;
	}
	if (PigeonNumber == 166)
	{
		pX = -1248.075, pY = 626.0641, pZ = -1.14737, pH = 299.0793;
	}
	if (PigeonNumber == 167)
	{
		pX = -1182.168, pY = 638.9814, pZ = 7.952938, pH = 92.7801;
	}
	if (PigeonNumber == 168)
	{
		pX = -1264.043, pY = 228.1638, pZ = 5.357387, pH = 337.7398;
	}
	if (PigeonNumber == 169)
	{
		pX = -1034.292, pY = 46.88329, pZ = 10.48765, pH = 3.3650;
	}
	if (PigeonNumber == 170)
	{
		pX = -915.2019, pY = 349.3284, pZ = 4.508037, pH = 252.2116;
	}
	if (PigeonNumber == 171)
	{
		pX = -1365.0349, pY = 278.8444, pZ = 19.3062, pH = 33.33299;
	}
	if (PigeonNumber == 172)
	{
		pX = -2247.302, pY = -24.00233, pZ = 5.097257, pH = 24.3397;
	}
	if (PigeonNumber == 173)
	{
		pX = -2132.161, pY = 18.01088, pZ = 16.09418, pH = 221.5258;
	}
	if (PigeonNumber == 174)
	{
		pX = -1790.545, pY = 113.6732, pZ = 18.91089, pH = 116.4781;
	}
	if (PigeonNumber == 175)
	{
		pX = -1435.265, pY = -75.17609, pZ = 34.36533, pH = 12.6048;
	}
	if (PigeonNumber == 176)
	{
		pX = -1997.672, pY = 201.0731, pZ = 16.89466, pH = 253.6190;
	}
	if (PigeonNumber == 177)
	{
		pX = -1505.8075, pY = -148.8124, pZ = 13.7608, pH = 188.2738;
	}
	if (PigeonNumber == 178)
	{
		pX = -1585.076, pY = 26.78789, pZ = 13.72593, pH = 107.4789;
	}
	if (PigeonNumber == 179)
	{
		pX = -1595.637, pY = 147.4583, pZ = 12.81961, pH = 82.9955;
	}
	if (PigeonNumber == 180)
	{
		pX = -1992.87, pY = -121.9282, pZ = 31.77801, pH = 281.7652;
	}
	if (PigeonNumber == 181)
	{
		pX = -1170.251, pY = 1833.316, pZ = 4.383008, pH = 206.5801;
	}
	if (PigeonNumber == 182)
	{
		pX = -1368.061, pY = 1526.635, pZ = 22.52939, pH = 249.8397;
	}
	if (PigeonNumber == 183)
	{
		pX = -1302.1476, pY = 1696.0741, pZ = 33.4487, pH = 97.3806;
	}
	if (PigeonNumber == 184)
	{
		pX = -957.9724, pY = 1763.157, pZ = 18.95356, pH = 307.9717;
	}
	if (PigeonNumber == 185)
	{
		pX = -1224.84, pY = 1459.704, pZ = 21.69226, pH = 242.0359;
	}
	if (PigeonNumber == 186)
	{
		pX = -1072.687, pY = 1574.995, pZ = 34.6039, pH = 203.4185;
	}
	if (PigeonNumber == 187)
	{
		pX = -608.7179, pY = -741.8929, pZ = 20.6431, pH = 352.0535;
	}
	if (PigeonNumber == 188)
	{
		pX = -609.4792, pY = -750.3654, pZ = 87.4757, pH = 308.5541;
	}
	if (PigeonNumber == 189)
	{
		pX = -609.4792, pY = -750.3654, pZ = 87.4757, pH = 303.9124;
	}
	if (PigeonNumber == 190)
	{
		pX = -608.2710, pY = -750.6586, pZ = 79.1245, pH = 137.8931;
	}
	if (PigeonNumber == 191)
	{
		pX = -610.3657, pY = -751.2927, pZ = 84.3303, pH = 261.2173;
	}
	if (PigeonNumber == 192)
	{
		pX = -613.2906, pY = -757.3104, pZ = 72.5530, pH = 35.3476;
	}
	if (PigeonNumber == 193)
	{
		pX = 583.6985, pY = 727.7499, pZ = 2.801165, pH = 98.1739;
	}
	if (PigeonNumber == 194)
	{
		pX = 376.5974, pY = 125.6199, pZ = 5.8811, pH = 225.0225;
	}
	if (PigeonNumber == 195)
	{
		pX = 484.537, pY = 102.5513, pZ = 7.963133, pH = 170.6147;
	}
	if (PigeonNumber == 196)
	{
		pX = 429.6814, pY = 237.8659, pZ = 14.72012, pH = 71.3860;
	}
	if (PigeonNumber == 197)
	{
		pX = 508.8451, pY = -51.78592, pZ = 16.87663, pH = 281.1568;
	}
	if (PigeonNumber == 198)
	{
		pX = 505.6121, pY = 218.0988, pZ = 30.70965, pH = 29.9183;
	}
	if (PigeonNumber == 199)
	{
		pX = 648.7534, pY = 242.0278, pZ = 42.3706, pH = 290.3705;
	}
	if (PigeonNumber == 200)
	{
		pX = 1412.357, pY = 1243.246, pZ = 2.446855, pH = 246.3507;
	}
}

void PrintText(void)
{
	if (InfoToggle == 1)
	{
		DRAW_RECT(0.17187500, 0.11111111, 0.31562500, 0.18333335, 0, 255, 0, 150);
		DRAW_RECT(0.17187500, 0.11111111, 0.31250000, 0.17777778, 0, 0, 0, 150);
		set_up_draw(2, width, height, 255, 255, 255, 210);
		draw_text("STRING", 0.02343750, 0.02555555, "Jumping to Pigeon");
		if (PigeonNumber == 86)
		{
			set_up_draw(2, width, height, 255, 255, 255, 180);
			draw_text("STRING", 0.02343750, 0.05555555, "On top of the other cross");
		}
		if (PigeonNumber == 114 || PigeonNumber == 115)
		{
			set_up_draw(2, width, height, 255, 255, 255, 180);
			draw_text("STRING", 0.02343750, 0.05555555, "In the tree..");
		}
		if (PigeonNumber == 192)
		{
			set_up_draw(2, width, height, 255, 255, 255, 180);
			draw_text("STRING", 0.02343750, 0.05555555, "Aim left a bit, or snipe from below");
		}
		set_up_draw(2, width, height, 255, 255, 10, 210);
		draw_number("NUMBR", 0.18343750, 0.02555555, PigeonNumber);
		set_up_draw(2, width, height, 255, 255, 10, 180);
		draw_text("STRING", 0.02343750, 0.08555555, "L1 + DPad Right");
		set_up_draw(2, width, height, 255, 255, 10, 180);
		draw_text("STRING", 0.02343750, 0.11555555, "L1 + DPad Left");
		set_up_draw(2, width, height, 255, 255, 10, 180);
		draw_text("STRING", 0.02343750, 0.14555555, "L1 + DPad Down");
		set_up_draw(2, width, height, 255, 255, 10, 180);
		draw_text("STRING", 0.02343750, 0.17555555, "L1 + DPad Up");
		set_up_draw(2, width, height, 255, 255, 255, 180);
		draw_text("STRING", 0.18343750, 0.08555555, "next location");
		set_up_draw(2, width, height, 255, 255, 255, 180);
		draw_text("STRING", 0.18343750, 0.11555555, "previous location");
		set_up_draw(2, width, height, 255, 255, 255, 180);
		draw_text("STRING", 0.18343750, 0.14555555, "jump");
		set_up_draw(2, width, height, 255, 255, 255, 180);
		draw_text("STRING", 0.18343750, 0.17555555, "toggle info");
	}
}

void Activators(void)
{

	if (activatePigeonIncrement)
	{
		activatePigeonIncrement = false;
		if (PigeonNumber == 200)
		{
			PigeonNumber = 1;
		}
		else
		{
			PigeonNumber = PigeonNumber + 1;
		}
	}

	if (activatePigeonDecrement)
	{
		activatePigeonDecrement = false;
		if (PigeonNumber == 1)
		{
			PigeonNumber = 200;
		}
		else
		{
			PigeonNumber = PigeonNumber - 1;
		}
	}

	if (activateToggleInfo)
	{
		activateToggleInfo = false;
		if (InfoToggle == 1)
		{
			InfoToggle = 0;
		}
		else
		{
			InfoToggle = 1;
		}
	}

	if (activateJump)
	{
		activateJump = false;
		SET_PLAYER_CONTROL(GetPlayerIndex(), false);
		LOAD_SCENE(pX, pY, pZ);
		WAIT(500);
		SET_CHAR_COORDINATES_NO_OFFSET(GetPlayerPed(), pX, pY, pZ);
		SET_CHAR_HEADING(GetPlayerPed(), pH);
		WAIT(100);
		SET_CAM_BEHIND_PED(GetPlayerPed());
		SET_PLAYER_CONTROL(GetPlayerIndex(), true);
	}

}

void ButtonInput(void)
{

	if (IS_BUTTON_PRESSED(0, BUTTON_L1) && IS_BUTTON_JUST_PRESSED(0, BUTTON_DPAD_RIGHT))
	{
		activatePigeonIncrement = true;
	}

	if (IS_BUTTON_PRESSED(0, BUTTON_L1) && IS_BUTTON_JUST_PRESSED(0, BUTTON_DPAD_LEFT))
	{
		activatePigeonDecrement = true;
	}

	if (IS_BUTTON_PRESSED(0, BUTTON_L1) && IS_BUTTON_JUST_PRESSED(0, BUTTON_DPAD_DOWN))
	{
		activateJump = true;
	}

	if (IS_BUTTON_PRESSED(0, BUTTON_L1) && IS_BUTTON_JUST_PRESSED(0, BUTTON_DPAD_UP))
	{
		activateToggleInfo = true;
	}

}

void main(void)
{
	while(true)
	{
		WAIT(0);
		ButtonInput();
		Activators();
		PrintText();
		CoordSelect();
		UpdateWeaponOfPed(GetPlayerPed(), WEAPON_PISTOL);
	}
}