#include <natives.h>
#include <common.h>
#include <strings.h>
#include <types.h>
#include <consts.h>

float ppedX, ppedY, ppedZ;
Vehicle ClosestCar;
bool CarIsGrabbed;
Object test_obj;

void GrabCar(void)
{
	GET_CHAR_COORDINATES(GetPlayerPed(),&ppedX,&ppedY,&ppedZ);
	ClosestCar = GET_CLOSEST_CAR(ppedX, ppedY, ppedZ, 90, false, 70);
	if (DOES_VEHICLE_EXIST(ClosestCar))
	{
		REQUEST_MODEL(0xB00ABE6D);	// golf ball
		while(!HAS_MODEL_LOADED(0xB00ABE6D))	{	WAIT(0);	}
		CREATE_OBJECT(0xB00ABE6D, ppedX, ppedY, ppedZ + 5.0f, &test_obj, 1);
		while(!DOES_OBJECT_EXIST(test_obj))	{	WAIT(0);	}
		SET_OBJECT_INVINCIBLE(test_obj,0);
		FREEZE_OBJECT_POSITION(test_obj,0);
		SET_OBJECT_DYNAMIC(test_obj,1);
		SET_OBJECT_AS_STEALABLE(test_obj,1);
		SET_OBJECT_COLLISION(test_obj,1);
		SET_OBJECT_RECORDS_COLLISIONS(test_obj, 1);
		GIVE_PED_PICKUP_OBJECT(GetPlayerPed(), test_obj, 1);
		if (GET_OBJECT_PED_IS_HOLDING(GetPlayerPed()) == test_obj)
		{
			FREEZE_CAR_POSITION(ClosestCar, 1);
			ATTACH_CAR_TO_OBJECT(ClosestCar, test_obj, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
			CarIsGrabbed = 1;
		}
	}
}

void CarGrabbedLoop(void)
{
	if (GET_OBJECT_PED_IS_HOLDING(GetPlayerPed()) != test_obj)
	{
		CarIsGrabbed = 0;
		WAIT(200);
		FREEZE_CAR_POSITION(ClosestCar, 0);
		DETACH_CAR(ClosestCar);
	}
}

void main(void)
{
	while(true)
	{
		WAIT(0);
		if (IS_CHAR_ON_FOOT(GetPlayerPed()))
		{
			if ((GET_CONTROL_VALUE(0, 6) == 255) && !CarIsGrabbed)
			{
				GrabCar();
			}
			if (CarIsGrabbed)
			{
				CarGrabbedLoop();
			}
		}
	}
}